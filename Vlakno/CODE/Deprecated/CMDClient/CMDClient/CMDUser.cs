﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace CMDClient
{
    using Lights;
    using Lights.EquipmnetsPureData;
    using Lights.Messages;
    using Lights.Transport;
    using System.Net;
    

    public class CMDUser
    {
        
        User server;

        string help;
        bool isActive=true;

        public void Commander()
        {

            string command;

            while (isActive)
            {
                Console.Write("USER:>");
                command = Console.ReadLine();
                switch (command.ToUpper())
                {
                    case "MSG":
                        {
                            Chat msg = new Chat();
                            msg.Recipient = serverID;
                            msg.Sender = myID;
                            Console.Write("MSG : ");
                            msg.Text = Console.ReadLine();
                            server.Send(msg);
                        }
                        break;

                    case "CHAT":
                        {
                            lock (server.Conversation)
                                foreach (var item in server.Conversation)
                                {
                                    Console.WriteLine(item);
                                }
                        }
                        break;

                    case "DATA":
                        {
                            lock (server.Data)
                                foreach (var item in server.Data)
                                {
                                    Console.WriteLine(item);
                                }
                        }
                        break;


                    case "IN":
                    case "LOGIN":
                        {
                            Console.Write("Set your name: ");
                            string name = Console.ReadLine();
                            LogIn msg = new LogIn(name);
                            msg.Recipient = serverID;
                            msg.Sender = myID;
                            server.Send(msg);
                        }
                        break;

                    case "OUT":
                    case "LOGOUT":
                        {
                            LogOut msg = new LogOut();
                            msg.Recipient = serverID;
                            msg.Sender = myID;
                            server.Send(msg);
                        }
                        break;

                    case "EQ":
                        {
                            Console.Write("Kind of message: ");
                            string answer = Console.ReadLine();
                            
                            bool decide = true;
                             
                            while (decide) 
                            switch (answer.ToUpper())
                            {
                                    case "RGB1":
                                        {
                                            var values = new Components<RGBLight>(true);

                                            Console.Write("1: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[0]);
                                            Console.Write("2: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[1]);
                                            Console.Write("3: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[2]);
                                            Console.Write("Priority: ");
                                            byte imp;
                                            byte.TryParse(Console.ReadLine(), out imp);

                                            //myID jenom testovani
                                            decide = false;
                                            State<RGBLight> stav = new State<RGBLight>(myID, imp, values);

                                            Scene sc = new Scene();
                                            sc.EquipmentsData.Add(stav);
                                            sc.Sender = new ID(new byte[] {192,168,1,101 });
                                            sc.Recipient = serverID;
                                            server.Send(sc);
                                        }
                                        break;


                                    case "RGB2":
                                        {
                                            var values = new Components<RGBLight>(true);

                                            Console.Write("1: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[0]);
                                            Console.Write("2: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[1]);
                                            Console.Write("3: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[2]);
                                            Console.Write("Priority: ");
                                            byte imp;
                                            byte.TryParse(Console.ReadLine(), out imp);

                                            //myID jenom testovani
                                            decide = false;
                                            State<RGBLight> stav = new State<RGBLight>(myID, imp, values);

                                            Scene sc = new Scene();
                                            sc.EquipmentsData.Add(stav);
                                            sc.Sender = sc.Sender = new ID(new byte[] { 192, 168, 1, 102});
                                            sc.Recipient = serverID;
                                            server.Send(sc);
                                        }
                                        break;




                                    case "RGB3":
                                        {
                                            var values = new Components<RGBLight>(true);

                                            Console.Write("1: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[0]);
                                            Console.Write("2: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[1]);
                                            Console.Write("3: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[2]);
                                            Console.Write("Priority: ");
                                            byte imp;
                                            byte.TryParse(Console.ReadLine(), out imp);

                                            //myID jenom testovani
                                            decide = false;
                                            State<RGBLight> stav = new State<RGBLight>(myID, imp, values);

                                            Scene sc = new Scene();
                                            sc.EquipmentsData.Add(stav);
                                            sc.Sender = sc.Sender = new ID(new byte[] { 192, 168, 1, 103 });
                                            sc.Recipient = serverID;
                                            server.Send(sc);
                                        }
                                        break;



                                    case "SPYDER":
                                        {
                                            var values = new Components<SpyderLight>(true);

                                            Console.Write("1: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[0]);
                                            Console.Write("2: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[1]);
                                            Console.Write("3: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[2]);
                                            Console.Write("4: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[3]);
                                            Console.Write("5: ");
                                            byte.TryParse(Console.ReadLine(), out values.Data[4]);
                                            
                                            Console.Write("Priority: ");
                                            byte imp;
                                            byte.TryParse(Console.ReadLine(), out imp);
                                            State<SpyderLight> stav = new State<SpyderLight>(myID, 21, values);
                                            decide = false;
                                            Scene sc = new Scene();
                                            sc.EquipmentsData.Add(stav);
                                            sc.Sender = myID;
                                            sc.Recipient = serverID;
                                            server.Send(sc);
                                        }
                                        break;
                                    default:
                                        {
                                            Console.WriteLine("RGB/SPYDER");
                                            answer = Console.ReadLine();
                                        }
                                        break;
                                }


                           
                            
                        }
                        break;
                   
                    case "H":
                    case "HELP":
                    case "-H":
                    case "-HELP":
                        {
                            Console.WriteLine(help);
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Wrong command, use help. -HELP");
                            
                        }
                        break;
                }
            }
        }

        public void Ports(ref int level)
        {
            Console.WriteLine("Default Port for Users is "+level);
            Console.Write("Change it? y/n :  ");
             string answer= Console.ReadLine();
             while (answer!="y" && answer != "n")
             {
                 Console.Write("y/n :  ");
                answer = Console.ReadLine();
            }
             if (answer == "y")
             {
                 Console.WriteLine("Set number > 1024:");
                 string portUser = Console.ReadLine();
                int newPort;
                if(int.TryParse(portUser,out newPort))
                {
                    level = newPort;
                }
                Console.WriteLine("New port for User is" + level);
            }
            
        }

        public void IPaddress(ref IPAddress ip)
        {
            Console.WriteLine("Default IP of server is " + ip);
            Console.Write("Change it? y/n :  ");
            string answer = Console.ReadLine();
            while (answer != "y" && answer != "n")
            {
                Console.Write("y/n :  ");
                answer = Console.ReadLine();
            }
            if (answer == "y")
            {
                Console.WriteLine("Set IP ADRESS of Server in format xxx.xxx.xxx.xxx : ");
                string newIp = Console.ReadLine();
                IPAddress.TryParse(newIp, out ip);
                
                Console.WriteLine("New port for User is" + ip);
            }

        }

       

        ID serverID;
        ID myID;
        

        public CMDUser(int Port = 6666)
        {
            Ports(ref Port);
            server = new User(Port);
            
            ///set sender ip --stack
            myID=server.SetID(Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork));

            ///for testing  adress my server pc
            IPAddress s;
            IPAddress.TryParse("192.168.88.208", out s); //for now my pc 
            IPaddress(ref s);
            serverID = server.SetID(s);
            server.ServerID = serverID;


             Commander();
            
            
            ///98.211.124.62 local 10.0.0.246
        }
    }
}





