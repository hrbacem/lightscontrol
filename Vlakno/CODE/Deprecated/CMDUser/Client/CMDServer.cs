﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace ConsoleUser
{
    using Lights;
    using Lights.Messages;
    using Lights.Transport;
    using System.Net;

    class CMDServer
    {
        Server server;
        int portUser = 6666;
        int portEq = 6665;
        IPAddress myIP;
        IID myID;
       
        public CMDServer()
        {
            
            isActive = true;
            myIP = Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            int portUser = 6666;
            int portEq = 6665;
            Ports(ref portUser,ref portEq);
            server = new Server(portUser,portEq);
            myID = server.setID(Dns.GetHostEntry(Dns.GetHostName()).AddressList
           .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork));

            Commander();
        }

        public void Ports(ref int up,ref int down)
        {
            Console.WriteLine("Default Port for Users is " + up);
            Console.Write("Change it? y/n :  ");
            string answer = Console.ReadLine();
            while (answer != "y" && answer != "n")
            {
                Console.Write("y/n :  ");
                answer = Console.ReadLine();
            }
            if (answer == "y")
            {
                Console.WriteLine("Set number > 1024:");
                string portUser = Console.ReadLine();
                int newPort;
                if (int.TryParse(portUser, out newPort))
                {
                    up = newPort;
                }
                Console.WriteLine("New port for User is" + up);
            }
            
            Console.WriteLine("Default Port for Equipments is " + down);
            Console.Write("Change it? y/n :  ");
            answer = Console.ReadLine();
            while (answer != "y" && answer != "n")
            {
                Console.Write("y/n : ");
                answer = Console.ReadLine();
            }
            if (answer == "y")
            {
                Console.WriteLine("Set number > 1024:");
                string portUser = Console.ReadLine();
                int newPort;
                if (int.TryParse(portUser, out newPort))
                {
                    down = newPort;
                }
                Console.WriteLine("New port for Equipment is"+down);
            }
        }

        bool isActive;

        /// <summary>
        /// Flag of working 
        /// </summary>
        bool IsActive { get => isActive; }


        /// <summary>
        /// Hard write down basic options
        /// </summary>
        string help = "";

        public void Commander()
        {
            string command;

            while (isActive)
            {
                Console.Write("USER:>");
                command = Console.ReadLine();
                switch (command.ToUpper())
                {
                    case "CHAT":
                        {
                            lock (server.Conversation)
                                foreach (var item in server.Conversation)
                                {
                                    Console.WriteLine(item);
                                }
                        }
                        break;

                    case "MSG":
                        {
                            Chat msg = new Chat();
                            msg.recipient = myID;
                            msg.sender = myID;
                            Console.Write("MSG : ");
                            msg.Text = Console.ReadLine();
                            server.Conversation.Add("Server: "+msg.Text);
                            server.toAll("Server: " + msg.Text);
                        }
                        break;

                    case "USERS":
                        {
                            lock (server.Connected)
                                foreach (var item in server.Connected)
                            {
                                Console.WriteLine(item.Key + " >" + item.Value);
                            }
                        }
                        break;

                    case "DATA":
                        {
                            lock (server.Data)
                                foreach (var item in server.Data)
                            {
                                Console.WriteLine(item.Key + " >" + item.Value);
                            }
                        }
                        break;

                    case "MAP":
                        {
                            lock (server.Map)
                                foreach (var item in server.Map)
                            {
                                Console.WriteLine(item.Key + " >" + item.Value);
                            }
                        }
                        break;

                   
                    case "SHUTDOWN":
                    case "OFF":
                        {
                            server.shutdown();
                            isActive = false;
                        }
                        break;


                    case "H":
                    case "HELP":
                    case "-H":
                    case "-HELP":
                        {
                            Console.WriteLine(help);
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Wrong command, use help.");
                        }
                        break;
                }
            }

        }

    }
}
