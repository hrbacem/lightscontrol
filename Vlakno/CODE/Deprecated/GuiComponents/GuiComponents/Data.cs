﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
    using Lights;
using Lights.Transport;

namespace GuiComponents
{
    
    public partial class Data<T> : DockContent 
    {

        private Dictionary<IID,T> data;
        private TextFormater formater;
        Line.ActionOfButton action;
        public delegate string TextFormater(IID key,T item);
        public Data( Dictionary<IID, T> data,Line.ActionOfButton act,TextFormater t) 
        {
            InitializeComponent();
            formater = t;
            this.data =data;
            this.action += act;
            this.action += ReloadBox;
            ReloadBox();
            
        }
               
        private void DataList_Load(object sender, EventArgs e)
        {         
        }

        private void Data_Load(object sender, EventArgs e)
        {

        }

        public void ReloadBox(IID id = null)
        {
            
            if (!(flowLayoutPanel1.Controls.Count == data.Count))
            {
                Line look;
                for (int i = flowLayoutPanel1.Controls.Count-1; i>=0; i--)
                {
                    if(flowLayoutPanel1.Controls[i] is Line)
                    {
                        look = ((Line)flowLayoutPanel1.Controls[i]);
                        if (!data.ContainsKey(look.Index)) { flowLayoutPanel1.Controls.RemoveAt(i); }

                    }
                    
                }



                //for now? 
                Line[] Contains = new Line[flowLayoutPanel1.Controls.Count];
                for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
                {
                    Contains[i] = (Line)flowLayoutPanel1.Controls[i];
                }

                lock (data)
                    foreach (var item in data)
                    {
                        if (!Contains.Where(i => i.Index == item.Key).Any())
                        {
                            Line line = Line.newLine(item.Key, formater(item.Key, item.Value), action);
                            line.Index = item.Key;
                            line.Visible = true;
                            flowLayoutPanel1.Controls.Add(line);
                        }
                    }

            }
          
        }

        

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
