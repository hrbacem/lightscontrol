﻿namespace GuiComponents
{
    partial class Line
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.delete = new System.Windows.Forms.Button();
            this.Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
           
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(3, 3);
            this.delete.Name = "delete";
            
            this.delete.Width = 70;
            this.delete.Height = 20;
            this.delete.TabIndex = 0;
            this.delete.Text = "DELETE";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // Label
            // 
            this.Label.AutoSize = true;
            this.Label.Location = new System.Drawing.Point(80, 15);
            this.Label.Name = "Label";
            //this.Label.Size = new System.Drawing.Size(44, 13);
            this.Label.TabIndex = 1;
            this.Label.Text = "EMPTY";
            // 
            // Line
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           // this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Label);
            this.Controls.Add(this.delete);
            this.Name = "Line";
            //this.Size = new System.Drawing.Size(325, 45);
            this.Width = 370;
            this.Height = 45;
            this.Load += new System.EventHandler(this.Line_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Label Label;
    }
}
