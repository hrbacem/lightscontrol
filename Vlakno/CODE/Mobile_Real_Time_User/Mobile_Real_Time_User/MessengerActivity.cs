﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Lights.Messages;
using static GlobalAppState;



namespace Mobile_Real_Time_User.Assets
{

    [Activity(Label = "Messenger")]
    public class MessengerActivity : AppCompatActivity
    {
        EditText message_edit;
        TextView show_chat;
        ScrollView text_scroll;

        internal void OnCreate(Bundle savedInstanceState, bool pure)
        {
            base.OnCreate(savedInstanceState);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_messenger);
            FloatingActionButton buttonSend = FindViewById<FloatingActionButton>(Resource.Id.button_send);
            buttonSend.Click += SendOnClick;

            message_edit = FindViewById<EditText>(Resource.Id.message_txt);
            show_chat = FindViewById<TextView>(Resource.Id.show_txt);
            text_scroll = FindViewById<ScrollView>(Resource.Id.view_scroll);

            // set update of conversation
            user.conversationUpdate += RecievedMsgConversation;

            //Write old messages - from connecting to now
            lock (user.Conversation)
                foreach (var message in user.Conversation)
                {
                    show_chat.Text += message + "\n";
                }
            // show the newest 
            text_scroll.FullScroll(FocusSearchDirection.Down);

        }


        private void SendOnClick(object sender, EventArgs eventArgs)
        {
            //send msg
            Chat msg = new Chat
            {
                Recipient = user.ServerID,
                Sender = user.UserID
            };
            string message = message_edit.Text;
            if (isDemo)
            {
                //Create ilusion of real server 
                message = nameOfUser + message;
            }
            msg.Text = message;
            user.Send(msg);
            message_edit.Text = string.Empty;
        }

        private void RecievedMsgConversation(string newmsg)
        {
            // just update textview 
            try
            {
                show_chat.Text += newmsg + "\n";
            }
            catch (Exception e)
            {


            }

            text_scroll.FullScroll(FocusSearchDirection.Down);


        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

        }
    }
}