﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Lights;
using Lights.Messages;
using Android.Util;
using Android.Graphics;
using Lights.Transport;
using Lights.Comunication;

namespace Mobile_Real_Time_User.Assets
{
    public class EqControl : LinearLayout
    {

        public List<EqControl> fathersControl;
        public delegate void onUpdate(Scene scene);

        public onUpdate update;

        public onUpdate sendUpdate;
        Context context;
        public IState state;
        public EqControl(Context context, IState state) :
        base(context)
        {

            this.state = state;

            Init(context);
            
        }

        public EqControl(Context context, IAttributeSet attrs) :
        base(context, attrs)
        {
            Init(context);
        }

        public EqControl(Context context, IAttributeSet attrs, int defStyle) :
        base(context, attrs, defStyle)
        {
            Init(context);
        }

        public int position;


        TopDownButton positionInLine;
        TopDownButton setAll;
        TopDownButton priority;
        LinearLayout holder;

        List<SeekBar> seeksBar;

        private void Init(Context ctx)
        {
            context = ctx;
            this.AddView(CreateName(4));
            seeksBar = new List<SeekBar>();

            holder = new LinearLayout(ctx);

            GenerateController(holder, state);
            this.AddView(holder);


            positionInLine = new TopDownButton(context);
            positionInLine.contentText.Text = " POSITION";

            setAll = new TopDownButton(context);
            setAll.contentText.Text = " VALUES";
            priority = new TopDownButton(context);
            priority.contentText.Text = " PRIORITY " + state.Importance.Importance.ToString();

            this.AddView(setAll);
            setAll.topButton.Text = "100%";
            setAll.topAction += Max;
            setAll.downButton.Text = "0%";
            setAll.downAction += Min;

            this.AddView(priority);
            priority.topButton.Text = "+";
            priority.topAction += (object sender, EventArgs eventArgs) =>
            {
                Lights.Transport.Priority imp = state.Importance;
                if (imp.Importance < Server.MaximalPriorityValue)
                {
                    imp = state.Importance;
                    imp.Importance++;
                    state.Importance = imp;
                    priority.contentText.Text = "PRIORITY " + state.Importance.Importance.ToString();

                }
            };

            priority.downButton.Text = "-";
            priority.downAction += (object sender, EventArgs eventArgs) =>
            {
                Lights.Transport.Priority imp = state.Importance;
                if (imp.Importance > 0)
                {
                    imp = state.Importance;
                    imp.Importance--;
                    state.Importance = imp;
                    priority.contentText.Text = "PRIORITY " + state.Importance.Importance.ToString();

                }
            };

            this.AddView(positionInLine);
            positionInLine.topButton.Text = "/\\";
            positionInLine.downButton.Text = "\\/";
            positionInLine.topAction += MoveUp;
            positionInLine.downAction += MoveDown;

            this.SetBackgroundColor(Color.DarkGray);

            this.SetPadding(10, 10, 10, 10);


            SpecialCharacteristic(state);
        }
        int fromServerCount = 0;
        bool fromServer = false;
        public void UpdateView()
        {
            fromServer = true;
            
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                if (seeksBar[i].Progress == state.Parts.Data[i]) continue;
                fromServerCount++;
                seeksBar[i].Progress = state.Parts.Data[i];
                

            }
            fromServer = false;

        }

        

      
        private void GenerateController(LinearLayout container, IState state)
        {

            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                var item = state.Parts.Data[i];
                int k = i;
                LinearLayout lin = new LinearLayout(context)
                {
                    Orientation = Orientation.Vertical
                };
                TextView tv = new TextView(context)
                {
                    Text = item.ToString()
                };

                SeekBar bar = new SeekBar(context)
                {
                    Max = 255,
                    Min = 0,
                    ScaleX = 1.3f,
                    ScaleY = 2.8f,
                    Rotation = 270,

                    Progress = item
                };

                bar.ProgressChanged +=
                    (object x, SeekBar.ProgressChangedEventArgs y)
                    =>
                    {
                        tv.Text = bar.Progress.ToString();
                        if(fromServerCount > 0)
                        {
                            fromServerCount--;
                            return;
                        }
                        if (fromServer) return;
                        if (setAllFlag) return;
                        state.Parts.Data[k] = (byte)bar.Progress;
                        state.PartImportance[k] = true;
                        sendUpdate?.Invoke(null);
                    };

                LinearLayout.LayoutParams parametry = new LinearLayout.LayoutParams(350, 450);
                bar.LayoutParameters = (parametry);
                lin.AddView(bar);
                lin.AddView(tv);

                container.AddView(lin);
                seeksBar.Add(bar);

            }
        }

        public void SpecialCharacteristic(IState state)
        {
            ///SPecial setting for this case
            switch (state.Parts.Data.Length)
            {
                case 3:
                    {
                        foreach (var item in seeksBar)
                        {
                            item.ProgressChanged += (s, e) => ColorUpdate(0, 1, 2);
                        }
                        ColorUpdate(0, 1, 2);
                    }
                    break;
                case 5:
                    {
                        seeksBar[3].Max = 180;
                        seeksBar[4].Max = 180;
                        for (int i = 0; i < 3; i++)
                        {
                            seeksBar[i].ProgressChanged += (s, e) => ColorUpdate(0, 1, 2);
                        }
                        ColorUpdate(0, 1, 2);
                    }
                    break;
                case 12:
                    {
                        //Nothing
                    }
                    break;
                default:
                    {
                        //Nothing
                    }
                    break;
            }


        }


        public void ColorUpdate(int idR, int idG, int idB)
        {
            int R = seeksBar[idR].Progress;
            int G = seeksBar[idG].Progress;
            int B = seeksBar[idB].Progress;
            if (R + G + B < 60)
            {
                if (R < 20) R = 20;
                if (G < 20) G = 20;
                if (B < 20) B = 20;
            }
            this.SetBackgroundColor(new Color(R, G, B));
        }



        private void Bar_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }


        bool setAllFlag = false;
        private void Min(object sender, EventArgs eventArgs)
        {
            setAllFlag = true;
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                state.SetValue(i, (byte)seeksBar[i].Min);
                seeksBar[i].Progress = seeksBar[i].Min;
            }
            setAllFlag = false;
            sendUpdate?.Invoke(null);

        }

        private void Max(object sender, EventArgs eventArgs)
        {
            setAllFlag = true;
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                state.SetValue(i, (byte)seeksBar[i].Max);
                seeksBar[i].Progress = seeksBar[i].Max;
            }

            setAllFlag = false;
            sendUpdate?.Invoke(null);
        }

        private void MoveUp(object sender, EventArgs eventArgs)
        {

            if (fathersControl == null) return;
            if (position <= 0) return;


            var down = fathersControl[position];
            var top = fathersControl[position - 1];

            fathersControl[position] = top;
            fathersControl[position - 1] = down;

            top.position++;
            down.position--;
            update?.Invoke(null);

        }

        private void MoveDown(object sender, EventArgs eventArgs)
        {

            if (fathersControl == null) return;
            if (position >= fathersControl.Count - 1) return;


            var down = fathersControl[position + 1];
            var top = fathersControl[position];

            fathersControl[position] = down;
            fathersControl[position + 1] = top;

            top.position++;
            down.position--;

            update?.Invoke(null);
        }

        private LinearLayout CreateName(int numb)
        {
            LinearLayout name = new LinearLayout(context)
            {
                Orientation = Orientation.Vertical
            };

            for (int i = 0; i < numb && i < state.Id.Length; i++)
            {

                var part = new TextView(context)
                {
                    Text = state.Id.Parts[i].ToString()
                };

                name.AddView(part);
            }

            return name;
        }

    }
}