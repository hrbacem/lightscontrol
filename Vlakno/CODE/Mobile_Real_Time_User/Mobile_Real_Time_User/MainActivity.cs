﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Android.App;
using Android.Content;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Lights;
using Lights.Comunication;
using Lights.Messages;
using Lights.Transport;
using Mobile_Real_Time_User.Assets;

using static GlobalAppState;


// Global state in whole app
public class GlobalAppState
{
    //Default user
    public static User user;
    public static IPAddress MyIp;
    public static ID MyId;
    public static string nameOfUser;
    public static bool isDemo = false;
}


namespace Mobile_Real_Time_User
{

    [Activity(Label = "Login to Light Controller", Icon = "@drawable/track_light", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        const int TimeConstant = 3000;
        // Port for this 
        int port = 6666;

        //default IP
        readonly string ip_addres = "192.168.42.45";
        //My default IP
        string my_ip_addres = "10.128.121.154";

        EditText port_connect;
        EditText ip_connect;
        EditText name_connect;
        EditText password;
        Button button_connect;
        FloatingActionButton find;
        View  rootViewLogin;


        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            find = FindViewById<FloatingActionButton>(Resource.Id.find);
           rootViewLogin =  FindViewById<CoordinatorLayout>(Resource.Id.root_View);
            find.Click += FindOnClick;
            


            // Get my IP 
            MyIp = Dns.GetHostAddresses(Dns.GetHostName()).First();
            my_ip_addres = MyIp.ToString();
            MyId = ComunicationClass.CreateID(MyIp);

           
            button_connect = FindViewById<Button>(Resource.Id.button_connect);
            button_connect.Click += ConnectOnClick;
            
            

            port_connect = FindViewById<EditText>(Resource.Id.port_txt);
            ip_connect = FindViewById<EditText>(Resource.Id.ip_txt);
            name_connect = FindViewById<EditText>(Resource.Id.name_txt);
            password = FindViewById<EditText>(Resource.Id.password_txt);

            ip_connect.Hint = "default ip: " + ip_addres;
            port_connect.Hint = "default port: " + port.ToString();
            name_connect.Hint = "default name: Phone " + my_ip_addres;

            find = FindViewById<FloatingActionButton>(Resource.Id.find);
            find.Click += FindOnClick;

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        /// <summary>
        /// Get Ip address of server in LAN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void FindOnClick(object sender, EventArgs eventArgs)
        {

            
            //Create user withount server
            user = new User(null);           
            user.SetUserID(MyId);
            user.SendBroadCast(new IntroducingUser());
            //Wait for trying find server
            for (int i = 0; i < 3; i++)
            {
                System.Threading.Thread.Sleep(TimeConstant/3);
                if (user.Servers.Any())
                {
                    break;
                }
            }
            if (user.Servers.Any())
            {
                StringBuilder ipBuilder = new StringBuilder();
                var id = user.Servers.First();
                ipBuilder.Append(id.Parts[0]).Append(".");
                ipBuilder.Append(id.Parts[1]).Append(".");
                ipBuilder.Append(id.Parts[2]).Append(".");
                ipBuilder.Append(id.Parts[3]);
                ip_connect.Text = ipBuilder.ToString();
            }
            else
            {
                try
                {
                    
                    Snackbar.Make(rootViewLogin, "NO SERVER FOUND", Snackbar.LengthLong)
                    .SetAction("Close", v => { }).Show();
                }
                catch (Exception e) { }

            }
            user.Shutdown();
            user = null;
            
        }



        private void ConnectOnClick(object sender, EventArgs eventArgs)
        {

            if(user != null)
            {
                user.Shutdown();                
            } 
            isDemo = false;

            // get user port to connect
            if (!int.TryParse(port_connect.Text, out int port_to_connect))
                port_to_connect = port;

            ///for testing  adress my server pc
            IPAddress.TryParse(ip_connect.Text, out IPAddress s);
            if (s == null) // User addres is in correct - use default
                IPAddress.TryParse(ip_addres, out s); //for now my pc 

            
            user = new User(s);

            
            //MyId = CMD.CreateID(MyIp);
            user.SetUserID(MyId);
            user.SetServerID(ComunicationClass.CreateID(s));

            if (name_connect.Text != string.Empty)
                nameOfUser = "Phone " + name_connect.Text;
            else
                nameOfUser = "Phone " + my_ip_addres;

            user.Login(nameOfUser, password.Text);
            user.MessageToServer("User " + nameOfUser + " join this server.");

            //Wait for connection 
            for (int i = 0; i < 10; i++)          
            {
                System.Threading.Thread.Sleep(TimeConstant/10);
                if (user.ConnectedToServer)
                {
                    
                    var intent = new Intent(this, typeof(MenuActivity));
                    this.StartActivity(intent);
 
                    return;
                }
            }
           
            //Connection failed
            {
                // TAG DELETE
                bool DemoIsEnable = true;

                if (DemoIsEnable)
                {
                    isDemo = true;
                    var intent = new Intent(this, typeof(MenuActivity));
                    this.StartActivity(intent);

                    return;
                }
                try
                {
                    
                    Snackbar.Make(rootViewLogin, "NOT CONNECTED TO SERVER", Snackbar.LengthLong)
                    .SetAction("Close", v => { }).Show();

                    
                }
                catch (Exception e) { }
            }


            
            


        }
    }
}


