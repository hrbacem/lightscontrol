﻿namespace GUIUser
{
    partial class UserHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserHome));
            this.updateViewTimer = new System.Windows.Forms.Timer(this.components);
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.serverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipmentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCurrentGroupsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nicknamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadNicknamesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCurrentNicknamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNicknameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteNicknameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateViewTimer
            // 
            this.updateViewTimer.Interval = 50;
            this.updateViewTimer.Tick += new System.EventHandler(this.UpdateTimeLayout);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dockPanel1.Location = new System.Drawing.Point(0, 27);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.ShowDocumentIcon = true;
            this.dockPanel1.Size = new System.Drawing.Size(800, 420);
            this.dockPanel1.TabIndex = 4;
            this.dockPanel1.Theme = null;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serverToolStripMenuItem,
            this.equipmentsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // serverToolStripMenuItem
            // 
            this.serverToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informationToolStripMenuItem,
            this.changePasswordToolStripMenuItem});
            this.serverToolStripMenuItem.Name = "serverToolStripMenuItem";
            this.serverToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.serverToolStripMenuItem.Text = "Setting";
            // 
            // informationToolStripMenuItem
            // 
            this.informationToolStripMenuItem.Name = "informationToolStripMenuItem";
            this.informationToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.informationToolStripMenuItem.Text = "Information";
            this.informationToolStripMenuItem.Click += new System.EventHandler(this.informationToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // equipmentsToolStripMenuItem
            // 
            this.equipmentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.equipmentsToolStripMenuItem1,
            this.groupsToolStripMenuItem,
            this.nicknamesToolStripMenuItem,
            this.contextToolStripMenuItem});
            this.equipmentsToolStripMenuItem.Name = "equipmentsToolStripMenuItem";
            this.equipmentsToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.equipmentsToolStripMenuItem.Text = "Data";
            // 
            // equipmentsToolStripMenuItem1
            // 
            this.equipmentsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.addToolStripMenuItem});
            this.equipmentsToolStripMenuItem1.Name = "equipmentsToolStripMenuItem1";
            this.equipmentsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.equipmentsToolStripMenuItem1.Text = "Equipments";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.loadToolStripMenuItem.Text = "Load Equipments";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadEquipments);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.saveToolStripMenuItem.Text = "Save Equipments";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Enabled = false;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.addToolStripMenuItem.Text = "Add Equipments";
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadGroupToolStripMenuItem,
            this.saveCurrentGroupsToolStripMenuItem1,
            this.deleteGroupsToolStripMenuItem});
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.groupsToolStripMenuItem.Text = "Groups";
            // 
            // loadGroupToolStripMenuItem
            // 
            this.loadGroupToolStripMenuItem.Name = "loadGroupToolStripMenuItem";
            this.loadGroupToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadGroupToolStripMenuItem.Text = "Load group";
            this.loadGroupToolStripMenuItem.Click += new System.EventHandler(this.loadGroup);
            // 
            // saveCurrentGroupsToolStripMenuItem1
            // 
            this.saveCurrentGroupsToolStripMenuItem1.Name = "saveCurrentGroupsToolStripMenuItem1";
            this.saveCurrentGroupsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.saveCurrentGroupsToolStripMenuItem1.Text = "Save current groups";
            this.saveCurrentGroupsToolStripMenuItem1.Click += new System.EventHandler(this.saveGroups);
            // 
            // nicknamesToolStripMenuItem
            // 
            this.nicknamesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadNicknamesToolStripMenuItem1,
            this.saveCurrentNicknamesToolStripMenuItem,
            this.addNicknameToolStripMenuItem,
            this.deleteNicknameToolStripMenuItem});
            this.nicknamesToolStripMenuItem.Name = "nicknamesToolStripMenuItem";
            this.nicknamesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nicknamesToolStripMenuItem.Text = "Nicknames";
            // 
            // loadNicknamesToolStripMenuItem1
            // 
            this.loadNicknamesToolStripMenuItem1.Name = "loadNicknamesToolStripMenuItem1";
            this.loadNicknamesToolStripMenuItem1.Size = new System.Drawing.Size(201, 22);
            this.loadNicknamesToolStripMenuItem1.Text = "Load Nicknames";
            this.loadNicknamesToolStripMenuItem1.Click += new System.EventHandler(this.loadNicknames);
            // 
            // saveCurrentNicknamesToolStripMenuItem
            // 
            this.saveCurrentNicknamesToolStripMenuItem.Name = "saveCurrentNicknamesToolStripMenuItem";
            this.saveCurrentNicknamesToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.saveCurrentNicknamesToolStripMenuItem.Text = "Save current Nicknames";
            this.saveCurrentNicknamesToolStripMenuItem.Click += new System.EventHandler(this.saveNicknames);
            // 
            // addNicknameToolStripMenuItem
            // 
            this.addNicknameToolStripMenuItem.Name = "addNicknameToolStripMenuItem";
            this.addNicknameToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.addNicknameToolStripMenuItem.Text = "Add nicknames";
            this.addNicknameToolStripMenuItem.Click += new System.EventHandler(this.addNickname);
            // 
            // contextToolStripMenuItem
            // 
            this.contextToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadContextToolStripMenuItem,
            this.createNewContextToolStripMenuItem});
            this.contextToolStripMenuItem.Name = "contextToolStripMenuItem";
            this.contextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.contextToolStripMenuItem.Text = "Context";
            // 
            // loadContextToolStripMenuItem
            // 
            this.loadContextToolStripMenuItem.Name = "loadContextToolStripMenuItem";
            this.loadContextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadContextToolStripMenuItem.Text = "Load Context";
            this.loadContextToolStripMenuItem.Click += new System.EventHandler(this.loadContextFile);
            // 
            // createNewContextToolStripMenuItem
            // 
            this.createNewContextToolStripMenuItem.Name = "createNewContextToolStripMenuItem";
            this.createNewContextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.createNewContextToolStripMenuItem.Text = "Create New Context";
            this.createNewContextToolStripMenuItem.Click += new System.EventHandler(this.contexCreator);
            // 
            // deleteGroupsToolStripMenuItem
            // 
            this.deleteGroupsToolStripMenuItem.Name = "deleteGroupsToolStripMenuItem";
            this.deleteGroupsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteGroupsToolStripMenuItem.Text = "Delete groups";
            this.deleteGroupsToolStripMenuItem.Click += new System.EventHandler(this.deleteGroupsToolStripMenuItem_Click);
            // 
            // deleteNicknameToolStripMenuItem
            // 
            this.deleteNicknameToolStripMenuItem.Name = "deleteNicknameToolStripMenuItem";
            this.deleteNicknameToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.deleteNicknameToolStripMenuItem.Text = "Delete nickname";
            this.deleteNicknameToolStripMenuItem.Click += new System.EventHandler(this.deleteNicknameToolStripMenuItem_Click);
            // 
            // UserHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dockPanel1);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserHome";
            this.Text = "User";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer updateViewTimer;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem serverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipmentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipmentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveCurrentGroupsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nicknamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadNicknamesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveCurrentNicknamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createNewContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNicknameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteGroupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteNicknameToolStripMenuItem;
    }
}

