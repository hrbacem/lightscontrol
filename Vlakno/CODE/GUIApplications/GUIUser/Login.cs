﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIUser
{
    public partial class LoginForm : Form
    {

        public IPAddress IP { get { IPAddress ip; IPAddress.TryParse(ipAddress.Text,out ip); return ip; } }

        public string IPaddress { set { ipAddress.Text = value; } }
        public int Port { get { int p; int.TryParse(port.Text,out p); return p; } }

        public string UserName { get => name.Text; }

        public string Password { get => password.Text; }

        public System.EventHandler Finder { set { TryFindServer.Click += value; } } 

        public bool Wait { get=>wait; }
        public bool Log { get=>log; }

        bool wait = true;
        bool log = false;

        LoginData login;

        public LoginForm(LoginData login)
        {
            InitializeComponent();
            this.login = login;

        }

       
        
        /// <summary>
        /// Fill information 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginButton_Click(object sender, EventArgs e)
        {
            IPAddress ip;
            if(!IPAddress.TryParse(ipAddress.Text, out ip))
            {
                MessageBox.Show("Not valid IP address!");
                return;
            }
            if(!int.TryParse(port.Text, out login.Port))
            {
                
                MessageBox.Show("Not valid port!");
                return;
            }
            else
            {
                if (login.Port > 65535 || login.Port < 0)
                {
                    MessageBox.Show("Not valid port!");
                    return;
                }
                    
            }

            try
            {
                IPEndPoint ipTest = new IPEndPoint(ip, Port);
                Socket testSocket = new Socket(ipTest.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                testSocket.Connect(ipTest);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Not valid connection! \n"+ex.ToString());
                return;
            }

            login.IP = ip;
            login.Name = name.Text;
            int.TryParse(port.Text,out login.Port);
            login.Password = password.Text;
            login.Login = true;

            this.Close();
        }

        /// <summary>
        /// Mark login as filled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            login.Filled = true;

        }

    }
}
