﻿using Lights.Comunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIUser
{
    /// <summary>
    /// Basic data to start client
    /// </summary>
    public class LoginData
    {
        public string Name;
        public IPAddress IP;
        public int Port ;
        public string Password;
        public bool Filled = false;
        public bool Login = false;
    }

    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            /// loging container
            LoginData log = new LoginData();    
            
            /// Loggin for client
            var login = new LoginForm(log);

            /// Finder of  server 
            login.Finder = (s, e) =>
            {
                
                Cursor.Current = Cursors.WaitCursor;
                {

                   

                                       
                        if (login.Port > 65535 || login.Port < 0)
                        {
                            MessageBox.Show("Not valid port!");
                            return;
                        }

                        var id = Client.GetServerID(login.Port);
                        /// Set Ip of loger 
                        if (id!=null)
                    {

                        StringBuilder ipBuilder = new StringBuilder();
                        ipBuilder.Append(id.Parts[0]).Append(".");
                        ipBuilder.Append(id.Parts[1]).Append(".");
                        ipBuilder.Append(id.Parts[2]).Append(".");
                        ipBuilder.Append(id.Parts[3]);
                        login.IPaddress = ipBuilder.ToString();
                    }
                    else
                    {
                        //Find server faild
                        MessageBox.Show("NO SERVER FOUND");
                    }
                   
                    Cursor.Current = Cursors.Default;
                }
            };


            Application.Run(login);

            /// Check if client is going to log
            if (log.Login)
            {
                ///set correct client
                Client client = client = new Client(log.IP, log.Port);
                
                {
                    client.Login(log.Name, log.Password);
                    /// Try wait for confirm of login
                    const int TimeConstant = 5000;
                    if (!client.ConnectedToServer)
                        for (int i = 0; i < 10; i++)
                        {
                            System.Threading.Thread.Sleep(TimeConstant/10);
                            if (client.ConnectedToServer)
                                break;
                            client.Login(log.Name, log.Password);
                        }


                    
                    if (client.ConnectedToServer )
                    {
                        //succesfully connect to server 
                        Application.Run(new UserHome(client));
                    }
                    else
                    {
                       
                       
                        switch (MessageBox.Show("Create console app anyway?", "The connection to the server failed.", MessageBoxButtons.YesNo))
                        {
                            case DialogResult.Yes:
                                Application.Run(new UserHome(client));
                                break;
                            default:
                                break;
                        }

                    }
                    client.Shutdown();
                }
            }
        }

        /// <summary>
        /// For Console start
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
    }
}
