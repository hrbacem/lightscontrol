﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GuiComponents;
using Lights.Transport;
using Lights;
using WeifenLuo.WinFormsUI.Docking;
using Lights.Messages;
using GUIServer;
using Lights.Comunication;
using Lights.Protocols;
using RealTimeControl;
using Lights.Parsers;

namespace GUIUser
{
    public partial class UserHome : Form
    {
        Client client;

        Messenger chat;
        Messenger errors;
        RealTimeSetting RealTimeSetter;
        MenuFunctions menuSupport;
        string applicationName; 

        public UserHome(Client client)
        {

            this.client = client;
           
            InitializeComponent();
            updateViewTimer.Interval = 500;
            /// Create docking system
            this.dockPanel1.Theme = new VS2015BlueTheme();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            dockPanel1.DocumentStyle = DocumentStyle.DockingWindow;
            dockPanel1.AllowEndUserDocking = true;
            dockPanel1.AllowEndUserNestedDocking = true;
            this.Controls.Add(dockPanel1);
            
            
            /// Chat setting
            {
                chat = new Messenger(this.client.Conversation, msg => { client.MessageToServer(msg); });
                chat.Show(dockPanel1, DockState.DockRightAutoHide);
                chat.Name = "Chat";
                chat.Text = "Chat";
            }

            /// Error setting
            {
                errors = new Messenger(this.client.Errors, msg => { errors.addToDataList("User " + msg); client.Send(new Error(msg)); });
                errors.Show(dockPanel1, DockState.DockRightAutoHide);
                errors.Name = "Information";
                errors.Text = "Information";
            }
           
            
            /// Data
            {
                RealTimeSetter = new RealTimeSetting(client.Data,client.Aliases, (s) => { client.SendUpdateToServer(); })
                {
                    AutoScaleMode = AutoScaleMode.Font,
                    Text = "Data sended to equipments"        
                };
                RealTimeSetter.actionOnUserFormClosing += (e) =>
                 {
                    // Confirm user wants to close
                    switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
                     {
                         case DialogResult.No:
                             e.Cancel = true;
                             break;
                         default:
                             break;
                     }
                 };
                RealTimeSetter.Show(dockPanel1, DockState.Document);
                        
            }
            menuSupport = new MenuFunctions(client, dockPanel1);
            applicationName = this.Text;
            updateViewTimer.Start();
        }

        private void UpdateTimeLayout(object sender, EventArgs e)
        {
            if (client.ConnectedToServer)
                this.Text = applicationName;
            else
                this.Text = applicationName+" - Not connected!";
            
            if (!client.DataCFO) return;
            this.chat.RefreshText();
            this.errors.RefreshText();
            this.RealTimeSetter.DataWasChanged?.Invoke(sender);
            menuSupport.Reload();
            
                
        }

        
        private void informationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nComunication Port: " + client.Port);
            sb.Append("\nUser ID: " + client.ClientID.ToString());
            sb.Append("\nServer ID: " + client.ServerID.ToString());
            MessageBox.Show(sb.ToString());
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var setter = new StringSetterForm();
            setter.Text = "CHANGE PASSWORD";
            setter.setButton.Click += (send, ev) => { client.Login(client.lastUsedName, setter.filledText.Text); setter.Close(); };
            setter.Show();
        }

        private void loadGroup(object sender, EventArgs e)
        {
            menuSupport.LoadGroups(sender, e);
        }

        private void saveGroups(object sender, EventArgs e)
        {
            menuSupport.SaveGroups(sender, e);
        }

        private void loadNicknames(object sender, EventArgs e)
        {
            menuSupport.LoadNicknames(sender, e);
        }

        private void saveNicknames(object sender, EventArgs e)
        {
            menuSupport.SaveNicknames(sender, e);
        }

      
        private void loadEquipments(object sender, EventArgs e)
        {
            if (FileManager.loadExistFileName(out string fileName))
            {
                if (!LoadEquipment(fileName))
                {
                    MessageBox.Show("Equipments not loaded!");
                }
            }
        }

        private bool LoadEquipment(string filename) {  

            if (
            DefultExtractorMap.TryReadProtocolData(filename, out Dictionary<IID, IProtocol> map, out Dictionary<IID, IState> vals)
            )
            {


                lock (client.Data)
                {
                    foreach (var item in vals)
                    {
                        if (!client.Data.ContainsKey(item.Key))
                            client.Data.Add(item.Key, item.Value);
                    }

                }
                client.DataCFO = true;
                client.SendUpdateToServer();
                return true;
            }
            return false;
        }

        private void contexCreator(object sender, EventArgs e)
        {
            menuSupport.NewContext(sender, e);
        }

        private void loadContextFile(object sender, EventArgs e)
        {
            if (FileManager.loadExistFileName(out string fileName))
            {
                if (contextLoader(fileName))
                    return;
                else
                    MessageBox.Show("Not context file!");
            }
        }

        private bool contextLoader(string filename)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            Context contextToLoad = new Context();

            if (!contextToLoad.LoadContext(filename))
            {
                Cursor.Current = cursor;
                return false;
            }
            if (contextToLoad.Nicknames == null) return false;
            if (contextToLoad.Groups == null) return false;
            if (contextToLoad.Equipments == null) return false;
            foreach (var item in contextToLoad.Nicknames)
            {
                client.Aliases.LoadNickNames(item);
            }

            foreach (var item in contextToLoad.Groups)
            {
                client.Aliases.LoadGroups(item);
            }
            foreach (var item in contextToLoad.Equipments)
            {
                LoadEquipment(item);

            }

            client.Aliases.AliasesChanged?.Invoke();

            client.DataCFO = true;
            Cursor.Current = cursor;
            client.DataCFO = true;
            client.Aliases.AliasesChanged?.Invoke();
            return true;
        }

        private void addNickname(object sender, EventArgs e)
        {
            menuSupport.AddNickname(sender, e);
        }

       

        private void deleteGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            menuSupport.GroupsDeleter(sender, e);
        }

        private void deleteNicknameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            menuSupport.NicknamesDeleter(sender, e);
        }
    }
}
