﻿using System.Windows.Forms;

namespace GUIServer
{
    partial class GUIServerForm
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.serverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipmentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCurrentGroupsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nicknamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadNicknamesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCurrentNicknamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNicknameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.UpdateTimeLayout);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dockPanel1.Location = new System.Drawing.Point(0, 27);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.ShowDocumentIcon = true;
            this.dockPanel1.Size = new System.Drawing.Size(800, 420);
            this.dockPanel1.TabIndex = 3;
            this.dockPanel1.Theme = null;
            this.dockPanel1.ActiveContentChanged += new System.EventHandler(this.DockPanel1_ActiveContentChanged_1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serverToolStripMenuItem,
            this.equipmentsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked_1);
            // 
            // serverToolStripMenuItem
            // 
            this.serverToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informationToolStripMenuItem,
            this.changePasswordToolStripMenuItem});
            this.serverToolStripMenuItem.Name = "serverToolStripMenuItem";
            this.serverToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.serverToolStripMenuItem.Text = "User Setting";
            // 
            // informationToolStripMenuItem
            // 
            this.informationToolStripMenuItem.Name = "informationToolStripMenuItem";
            this.informationToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.informationToolStripMenuItem.Text = "Information";
            this.informationToolStripMenuItem.Click += new System.EventHandler(this.informationToolStripMenuItem_Click_1);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // equipmentsToolStripMenuItem
            // 
            this.equipmentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.equipmentsToolStripMenuItem1,
            this.groupsToolStripMenuItem,
            this.nicknamesToolStripMenuItem,
            this.contextToolStripMenuItem});
            this.equipmentsToolStripMenuItem.Name = "equipmentsToolStripMenuItem";
            this.equipmentsToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.equipmentsToolStripMenuItem.Text = "Equipments";
            // 
            // equipmentsToolStripMenuItem1
            // 
            this.equipmentsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.addToolStripMenuItem});
            this.equipmentsToolStripMenuItem1.Name = "equipmentsToolStripMenuItem1";
            this.equipmentsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.equipmentsToolStripMenuItem1.Text = "Equipments";
            this.equipmentsToolStripMenuItem1.Click += new System.EventHandler(this.equipmentsToolStripMenuItem1_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadToolStripMenuItem.Text = "Load Equipments";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadEquipments);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save Equipments";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveMap);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addToolStripMenuItem.Text = "Add Equipments";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadGroupToolStripMenuItem,
            this.saveCurrentGroupsToolStripMenuItem1});
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.groupsToolStripMenuItem.Text = "Groups";
            // 
            // loadGroupToolStripMenuItem
            // 
            this.loadGroupToolStripMenuItem.Name = "loadGroupToolStripMenuItem";
            this.loadGroupToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.loadGroupToolStripMenuItem.Text = "Load group";
            this.loadGroupToolStripMenuItem.Click += new System.EventHandler(this.loadGroupMenu);
            // 
            // saveCurrentGroupsToolStripMenuItem1
            // 
            this.saveCurrentGroupsToolStripMenuItem1.Name = "saveCurrentGroupsToolStripMenuItem1";
            this.saveCurrentGroupsToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.saveCurrentGroupsToolStripMenuItem1.Text = "Save current groups";
            this.saveCurrentGroupsToolStripMenuItem1.Click += new System.EventHandler(this.saveCurrentGroupsMenu);
            // 
            // nicknamesToolStripMenuItem
            // 
            this.nicknamesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadNicknamesToolStripMenuItem1,
            this.addNicknameToolStripMenuItem,
            this.saveCurrentNicknamesToolStripMenuItem});
            this.nicknamesToolStripMenuItem.Name = "nicknamesToolStripMenuItem";
            this.nicknamesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nicknamesToolStripMenuItem.Text = "Nicknames";
            this.nicknamesToolStripMenuItem.Click += new System.EventHandler(this.nicknamesToolStripMenuItem_Click);
            // 
            // loadNicknamesToolStripMenuItem1
            // 
            this.loadNicknamesToolStripMenuItem1.Name = "loadNicknamesToolStripMenuItem1";
            this.loadNicknamesToolStripMenuItem1.Size = new System.Drawing.Size(201, 22);
            this.loadNicknamesToolStripMenuItem1.Text = "Load Nicknames";
            this.loadNicknamesToolStripMenuItem1.Click += new System.EventHandler(this.loadNicknamesMenu);
            // 
            // saveCurrentNicknamesToolStripMenuItem
            // 
            this.saveCurrentNicknamesToolStripMenuItem.Name = "saveCurrentNicknamesToolStripMenuItem";
            this.saveCurrentNicknamesToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.saveCurrentNicknamesToolStripMenuItem.Text = "Save current Nicknames";
            this.saveCurrentNicknamesToolStripMenuItem.Click += new System.EventHandler(this.saveCurrentNicknamesMenu);
            // 
            // contextToolStripMenuItem
            // 
            this.contextToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadContextToolStripMenuItem,
            this.createNewContextToolStripMenuItem});
            this.contextToolStripMenuItem.Name = "contextToolStripMenuItem";
            this.contextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.contextToolStripMenuItem.Text = "Context";
            // 
            // loadContextToolStripMenuItem
            // 
            this.loadContextToolStripMenuItem.Name = "loadContextToolStripMenuItem";
            this.loadContextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadContextToolStripMenuItem.Text = "Load Context";
            this.loadContextToolStripMenuItem.Click += new System.EventHandler(this.loadContextFile);
            // 
            // createNewContextToolStripMenuItem
            // 
            this.createNewContextToolStripMenuItem.Name = "createNewContextToolStripMenuItem";
            this.createNewContextToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.createNewContextToolStripMenuItem.Text = "Create New Context";
            this.createNewContextToolStripMenuItem.Click += new System.EventHandler(this.createNewContext);
            // 
            // addNicknameToolStripMenuItem
            // 
            this.addNicknameToolStripMenuItem.Name = "addNicknameToolStripMenuItem";
            this.addNicknameToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.addNicknameToolStripMenuItem.Text = "Add Nickname";
            this.addNicknameToolStripMenuItem.Click += new System.EventHandler(this.addNicknameMenu);
            // 
            // GUIServerForm
            // 
            this.AccessibleDescription = "m";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dockPanel1);
            this.IsMdiContainer = true;
            this.Name = "GUIServerForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Server";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem serverToolStripMenuItem;
        private ToolStripMenuItem informationToolStripMenuItem;
        private ToolStripMenuItem equipmentsToolStripMenuItem;
        private ToolStripMenuItem equipmentsToolStripMenuItem1;
        private ToolStripMenuItem loadToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem addToolStripMenuItem;
        private ToolStripMenuItem groupsToolStripMenuItem;
        private ToolStripMenuItem loadGroupToolStripMenuItem;
        private ToolStripMenuItem saveCurrentGroupsToolStripMenuItem1;
        private ToolStripMenuItem nicknamesToolStripMenuItem;
        private ToolStripMenuItem loadNicknamesToolStripMenuItem1;
        private ToolStripMenuItem saveCurrentNicknamesToolStripMenuItem;
        private ToolStripMenuItem contextToolStripMenuItem;
        private ToolStripMenuItem loadContextToolStripMenuItem;
        private ToolStripMenuItem createNewContextToolStripMenuItem;
        private ToolStripMenuItem changePasswordToolStripMenuItem;
        private ToolStripMenuItem addNicknameToolStripMenuItem;
    }
}

