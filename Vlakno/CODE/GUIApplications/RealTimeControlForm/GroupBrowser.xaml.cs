﻿using Lights.Parsers;
using Lights.EquipmentsKinds;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for GroupBroswer.xaml
    /// </summary>
    public partial class GroupBrowser : UserControl
    {

        public ControlDockPanel Layout { get; set; }
        public RepresentStateFactory RepresentationFactory { get; set; }
        public Dictionary<IID, IState> Data { get => data; set { data = value; } }
        Dictionary<IID, IState> data;

        Aliases Aliases { get => aliases; set { aliases = value; aliases.AliasesChanged?.Invoke(); } }
        Aliases aliases;

        public GroupBrowser()
        {
            InitializeComponent();
            addToCurrent = new Dictionary<CheckBox, GroupState>();

        }
        string NameOfFile { get; set; }


        Dictionary<CheckBox, GroupState> addToCurrent;



        private void SelecAllMembers(object sender, RoutedEventArgs e)
        {
            foreach (var item in addToCurrent)
            {
                item.Key.IsChecked = true;
            }
        }

        private void ClearMembers(object sender, RoutedEventArgs e)
        {
            foreach (var item in addToCurrent)
            {
                item.Key.IsChecked = false;
            }
        }

        public void updateMembers()
        {
            addToCurrent.Clear();
            listView.Items.Clear();
            if (Aliases != null)
                foreach (var pair in Aliases.GroupMembers)
                {
                    var item = pair.Value;
                    CheckBox nameOfGroup = new CheckBox();
                    nameOfGroup.Content = item.Name;
                    addToCurrent.Add(nameOfGroup, item);


                    ComboBox members = new ComboBox();
                    members.Text = "MEMBERS";
                    members.IsDropDownOpen = false;
                    members.IsEditable = true;

                    foreach (var member in item.Members)
                    {
                        Label label = new Label();
                        label.Content = member.name;
                        label.FontSize = 20;
                        members.Items.Add(label);
                    }
                    listView.Items.Add(nameOfGroup);
                    listView.Items.Add(members);

                }
        }

        private void UseGroup(object sender, RoutedEventArgs e)
        {
            foreach (var item in addToCurrent)
            {
                if ((bool)item.Key.IsChecked)
                {
                    CreateGroup(item.Value);
                }
            }
            Aliases.AliasesChanged?.Invoke();
        }

        public void SetAliases(Aliases aliases)
        {

            Aliases = aliases;
            if (aliases == null) return;
            Aliases.AliasesChanged += this.updateMembers;

        }
        private void SaveGroupsFile(object sender, RoutedEventArgs e)
        {
            var opFile = new System.Windows.Forms.OpenFileDialog();
            opFile.ShowDialog();
            if (opFile.FileName == "") return;
            if (!opFile.CheckFileExists) return;
            NameOfFile = opFile.FileName;

            Aliases.SaveGroups(NameOfFile);

        }

        private void LoadFile(object sender, RoutedEventArgs e)
        {
            var opFile = new System.Windows.Forms.OpenFileDialog();
            opFile.ShowDialog();
            if (opFile.FileName == "") return;
            if (!opFile.CheckFileExists) return;

            addToCurrent.Clear();
            NameOfFile = opFile.FileName;
            listView.Items.Clear();
            Aliases.LoadGroups(NameOfFile);

            Aliases.AliasesChanged?.Invoke();

        }




        private void CreateGroup(GroupState group)
        {
            List<string> members = new List<string>();

            IEquipmentControl controll = null;

            RepresentStateFactory generator = RepresentationFactory;

            Dictionary<GroupState.Equipment, IState> toChange = new Dictionary<GroupState.Equipment, IState>();
            foreach (var item in group.Members)
            {
                if (!Data.ContainsKey(item.id)) continue;
                var state = Data[item.id];
                toChange.Add(item, state);
                members.Add(item.name);
            }
            if (!toChange.Any()) return;
            SupportedGroups kindOfGroup = SupportedGroups.SLIDER;
            if (group.TypeOfGroup == typeof(RGBLight)) kindOfGroup = SupportedGroups.RGB;
            if (group.TypeOfGroup == typeof(SpyderLight)) kindOfGroup = SupportedGroups.AXIS;

            /// Create control and set behavior
            switch (kindOfGroup)
            {


                case
                    SupportedGroups.RGB:


                    {
                        State<RGBLight> stateForFroup = (State<RGBLight>)KindSerialization.GetState(ImplementedKinds.RGBLight,new ID(true));

                        controll = generator.GetUserControlRGB(stateForFroup, Layout);
                        controll.ControlChangedData += (s) =>
                        {

                            byte R = ((IColorControl)(controll).getIControl).R;
                            byte G = ((IColorControl)(controll).getIControl).G;
                            byte B = ((IColorControl)(controll).getIControl).B;

                            foreach (var item in toChange)
                            {
                                item.Value.Parts.Data[0] = R;
                                item.Value.Parts.Data[1] = G;
                                item.Value.Parts.Data[2] = B;

                            }

                        };
                    }


                    break;
                case
                        SupportedGroups.AXIS:
                    {
                        State<SpyderLight> stateForGroup = (State<SpyderLight>)KindSerialization.GetState(ImplementedKinds.SpyderLight,new ID(true));
                        controll = generator.GetUserControlAXIS((State<SpyderLight>)toChange.First().Value, Layout);
                        controll.ControlChangedData += (s) =>
                        {
                            byte valueX = (byte)(((WpfJoystick)((EquipmentControlAxis)controll).getIControl).getX() + 90);
                            byte valueY = (byte)(((WpfJoystick)((EquipmentControlAxis)controll).getIControl).getY() + 90);
                            foreach (var item in toChange)
                            {
                                item.Value.Parts.Data[3] = valueX;
                                item.Value.Parts.Data[4] = valueY;
                            }

                        };
                    }

                    break;

                default:
                case SupportedGroups.SLIDER:

                    {

                        controll = generator.GetUserControlSlider(toChange.First().Value, Layout, toChange.First().Key.offset);

                        controll.ControlChangedData += (s) => {
                            byte value = (byte)((WideSlider)((EquipmentControlSlider)controll).getIControl).Value;
                            foreach (var item in toChange)
                            {
                                double koeficient = ((
                                (double)item.Key.level) / 100);
                                if (item.Key.IsInverse)
                                {
                                    if (value > 180 && item.Key.offset > 2 && item.Value.GetType() == typeof(State<SpyderLight>))
                                        item.Value.Parts.Data[item.Key.offset] = (byte)(180 - 180 * koeficient);
                                    else
                                        item.Value.Parts.Data[item.Key.offset] = (byte)(255 - value * koeficient);

                                }
                                else
                                {
                                    if (value > 180 && item.Key.offset > 2 && item.Value.GetType() == typeof(State<SpyderLight>))
                                        item.Value.Parts.Data[item.Key.offset] = (byte)(180 * koeficient);
                                    else
                                        item.Value.Parts.Data[item.Key.offset] = (byte)(value * koeficient);

                                }
                            }

                        };
                    }
                    break;

            }

            ///Create label
            controll.groupControl = group;
            var header = controll.getIHeaderControl;
            header.NameLabel = group.Name;

            header.SetGroup(members);
        }

    }

}

