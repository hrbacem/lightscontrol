﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for PositionControl.xaml
    /// </summary>
    public partial class PositionControl : UserControl,IPositionControl
    {
        public UserAction MoveRight { get; set; }
        public UserAction MoveLeft { get; set; }
        public UserAction SetMax { get; set; }
        public UserAction SetMin { get; set; }

        public double ControlHeight { get => this.Height; set { this.Height = value; } }
        public double ControlWidth { get => this.Width; set { this.Width = value; } }

        public string SetMinLabel { get { return this.MinButton.Content.ToString(); } set { this.MinButton.Content = value; } }
        public string SetMaxLabel { get { return this.MaxButton.Content.ToString(); } set { this.MaxButton.Content = value; } }

        public PositionControl()
        {
            InitializeComponent();
        }

        private void Button_ClickMax(object sender, RoutedEventArgs e)
        {
            SetMax?.Invoke(sender);
        }

        private void Button_ClickMin(object sender, RoutedEventArgs e)
        {
            SetMin?.Invoke(sender);
        }

        private void Button_ClickLeft(object sender, RoutedEventArgs e)
        {
            MoveLeft?.Invoke(sender);
        }

        private void Button_ClickRight(object sender, RoutedEventArgs e)
        {
            MoveRight?.Invoke(sender);
        }
    }
}
