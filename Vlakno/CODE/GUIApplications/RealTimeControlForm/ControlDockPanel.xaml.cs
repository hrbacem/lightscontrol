﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for ControllersRealTime.xaml
    /// </summary>
    public partial class ControlDockPanel  : UserControl
    {
        public ControlDockPanel()
        {
            InitializeComponent();
        }

        public bool ScrollBarVisible { get => ScrollBar.Visibility == Visibility.Visible; }

        public void AddControl(UserControl userControl)
        {
            SetMargins(userControl);
            this.dockPanel.Children.Add(userControl);
        }
        public void AddControlFirst(UserControl userControl)
        {
            SetMargins(userControl);
            this.dockPanel.Children.Insert(0,userControl);
        }

        private void SetMargins(UserControl control, int size = 3)
        {
            Thickness margin = control.Margin;
            int marginSize = size;
            margin.Top = marginSize;
            margin.Bottom = marginSize;
            margin.Right = marginSize;
            margin.Left = marginSize;
            control.Margin = margin;
        }

        public StackPanel Controls {get=>this.dockPanel; }

        
        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double controlsHeight = this.Controls.Height;

            if (controlsHeight < 150) return; // 150 is Size of HeaderController and Position

            if (controlsHeight-ScrollBar.Height - Controls.Height >150) //Check ScrollViewer  
                controlsHeight -= ScrollBar.Height - Controls.Height;

                foreach (System.Windows.Controls.UserControl item in Controls.Children)
                {
                   item.Height = (int)controlsHeight;

                    if (item is IEquipmentControl)
                    {
                    ((IEquipmentControl)item).ControlHeight = controlsHeight;

                    }

                }
        }
    }
}
