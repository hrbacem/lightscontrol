﻿using Lights.Parsers;
using Lights.EquipmentsKinds;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for MainLightControl.xaml
    /// </summary>
    public partial class MainLightControl : UserControl
    {
        public Dictionary<IID, IState> data { get => Data; set { Data = value; NewGroupManager.Data = Data;  UpdateMainControls(); FileGroupManager.Data = Data; } }
        Dictionary<IID, IState> Data;
        
        public ControlDockPanel Layout { get => layout; set { layout = value; NewGroupManager.Layout = value; FileGroupManager.Layout = value; } }
        ControlDockPanel layout;

        public RepresentStateFactory RepresentationFactory { get => representationFactory; set { representationFactory = value; NewGroupManager.RepresentationFactory = value; FileGroupManager.RepresentationFactory = value; } }
        RepresentStateFactory representationFactory;

         public Aliases Aliases { get => aliases; set { aliases = value; FileGroupManager.SetAliases(value); NewGroupManager.SetAliases(value); } }
         Aliases aliases;

        EquipmentControlAxis AllAxis;
        List<IState> axisControl = null;
        EquipmentControlColor AllColorControl;
        List<IState> colorControl=null;

        EquipmentControlSlider AllWhiteLights;
        List<IState> whiteLightControl = null;
        EquipmentControlSlider AllLights;
        public MainLightControl()
        {
            InitializeComponent();
          
            if (RepresentationFactory == null) RepresentationFactory = new RepresentStateFactory();

        }
      
        

        public void UpdateMainControls()
        {
            
            UpdateMainControlAxis();
            UpdateMainControlColor();
            UpdateMainControlWhiteLight();
            UpdateMainControlLight();
  
        }

        private void UpdateMainControlAxis()
        {
            if (AllAxis == null)
                CreateMainControlAxis();
            else
            {
                axisControl.Clear();
                foreach (var item in Data)
                {
                    if (item.Value.GetType() == typeof(State<SpyderLight>))
                        axisControl.Add(item.Value);
                }
                if (axisControl.Any())
                {
                    List<string> members = new List<string>();
                    foreach (var item in axisControl)
                    {
                        members.Add(item.Id.ToString());

                    }
                    
                    
                    AllAxis.Header.SetGroup(membersNames(axisControl));
                }
                
            }
        }

        void CreateMainControlAxis()
        {
            axisControl = new List<IState>();
            foreach (var item in Data)
            {
                if (item.Value.GetType() == typeof(State<SpyderLight>))
                    axisControl.Add(item.Value);
            }
            if (!axisControl.Any()) return;
            AllAxis = (EquipmentControlAxis)representationFactory.GetUserControlAXIS((State<SpyderLight>)axisControl[0], GlobalControls);

            
            AllAxis.ControlChangedData += (s) =>
            {
                byte valueX = (byte)(((WpfJoystick)AllAxis.getIControl).getX() + 90);
                byte valueY = (byte)(((WpfJoystick)AllAxis.getIControl).getY() + 90);
                foreach (var item in axisControl)
                {
                    item.Parts.Data[3] = valueX;
                    item.Parts.Data[4] = valueY;
                }
                RepresentationFactory.ControlChangedData(s);
            };


            List<string> members = new List<string>();
            foreach (var item in axisControl)
            {
                members.Add(item.Id.ToString());

            }
            AllAxis.Header.NickName.Content = "ALL AXIS";
            AllAxis.Header.SetGroup(membersNames(axisControl));
        }

        void CreateMainControlColor()
        {
            colorControl = new List<IState>();
            foreach (var item in Data)
            {
                if (item.Value.GetType() == typeof(State<RGBLight>) || item.Value.GetType() == typeof(State<SpyderLight>))
                    colorControl.Add(item.Value);
            }
           
            if (!colorControl.Any() ) return;
     

            AllColorControl = (EquipmentControlColor)representationFactory.GetUserControlRGB(colorControl[0], GlobalControls);

            
            AllColorControl.ControlChangedData += (s) =>
            {
                byte R = ((IColorControl)AllColorControl.getIControl).R;
                byte G = ((IColorControl)AllColorControl.getIControl).G;
                byte B = ((IColorControl)AllColorControl.getIControl).B;

                foreach (var item in colorControl)
                {
                    item.Parts.Data[0] = R;
                    item.Parts.Data[1] = G;
                    item.Parts.Data[2] = B;
                }
                RepresentationFactory.ControlChangedData(s);
            };


            AllColorControl.Header.NickName.Content = "ALL COLOR";
            AllColorControl.Header.SetGroup(membersNames(colorControl));
        }


        void CreateMainControlWhiteLight()
        {
            whiteLightControl = new List<IState>();
            foreach (var item in Data)
            {
                if (item.Value.GetType() == typeof(State<DIMMLight>))
                    whiteLightControl.Add(item.Value);
            }
            if (!whiteLightControl.Any()) return;
            
            AllWhiteLights =  (EquipmentControlSlider)representationFactory.GetUserControlSlider(whiteLightControl.First(), GlobalControls);
            

            AllWhiteLights.ControlChangedData += (s) =>
            {
                byte value = (byte)((WideSlider)AllWhiteLights.getIControl).Value;


                foreach (var item in whiteLightControl)
                {
                    for (int i = 0; i < item.Parts.Data.Length; i++)
                    {
                        item.Parts.Data[i] = value;
                    }

                }
                RepresentationFactory.ControlChangedData(s);
            };

            
            AllWhiteLights.Header.NickName.Content = "ALL WHITE LIGHTS";
            AllWhiteLights.Header.SetGroup(membersNames(whiteLightControl));
        }

        
        void CreateMainControlLight()
        {

            if (!Data.Any()) return;
            AllLights = (EquipmentControlSlider)representationFactory.GetUserControlSlider(Data.First().Value,GlobalControls, 0);
           
            

            AllLights.ControlChangedData += (s) =>
            {

                byte value = (byte)((WideSlider)AllLights.getIControl).Value;
                foreach (var item in Data)
                {
                    int numb = item.Value.Parts.Data.Length;
                    if (item.Value.GetType() == typeof(State<SpyderLight>)) numb -= 2;
                    for (int i = 0; i < numb; i++)
                    {
                        item.Value.Parts.Data[i] = value;
                    }

                }
                RepresentationFactory.ControlChangedData(s);
            };


            List<string> members = new List<string>();
            foreach (var item in Data)
            {
                members.Add(item.Value.Id.ToString());
            }
            AllLights.Header.NickName.Content = "ALL LIGHTS";
            AllLights.Header.SetGroup(members);
        }

        void UpdateMainControlLight()
        {
            if (AllLights == null)
            {
                CreateMainControlLight();
                return;

            }
            if (!Data.Any()) return;
           
            List<string> members = new List<string>();
            foreach (var item in Data)
            {
                members.Add(item.Value.Id.ToString());
            } 
           
            AllLights.Header.SetGroup(members);
        }


        void UpdateMainControlColor()
        {
            if (AllColorControl == null)
            {
                CreateMainControlColor();
            }
            else
            {
                colorControl.Clear();
                foreach (var item in Data)
                {
                    if (item.Value.GetType() == typeof(State<RGBLight>) || item.Value.GetType() == typeof(State<SpyderLight>))
                        colorControl.Add(item.Value);
                }

                
                AllColorControl.Header.SetGroup(membersNames(colorControl));

            }
        }


        void UpdateMainControlWhiteLight()
        {
            if (AllWhiteLights == null)
            {
                CreateMainControlWhiteLight();
                return;

            }
            whiteLightControl.Clear();
            foreach (var item in Data)
            {
                if (item.Value.GetType() == typeof(State<DIMMLight>))
                    whiteLightControl.Add(item.Value);
            }

            AllWhiteLights.Header.SetGroup(membersNames(whiteLightControl));
        }

        
        private List<string> membersNames(List<IState> states)
        {
            List<string> members = new List<string>();
            foreach (var item in states)
            {
                members.Add(item.Id.ToString());
            }
            return members;
        }

    }
}
