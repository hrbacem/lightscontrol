﻿using Lights.Parsers;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for EquipmentControl.xaml
    /// </summary>
    public partial class EquipmentControlColor : UserControl, IEquipmentControl
    {

        public UserAction ControlChangedData { get => controlChangedData; set { controlChangedData += value; MainControl.ControlChangedData += value; Header.ControlChangedData += value; } }
        UserAction controlChangedData;
        public UserAction DataWasChange { get; set; }
        
        public IControl getIControl => this.MainControl;

        public IHeaderControl getIHeaderControl => this.Header;

        public IPositionControl getIPositionControl => this.Position;

        IState state;

        public double ControlWidth { get => this.Width; set { this.Width = value; } }
        public double ControlHeight
        {
            get => this.Height;
            set
            {
                this.Height = value;
                double Height = value - getIHeaderControl.ControlHeight - getIPositionControl.ControlHeight;
                getIControl.ControlHeight = Height;

                MainControl.Height = Height;

            }
        }

        public int StatePosition { get; internal set; }

        public EquipmentControlColor(IState st, int position = 0)
        {
            DataWasChange += Reload;

            StatePosition = position;
            InitializeComponent();
            this.state = st;
            MainControl.StateOfEquipment = st;
            Header.setPriority(state);

            MainControl.SetColor(st.Parts.Data[0], st.Parts.Data[1], st.Parts.Data[2]);
            
            

            Position.SetMax +=
                (s) =>
                {
                    MainControl.SetColor(255, 255, 255);
                    st.SetImportanceAll(true, 0, 3);
                    MainControl.ControlChangedData?.Invoke(this);
                };

            ControlChangedData += (s) =>
            {
                
                state.SetValue(0, MainControl.R);
                state.SetValue(1, MainControl.G);
                state.SetValue(2, MainControl.B);                
            };
            flipR.Click += (s, e) =>
            {
                MainControl.R = (byte)(255 - MainControl.R);
                MainControl.ControlChangedData?.Invoke(s);
            };

            flipG.Click += (s, e) =>
            {
                MainControl.G = (byte)(255 - MainControl.G);
                MainControl.ControlChangedData?.Invoke(s);

            };

            flipB.Click += (s, e) =>
            {
                MainControl.B = (byte)(255 - MainControl.B);
                MainControl.ControlChangedData?.Invoke(s);
            };


            Position.SetMin +=
               (s) =>
               {
                   MainControl.SetColor(0, 0, 0);
                   st.SetImportanceAll(true, 0, 3);
                   MainControl.ControlChangedData?.Invoke(s);
               };
            InitializeControl();
          
            


        }

        public void Reload(object sender)
        {
            MainControl.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            Header.Reload();

        }

        public GroupState groupControl { get; set; }
        
        public void ResetNickName()
        {
            
        if (groupControl != null)
                Header.NickName.Content = groupControl.Name;
        else
           Header.NickName.Content = state.Id.ToString();
        }

        private void InitializeControl()
        {
            MainControl.ControlChangedData += this.ControlChangedData;
            Header.IpName.Content = state.Id.ToString();
            Header.NickName.Content = state.Id.ToString();
            
        }
     

        private void MaintContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

            ControlHeight = this.Height;

        }

        private void MainControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MainControl.ControlChangedData?.Invoke(sender);
        }

        private void MainControl_TouchUp(object sender, TouchEventArgs e)
        {
            MainControl.ControlChangedData?.Invoke(sender);
        }
    }
}
