﻿using Lights;
using Lights.Comunication;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for HeaderControl.xaml
    /// </summary>
    public partial class HeaderControl : UserControl, IHeaderControl
    {
        public IState StateOfEquipment { get; set; }

        public UserAction ControlChangedData { get => controlChangedData; set { controlChangedData += value; } }
        UserAction controlChangedData;

        public string NameLabel { get=>NickName.Content.ToString(); set { NickName.Content = value; } }
        public string TechnicalLabel { get => IpName.Content.ToString(); set { IpName.Content = value; } }

        public byte Priority { get => valuePriority; }
        byte valuePriority;

        public double ControlHeight { get=>this.Height;  set { this.Height = value; } }
        public double ControlWidth { get => this.Width; set { this.Width = value; } }

        public void SetGroup(IList<string> members)
        {
            if (members == null || members.Count <= 0) return;
            this.TechnicalGroup.Visibility = Visibility.Visible;
            this.TechnicalGroup.Text = "MEMBERS";
            this.TechnicalGroup.IsDropDownOpen = false;
            this.TechnicalGroup.IsEditable = true;
            this.upPriority.Visibility = Visibility.Hidden;
            this.downPriority.Visibility = Visibility.Hidden;
            this.priority.Visibility = Visibility.Hidden;
            this.TechnicalGroup.Items.Clear();
            foreach (var item in members)
            {
                Label label = new Label();
                label.Content = item;
                label.FontSize = 20;
                this.TechnicalGroup.Items.Add(label);                
            }
        }


        public HeaderControl()
        {
          
            InitializeComponent();
          
        }

        public void Reload()
        {
            valuePriority = StateOfEquipment.Importance.Importance;
            priority.Content = valuePriority;
        }
       public HeaderControl(IState state)
        {
            StateOfEquipment = state;
            InitializeComponent();
            
        }

        public void setPriority(IState st)
        {
            StateOfEquipment = st;
            valuePriority = StateOfEquipment.Importance.Importance;
            priority.Content = valuePriority;
            
            upPriority.Click += (s, e) =>
            {
                
                if (!(valuePriority < Server.MaximalPriorityValue)) return;
                valuePriority++;
                Priority imp;
                imp.Importance = valuePriority;
                StateOfEquipment.Importance = imp;
                priority.Content = valuePriority;                
                controlChangedData?.Invoke(s);
            };

            downPriority.Click += (s, e) =>
            {
                var imp = StateOfEquipment.Importance;
                if (!(imp.Importance > 0)) return;
                imp.Importance--;
                StateOfEquipment.Importance = imp;
                valuePriority = StateOfEquipment.Importance.Importance;
                priority.Content = valuePriority;
                controlChangedData?.Invoke(s);
            };
        }

        
    }
}
