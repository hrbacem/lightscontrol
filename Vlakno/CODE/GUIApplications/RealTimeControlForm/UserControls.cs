﻿using Lights.EquipmentsKinds;
using Lights.Parsers;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealTimeControl
{
    public delegate void UserAction(object sender);

    public interface ISizeControl
    {
        double ControlHeight { get; set; }
        double ControlWidth { get; set; }
    }

    public interface IPositionControl : ISizeControl
    {
        UserAction MoveRight { get; set; }
        UserAction MoveLeft { get; set; }
        UserAction SetMax { get; set; }
        UserAction SetMin { get; set; }
    }

    public interface IColorControl
    {
        byte R { get; set; }
        byte G { get; set; }
        byte B { get; set; }

        void SetColor(byte r, byte g, byte b);
    }

    public interface IHeaderControl : ISizeControl
    {
        IState StateOfEquipment { get; }

        byte Priority { get; }

        string NameLabel { get; set; }
        string TechnicalLabel { get; set; }

        void SetGroup(IList<string> members);


    }

    public interface IControl : ISizeControl
    {

        UserAction ControlChangedData { get; set; }

        IState StateOfEquipment { get; set; }
        
    }

    public interface IEquipmentControl : ISizeControl
    {
        void Reload(object sender);
        UserAction ControlChangedData { get; set; }
        UserAction DataWasChange { get; set; }
        double Width { get; }

        IControl getIControl { get; }
        IHeaderControl getIHeaderControl { get; }

        IPositionControl getIPositionControl { get; }

        void ResetNickName();
        int StatePosition { get; }

        GroupState groupControl { get; set; }

    }

    public interface IFactory
    {
        IEquipmentControl getControl(IState state, int position = 0);
        IEquipmentControl getControlSlider(IState state, int position);
        IEquipmentControl getControlAxis(IState state);
        IEquipmentControl getControlRGB(IState state);

        double Height { get; }
    }

    

    public interface IDefaultRepresentState
    {
        void CreateRepresentation(IState state, ControlDockPanel layout);

        UserAction ControlChangedData { get; set; }
    }
}
