﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lights.Transport;

namespace RealTimeControl
{
    

    /// <summary>
    /// Interaction logic for WideSlider.xaml
    /// </summary>
    public partial class WideSlider : UserControl, IControl
    {
        public double Value { get=>(int)SliderControl.Value; }
        public WideSlider()
        {
            
            InitializeComponent();
        }
        public double ControlHeight {
            get => this.Height;
            set
            {
                this.Height = value;

                Container.Height = value;
                ScaleTransform ScaleTransform = new ScaleTransform();
                TransformGroup transformGroup = new TransformGroup();
                ScaleTransform.ScaleY = value / SliderControl.Height;
                
                ScaleTransform.ScaleX = 10;
                
                transformGroup.Children.Add(ScaleTransform);
                
                SliderControl.RenderTransform = transformGroup;

            }
        }



        public double ControlWidth { get => this.Width; set { this.Width = value;throw new Exception(); } }
        public WideSlider(IState State)
        {
            StateOfEquipment = State;
            InitializeComponent();
        }

        public UserAction ControlChangedData { get; set; }
        public IState StateOfEquipment { get; set; }


        public IControl getControl(IState state)
        {
            throw new NotImplementedException();
        }

        private void MainControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            labelValue.Content = Value;
            ControlChangedData?.Invoke(sender);
        }


        
        private void SliderControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            Vector vec = e.DeltaManipulation.Translation; ;

            
            SliderControl.Value -= ((SliderControl.Maximum / SliderControl.Height) * vec.Y);
            
        }

        
    }

    
}
