﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lights.Transport;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for WpfJoystick.xaml
    /// </summary>
    public partial class WpfJoystick : UserControl, IControl
    {
        public UserAction ControlChangedData { get; set; }
        public IState StateOfEquipment { get; set; }

        public double ControlHeight
        {
            get => this.Height;
            set
            {
                this.block.Height = 400;
                double scale = value / this.block.Height;
           
                ScaleTransform scaleTrans = new ScaleTransform();
                scaleTrans.ScaleY = scale;

                TransformGroup trans= new TransformGroup();
                trans.Children.Add(scaleTrans);
                block.RenderTransform = trans;

            }
        }

        public double ControlWidth { get => this.Width; set { this.Width = value; } }

        public IControl getControl(IState state)
        {
            throw new NotImplementedException();
        }

        public int getX()
        {
            return (int)this.scrollbarRotation.Value ;
        }

        public int getY()
        {
            return (int)this.scrollbarTrans.Value ;
        }

        public void setMaximum(byte max)
        {
            this.scrollbarTrans.Maximum = max;
            this.scrollbarRotation.Maximum = max;
            
        }

        public void  setX(double x)
        {
            this.scrollbarRotation.Value=x;
        }

        public void setY(double y)
        {
            this.scrollbarTrans.Value =y;
        }

        public void setXY(double x, double y)
        {
            this.scrollbarRotation.Value = x;
            this.scrollbarTrans.Value = y;
        }

        bool touchEnabled = true;
        public void enableTouch(bool enable= true)
        {
            touchEnabled = enable;
        }

        // Circular slider is use from page of Charles Petzold
        public WpfJoystick()
        {
            InitializeComponent();

        }    

        private void ScrollBarChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ControlChangedData?.Invoke(sender);
            TransText.Text = getY().ToString();
            rotText.Text =getX().ToString();
        }

        private void CanvasContainer_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (!touchEnabled) return;
            Vector vec = e.DeltaManipulation.Translation;

            //2* because range is [-90,+90] and +1 because of feeling
            this.scrollbarRotation.Value += ((3*this.scrollbarRotation.Maximum / this.Width) * vec.X);
            this.scrollbarTrans.Value -= ((3*this.scrollbarTrans.Maximum / this.Height) * vec.Y);
        }

    }



    public class NegativeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return -System.Convert.ToDouble(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return -System.Convert.ToDouble(value);
        }
    }




}
