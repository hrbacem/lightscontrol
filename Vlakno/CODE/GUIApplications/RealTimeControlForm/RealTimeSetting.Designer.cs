﻿namespace RealTimeControl
{
    partial class RealTimeSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RealTimeSetting));
            this.Controllers = new System.Windows.Forms.Integration.ElementHost();
            this.controllersRealTime1 = new RealTimeControl.ControlDockPanel();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.mainLightControl1 = new RealTimeControl.MainLightControl();
            this.SuspendLayout();
            // 
            // Controllers
            // 
            this.Controllers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Controllers.BackColor = System.Drawing.Color.Transparent;
            this.Controllers.Location = new System.Drawing.Point(4, 208);
            this.Controllers.Name = "Controllers";
            this.Controllers.Size = new System.Drawing.Size(960, 520);
            this.Controllers.TabIndex = 2;
            this.Controllers.Text = "elementHost2";
            this.Controllers.Child = this.controllersRealTime1;
            // 
            // elementHost1
            // 
            this.elementHost1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHost1.BackColor = System.Drawing.Color.Transparent;
            this.elementHost1.Location = new System.Drawing.Point(4, 12);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(960, 190);
            this.elementHost1.TabIndex = 1;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.mainLightControl1;
            // 
            // RealTimeSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(970, 731);
            this.Controls.Add(this.Controllers);
            this.Controls.Add(this.elementHost1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RealTimeSetting";
            this.Text = "Control console";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private MainLightControl mainLightControl1;
        private System.Windows.Forms.Integration.ElementHost Controllers;
        private ControlDockPanel controllersRealTime1;
    }
}