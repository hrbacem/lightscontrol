﻿using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for ColorWheel.xaml
    /// </summary>
    public partial class ColorPickerWpf : UserControl,IControl, IColorControl
    {
        public ColorPickerWpf()
        {
            
            InitializeComponent();          
        }

        
        public double ControlHeight {
            get => this.Height;
            set
            {
                this.Height = value;
                
                ScaleTransform ScaleTransform = new ScaleTransform();
                TransformGroup transformGroup = new TransformGroup();
                ScaleTransform.ScaleY =(value) / Container.Height;
                transformGroup.Children.Add(ScaleTransform);
                TranslateTransform Translate = new TranslateTransform();               
                Translate.Y = -(this.Height - value)/2;
                transformGroup.Children.Add(Translate);                
                Container.RenderTransform = transformGroup;
                
                
            }
        }
        

        public double ControlWidth { get => this.Width; set { this.Width = value; } }
        public byte R{ get => ColorPicker.Color.R; set { ColorPicker.Color.R = value; ColorPicker.SetColor(Color.FromRgb(value, G, B)); } }
        public byte G{ get => ColorPicker.Color.G; set { ColorPicker.Color.G = value; ColorPicker.SetColor(Color.FromRgb(R, value, B)); } }
        public byte B{ get => ColorPicker.Color.B; set { ColorPicker.Color.B = value; ColorPicker.SetColor(Color.FromRgb(R, G, value)); } }
        
        public void SetColor(byte r, byte g, byte b) { this.ColorPicker.SetColor(Color.FromRgb(r,g,b)); }
        public UserAction ControlChangedData { get; set; }
        public IState StateOfEquipment { get; set; }

       
        private void ColorPicker_OnPickColor(Color color)
        {
            
            ControlChangedData?.Invoke(null);

            
        }
              
       
    }
    
}
