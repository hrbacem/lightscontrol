﻿using Lights.Parsers;
using Lights.EquipmentsKinds;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace RealTimeControl
{

    public enum SupportedGroups
    {
        RGB,
        AXIS,
        SLIDER

    }


    /// <summary>
    /// Interaction logic for newGroup.xaml
    /// </summary>
    public partial class GroupCreator : UserControl
    {

        //public List<GroupState> listOfGroups = new List<GroupState>();
        public ControlDockPanel Layout { get; set; }
        public RepresentStateFactory RepresentationFactory { get; set; }
        public Dictionary<IID, IState> Data { get => data; set { data = value; InitOptions(); } }
        Dictionary<IID, IState> data;
        Aliases Aliases;
        Dictionary<CheckBox, EquipmentPart> SetterOFMembers;
        SupportedGroups typeOfGroup;



        public GroupCreator()
        {
            InitializeComponent();
        }

        public void SetAliases(Aliases aliases)
        {
            Aliases = aliases;
        }

        protected struct EquipmentPart
        {

            public IState state;
            public int offset;
            public string name;
            public Slider level;
            public CheckBox IsInvers;

        }

        public string GetLabel(int i, Type type)
        {
            if (type == typeof(State<SpyderLight>))
            {
                switch (i)
                {
                    case 0: { return "color R"; }
                    case 1: { return "color G"; }
                    case 2: { return "color B"; }
                    case 3: { return "axis X"; }
                    case 4: { return "axis Y"; }
                    default: { return i.ToString(); }
                }
            }

            if (type == typeof(State<RGBLight>))
            {
                switch (i)
                {
                    case 0: { return "color R"; }
                    case 1: { return "color G"; }
                    case 2: { return "color B"; }
                    default: { return i.ToString(); }
                }
            }

            return i.ToString();
        }
        public void InitOptions()
        {

            RGB_kind.Click += (s, e) =>
            {
                typeOfGroup = SupportedGroups.RGB;
                SetterOFMembers = new Dictionary<CheckBox, EquipmentPart>();
                Kinds.IsDropDownOpen = false;
                Kinds.Text = "RGB EQUIPMENTS";
                this.listView.Items.Clear();

                if (this.data == null) return;
                foreach (var item in this.data)
                {
                    if (item.Value.GetType() == typeof(State<RGBLight>) || (item.Value.GetType() == typeof(State<SpyderLight>)))
                    {
                        if (Aliases.TryGetNickName(new PartNick((ID)item.Value.Id), out string name)) name += " ";
                        var part = new CheckBox
                        {
                            Content = name + item.Value.Id.ToString()
                        };

                        this.listView.Items.Add(part);

                        EquipmentPart stateOfChoosen;
                        stateOfChoosen.state = item.Value;
                        stateOfChoosen.offset = 0;
                        stateOfChoosen.name = part.Content.ToString();
                        stateOfChoosen.level = null;
                        stateOfChoosen.IsInvers = null;
                        SetterOFMembers.Add(part, stateOfChoosen);
                    }


                }

            };

            SPYDER_kind.Click += (s, e) =>
            {
                typeOfGroup = SupportedGroups.AXIS;
                SetterOFMembers = new Dictionary<CheckBox, EquipmentPart>();
                Kinds.IsDropDownOpen = false;
                Kinds.Text = "AXIS EQUIPMENTS";
                this.listView.Items.Clear();
                if (this.data == null) return;
                foreach (var item in this.data)
                {
                    if (item.Value.GetType() == typeof(State<SpyderLight>))
                    {
                        if (Aliases.TryGetNickName(new PartNick((ID)item.Value.Id), out string name)) name += " ";
                        var part = new CheckBox
                        {
                            Content = name + item.Value.Id.ToString()

                        };
                        
                        this.listView.Items.Add(part);

                        EquipmentPart stateOfChoosen;
                        stateOfChoosen.state = item.Value;
                        stateOfChoosen.offset = 0;
                        stateOfChoosen.name = part.Content.ToString();
                        stateOfChoosen.level = null;
                        stateOfChoosen.IsInvers = null;
                        SetterOFMembers.Add(part, stateOfChoosen);
                    }


                }

            };

            ONE_kind.Click += (s, e) =>
            {
                typeOfGroup = SupportedGroups.SLIDER;
                SetterOFMembers = new Dictionary<CheckBox, EquipmentPart>();
                Kinds.IsDropDownOpen = false;
                Kinds.Text = "ONE PART EQUIPMENTS";
                this.listView.Items.Clear();
                if (this.data == null) return;
                foreach (var item in this.data)
                {

                    for (int i = 0; i < item.Value.Parts.Data.Length; i++)
                    {

                        if (Aliases.TryGetNickName(new PartNick((ID)item.Value.Id, false, i), out string name)) name += " ";

                        string partName = name + GetLabel(i, item.Value.GetType()) + " :" + item.Value.Id.ToString();


                        StateSetter state = new StateSetter(partName);
                        this.listView.Items.Add(state);

                        EquipmentPart stateOfChoosen;
                        stateOfChoosen.state = item.Value;
                        stateOfChoosen.offset = i;
                        stateOfChoosen.name = partName;
                        stateOfChoosen.level = state.Ratio;
                        stateOfChoosen.IsInvers = state.UseInverse;
                        SetterOFMembers.Add(state.usePart, stateOfChoosen);
                    }

                }

            };
        }

        private void CreateGroup(object sender, RoutedEventArgs e)
        {
            List<string> members = new List<string>();
            if (null == SetterOFMembers) return;
            RepresentStateFactory generator = RepresentationFactory;

            // First check, if there is atleas one member -> empty groups are not supported
            bool haveMember = false;
            foreach (var item in SetterOFMembers)
            {
                if ((bool)item.Key.IsChecked)
                {
                    haveMember = true;
                    break;
                }
            }
            if (!haveMember)
                return;


            Type kindOfStates;
            switch (typeOfGroup)
            {
                case SupportedGroups.RGB:
                    kindOfStates = typeof(RGBLight);    
                    break;
                case SupportedGroups.AXIS:
                    kindOfStates = typeof(SpyderLight);
                    break;

                case SupportedGroups.SLIDER:
                default:
                    kindOfStates = null;
                    break;
            }


            GroupState newGroup = new GroupState(kindOfStates);
            List<EquipmentPart> toChange = new List<EquipmentPart>();
            foreach (var item in SetterOFMembers)
            {
                if (!(bool)item.Key.IsChecked) continue;
                toChange.Add(item.Value);
                members.Add(item.Value.name);
                item.Key.IsChecked = false;

                int level = 100;
                if (item.Value.level != null) level = (int)item.Value.level.Value;

                bool isInverseFlag = false;
                if (item.Value.IsInvers != null) isInverseFlag = (bool)item.Value.IsInvers.IsChecked;
                newGroup.AddMember((ID)item.Value.state.Id, item.Value.name, item.Value.offset, isInverseFlag, level);
            }

            //Create  a new group
            newGroup.Name = nameOfGroup.Text;
            newGroup.TypeOfGroup = kindOfStates;
            Aliases.TryAdd(newGroup);
            Aliases.AliasesChanged?.Invoke();

        }

    }
}
