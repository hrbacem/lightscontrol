﻿using Lights.Parsers;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for EquipmentControl.xaml
    /// </summary>
    public partial class EquipmentControlSlider : UserControl, IEquipmentControl
    {


        public UserAction ControlChangedData {
            get => controlChangedData;
            set {
                controlChangedData += value;
                MainControl.ControlChangedData += value;
                Header.ControlChangedData += value;
            }
        }

        UserAction controlChangedData;
        public UserAction DataWasChange { get; set; }
        public double ControlWidth { get => this.Width; set { this.Width = value; } }
        public double ControlHeight {
            get => this.Height;
            set {
                this.Height = value;
                double Height = value - getIHeaderControl.ControlHeight - getIPositionControl.ControlHeight;
                getIControl.ControlHeight = Height;

                MainControl.Height = Height;
            }
        }
        public IControl getIControl => this.MainControl;

        public IHeaderControl getIHeaderControl => this.Header;

        public IPositionControl getIPositionControl => this.Position;

        IState state;


        public int PartPosition { get; }

        public int StatePosition { get; internal set; }
        public EquipmentControlSlider(IState st, int position = 0)
        {
            DataWasChange += Reload;

            PartPosition = position;
            StatePosition = position;


            InitializeComponent();
            this.state = st;
            Header.setPriority(st);
            MainControl.StateOfEquipment = st;
            MainControl.SliderControl.Minimum = 0;
            MainControl.SliderControl.Maximum = 255;
            MainControl.SliderControl.Value = st.Parts.Data[PartPosition];

            ControlChangedData += (s) =>
            {
                st.SetValue(PartPosition, (byte)MainControl.SliderControl.Value);
                
            };
            Position.SetMax +=
               (s) =>
               {
                   MainControl.SliderControl.Value = MainControl.SliderControl.Maximum;
                   st.SetValue(PartPosition, (byte)MainControl.SliderControl.Maximum);
                   MainControl.ControlChangedData?.Invoke(s);
               };

            Position.SetMin +=
               (s) =>
               {
                   MainControl.SliderControl.Value = MainControl.SliderControl.Minimum;
                   st.SetValue(PartPosition, (byte)MainControl.SliderControl.Minimum);
                   MainControl.ControlChangedData?.Invoke(s);
               };

            InitializeControl();

        }

        public void Reload(object sender)
        {
            MainControl.SliderControl.Value = state.Parts.Data[PartPosition];
            Header.Reload();
        }

        public GroupState groupControl { get; set; }

       public void ResetNickName()
        {
            if (groupControl != null)
                Header.NickName.Content = groupControl.Name;
            else
             Header.NickName.Content = PartPosition;
        }

        private void InitializeControl()
        {
            Header.IpName.FontSize = 16;
            Header.NickName.FontSize = 16;
            MainControl.ControlChangedData += this.ControlChangedData;
            Header.IpName.Content = state.Id.ToIPv4();
            Header.NickName.Content = PartPosition; 
           
        }

        private void MaintContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ControlHeight = this.Height;
        }
    }
}
