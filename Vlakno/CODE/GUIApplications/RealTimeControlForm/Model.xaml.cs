﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for Model.xaml
    /// </summary>
    public partial class Model : UserControl
    {
        //Default position of light
        public int Zangle = 0;
        public int Yangle = 0;
        public Model(int axisYangle = 0,int axisZangle=0)
        {
            InitializeComponent();
            
            this.Zangle = axisZangle;
            this.Yangle = axisYangle;
            this.objectY.Angle = Yangle;
            this.objectZ.Angle = Zangle;
            ModelTouch.Visibility = Visibility.Hidden;
        }

        public void AddModelTouchAction(System.Windows.RoutedEventHandler action)
        {
           this.ModelTouch.Click += action;
        }
        public Color LightColor { get => this.Color.Color; set { this.Color.Color = value; } }

        public Model()
        {
            InitializeComponent();
           
            this.objectY.Angle = Yangle;
            this.objectZ.Angle = Zangle;
            ModelTouch.Visibility = Visibility.Hidden;
        }


        public void ButtonEnable(bool enable = true)
        {
            if (enable)
            {
                ModelTouch.Visibility = Visibility.Visible;
            }
            else
            {
                
                ModelTouch.Visibility = Visibility.Hidden;
            }
        }
        public void moveLightToDefault()
        {
            
            this.Zangle = 0;
            this.Yangle = 0;
            this.objectY.Angle = 0;
            this.objectZ.Angle = 0;
        }

        public void SetColor(byte R, byte G, byte B)
        {
            this.Color.Color = System.Windows.Media.Color.FromRgb(R,G,B);
        }

        public void moveLightTo(int axisYangle = 0, int axisZangle = 0)
        {

            this.Zangle = axisZangle;
            this.Yangle = axisYangle;
            this.headAxis.Angle = Yangle;
            this.holderLight.Angle = Zangle;
           
        }

    }


    
}
