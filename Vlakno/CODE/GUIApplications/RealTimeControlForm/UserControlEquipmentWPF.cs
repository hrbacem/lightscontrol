﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Controls;
using Lights.Transport;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Lights.EquipmentsKinds;

namespace RealTimeControl
{


    public  class RepresentStateFactory : IDefaultRepresentState
    {
        public double Height { get => height; set { height = value; } }
        double height =500;

        public RepresentStateFactory()
        {

            VisualBrush vBrush = new VisualBrush();

            vBrush.Opacity = 0.50;
            
            
            DefaultFactory = new EguipmentFactory(vBrush);
        }

        public RepresentStateFactory(System.Windows.Media.Brush brush)
        {
            DefaultFactory = new EguipmentFactory(brush);
        }

        public UserAction ControlChangedData { get; set; }

        public UserAction DataWasChange { get; set; }

        IFactory DefaultFactory = null; // factory creating user inteface
        private void controlButtons(IEquipmentControl userControl, ControlDockPanel layout)
        {
            IPositionControl positionHandler = userControl.getIPositionControl;

            positionHandler.MoveLeft +=
                (s) =>
                {

                    int positionThis = layout.Controls.Children.IndexOf((UserControl)userControl);
                    if (positionThis <= 0) return;

                    layout.Controls.Children.Remove((UserControl)userControl);
                    layout.Controls.Children.Insert(positionThis - 1, (UserControl)userControl);
                };

            positionHandler.MoveRight +=
                (s) =>
                {

                    int positionThis = layout.Controls.Children.IndexOf((UserControl)userControl);
                    if (positionThis >= layout.Controls.Children.Count - 1) return;

                    layout.Controls.Children.Remove((UserControl)userControl);
                    layout.Controls.Children.Insert(positionThis + 1, (UserControl)userControl);
                };




        }


        public void CreateRepresentation(IState state, ControlDockPanel layout)
        {



            if (state.GetType() == typeof(State<RGBLight>))
            {
                CreateRepresentationRGB(state, layout);
                return;
            }
            if (state.GetType() == typeof(State<RGBLight>))
            {
                CreateRepresentationRGB(state, layout);
                return;
            }

            if (state.GetType() == typeof(State<SpyderLight>))
            {
                CreateRepresentationSPYDER(state, layout);
                return;
            }

            if (state.GetType() == typeof(State<DIMMLight>))
            {
                CreateRepresentationGENERAL(state, layout);
                return;
            }




            CreateRepresentationGENERAL(state, layout);
        }


        public void CreateRepresentationGENERAL(IState state, ControlDockPanel layout)
        {
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                IEquipmentControl uf = GiveControl(state, DefaultFactory, i);
                DataWasChange += uf.DataWasChange;
                uf.ControlChangedData += ControlChangedData;
                layout.AddControl((UserControl)uf);
                controlButtons(uf, layout);
            }

        }

        public void CreateRepresentationRGB(IState state, ControlDockPanel layout)
        {
            IEquipmentControl uf = GiveControl(state, DefaultFactory);
            DataWasChange += uf.DataWasChange;
            uf.ControlChangedData += ControlChangedData;
            layout.AddControl((UserControl)uf);
            controlButtons(uf, layout);

        }


        public void CreateRepresentationSPYDER(IState state, ControlDockPanel layout)
        {
            IEquipmentControl uf1 = GiveControl(state, DefaultFactory);
            DataWasChange += uf1.DataWasChange;
            uf1.ControlChangedData += ControlChangedData;
            layout.AddControl((UserControl)uf1);
            controlButtons(uf1, layout);

            IEquipmentControl uf = GiveControl(state, DefaultFactory, 1);
            DataWasChange += uf.DataWasChange;
            uf.ControlChangedData += ControlChangedData;
            layout.AddControl((UserControl)uf);
            controlButtons(uf, layout);
        }

        public IEquipmentControl GetUserControlRGB(IState state, ControlDockPanel layout, bool setStateUIpdate = false)
        {
            IEquipmentControl uf = GiveControl(state, DefaultFactory, SupportedKindOfControls.RGB);
            if (setStateUIpdate)
                DataWasChange += uf.DataWasChange;
            uf.ControlChangedData += ControlChangedData;
            layout.AddControlFirst((UserControl)uf);

            controlButtons(uf, layout);
            return uf;

        }

        public IEquipmentControl GetUserControlAXIS(State<SpyderLight> state, ControlDockPanel layout,bool setStateUIpdate =false)
        {

            IEquipmentControl uf = GiveControl(state, DefaultFactory, SupportedKindOfControls.AXIS, 1);
            if(setStateUIpdate)
                DataWasChange += uf.DataWasChange;
            uf.ControlChangedData += ControlChangedData;
            layout.AddControlFirst((UserControl)uf);

            controlButtons(uf, layout);
            return uf;

        }

        public IEquipmentControl GetUserControlSlider(IState state, ControlDockPanel layout, int position = 0, bool setStateUIpdate = false)
        {

            IEquipmentControl uf = GiveControl(state, DefaultFactory, SupportedKindOfControls.SLIDER, position);
            if (setStateUIpdate)
                DataWasChange += uf.DataWasChange;
            uf.ControlChangedData += ControlChangedData;
            layout.AddControlFirst((UserControl)uf);

            controlButtons(uf, layout);
            return uf;
        }


        public IEquipmentControl GiveControl(IState state, IFactory creator, int pos = 0)
        {
            IEquipmentControl controller = creator.getControl(state, pos);
            controller.ControlChangedData += this.ControlChangedData;
            DataWasChange += controller.DataWasChange;
            int width = (int)controller.Width;
            
            return controller;

        }

        public IEquipmentControl GiveControl(IState state, IFactory creator, SupportedKindOfControls kind, int pos = 0)
        {
            IEquipmentControl controller;
            switch (kind)
            {
                case SupportedKindOfControls.RGB:
                    {
                        controller = creator.getControlRGB(state);
                    }
                    break;

                case SupportedKindOfControls.AXIS:
                    {
                        controller = creator.getControlAxis(state);
                    }
                    break;


                case SupportedKindOfControls.SLIDER:
                default:
                    {
                        controller = creator.getControlSlider(state, pos);
                    }
                    break;


            }

            controller.ControlChangedData += this.ControlChangedData;
            int width = (int)controller.Width;
            
            return controller;
        }

        public enum SupportedKindOfControls
        {
            RGB,
            AXIS,
            SLIDER
        }

    }

    public class EguipmentFactory : IFactory
    {
        public double Height { get => 500; }
        public EguipmentFactory()
        {
            BackroundBrush = System.Windows.Media.Brushes.Black;
        }

        public EguipmentFactory(System.Windows.Media.Brush backroundBrush)
        {
            this.BackroundBrush = backroundBrush;
        }

        public System.Windows.Media.Brush BackroundBrush = System.Windows.Media.Brushes.Black;

        public IEquipmentControl getControl(IState state, int position)
        {

            var realType = state.GetType();

            if (realType == typeof(State<RGBLight>) || realType == typeof(State<SpyderLight>) && position == 0)
            {
                var control = new EquipmentControlColor(state);
                control.Background = this.BackroundBrush;
                return control;
            }


            if (realType == typeof(State<SpyderLight>) && position >= 1)
            {
                var control = new EquipmentControlAxis(state);
                control.Background = this.BackroundBrush;
                return control;
            }

            if (realType == typeof(State<DIMMLight>))
            {
                var control = new EquipmentControlSlider(state, position);
                control.Background = this.BackroundBrush;
                return control;
            }


            {
                var control = new EquipmentControlSlider(state, position);
                control.Background = this.BackroundBrush;
                return control;
            }
        }

        public IEquipmentControl getControlRGB(IState state)
        {

            var control = new EquipmentControlColor(state);
            control.Background = this.BackroundBrush;
            return control;
        }

        public IEquipmentControl getControlAxis(IState state)
        {

            var control = new EquipmentControlAxis((State<SpyderLight>)state);
            control.Background = this.BackroundBrush;
            return control;
        }

        public IEquipmentControl getControlSlider(IState state, int position)
        {
            var control = new EquipmentControlSlider(state, position);
            control.Background = this.BackroundBrush;
            return control;

        }

    }

   
}
