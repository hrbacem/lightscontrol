﻿using Lights.Parsers;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for EquipmentControl.xaml
    /// </summary>
    public partial class EquipmentControlAxis : UserControl, IEquipmentControl
    {

        public double ControlWidth { get => this.Width; set { this.Width = value; } }
        public double ControlHeight { get => this.Height; set
            {
                this.Height = value;
                double Height = value - getIHeaderControl.ControlHeight - getIPositionControl.ControlHeight;
                getIControl.ControlHeight = Height;
                MainControl.Height = Height;
            }
        }
        public UserAction ControlChangedData { get => controlChangedData; set { controlChangedData += value; MainControl.ControlChangedData += value; Header.ControlChangedData += value; } }
        UserAction controlChangedData;
        public UserAction DataWasChange { get; set; }

        public IControl getIControl => this.MainControl;

        public IHeaderControl getIHeaderControl =>this.Header;

        public IPositionControl getIPositionControl => this.Position;

        IState state;

        public int StatePosition { get => 0; }

        int axisX = 3;
        int axisY = 4;
        public EquipmentControlAxis(IState st, int x = 3, int y = 4)
        {
            DataWasChange += Reload;
            axisX = x;
            axisY = y;
            InitializeComponent();
            state = st;
            MainControl.StateOfEquipment = st;

            Header.setPriority(st);
            MainControl.setX(state.Parts.Data[axisX] - 90);
            MainControl.setY(state.Parts.Data[axisY] - 90);
            Model.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            ControlChangedData += (s) =>
            {
                state.SetValue(axisX,(byte)(MainControl.getX() + 90));
                state.SetValue(axisY, (byte)(MainControl.getY()+90));


            };
            

            Position.SetMaxLabel = "Default X";
            Position.SetMax +=
                (s) =>
                {
                    MainControl.setX(0);
                    MainControl.ControlChangedData?.Invoke(s);

                };
            Position.SetMinLabel = "Default Y";
            Position.SetMin +=
               (s) =>
               {
                   MainControl.setY(0);
                   MainControl.ControlChangedData?.Invoke(s);
               };
            InitializeControl();
            
        }

        public void Reload(object sender)
        {
            MainControl.setXY(state.Parts.Data[axisX] - 90, state.Parts.Data[axisY] - 90);
            Model.moveLightTo(MainControl.getY(), MainControl.getX());
            Model.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            Header.Reload();
        }

        public GroupState groupControl { get; set; }
        
        public void ResetNickName()
        {
         
        if (groupControl != null)
                Header.NickName.Content = groupControl.Name;
        else
            Header.IpName.Content = state.Id.ToString();
        }

        private void InitializeControl()
        {            
            
            Header.IpName.Content = state.Id.ToString();
            Header.NickName.Content = state.Id.ToString();
            MainControl.ControlChangedData += (s) =>
            {
               Model.moveLightTo(MainControl.getY(), MainControl.getX());
            };

            MainControl.ControlChangedData += this.ControlChangedData;

        }
       
        private void MaintContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ControlHeight = this.Height;
        }

        private void MainControl_TouchUp(object sender, TouchEventArgs e)
        {
            MainControl.ControlChangedData?.Invoke(sender);
        }

        private void MainControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MainControl.ControlChangedData?.Invoke(sender);
        }
    }
}
