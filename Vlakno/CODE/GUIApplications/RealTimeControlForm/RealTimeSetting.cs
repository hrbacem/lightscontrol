﻿using Lights;
using Lights.Parsers;
using Lights.Messages;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RealTimeControl
{
    public partial class RealTimeSetting : DockContent
    {


        Dictionary<IID, IState> Data;
        public UserAction DataWasChanged { get; set; }
        public UserAction ControlChangedData { get; set; }
        RepresentStateFactory generator; 
        private UserAction ControlAction;
        Aliases Aliases;
        private List<IID> represented = new List<IID>();

        MainLightControl mainEqControl;
        ControlDockPanel realtimeControls;

        public RealTimeSetting(Dictionary<IID, IState> data,Aliases aliases, UserAction userChanged)
        {            
            Data = data;
            Aliases = aliases;
            generator = new RepresentStateFactory();
            DataWasChanged += updateCont;
            InitializeComponent();
            
            mainEqControl = ((MainLightControl)(elementHost1.Child));
            realtimeControls = ((ControlDockPanel)Controllers.Child);
            mainEqControl.data = data;

            mainEqControl.Aliases = Aliases;
            mainEqControl.Layout = realtimeControls;
            mainEqControl.RepresentationFactory = generator;
            
            
            ControlChangedData += userChanged;
            ControlAction += userChanged;
            Create();
            DataWasChanged += (s) => Create();
            Aliases.AliasesChanged += updateLabel;
            Aliases.AliasesChanged?.Invoke();
        }

        private void updateCont(object s)
        {
            generator.DataWasChange?.Invoke(s);
        }
        
        private void Create()
        {
            
            if (represented.Count == Data.Count) return;
            RemoveDeleted();
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            mainEqControl.UpdateMainControls();
            generator.ControlChangedData = ControlAction;
            bool addAny = false;
            lock(Data)
            foreach (var state in Data)
            {

                if (!represented.Contains(state.Key))
                {
                    generator.CreateRepresentation(state.Value, realtimeControls);
                    represented.Add(state.Key);
                    addAny = true;
                }

            }

            if (addAny) Aliases.AliasesChanged?.Invoke();
            Cursor.Current = cursor;
        }

        void updateLabel()
        {

            foreach (var item in realtimeControls.Controls.Children)
            {

                if (item is IEquipmentControl)
                {
                    var control = ((IEquipmentControl)item);
                    

                    if (Aliases.TryGetNickName(new PartNick((ID)control.getIControl.StateOfEquipment.Id, false, control.StatePosition), out string name))
                        control.getIHeaderControl.NameLabel = name;
                    else
                        control.ResetNickName();

                }
            }
        }

        public void RemoveDeleted()
        {

            ///Hardly ever is something deleted
            if (!(represented.Count > Data.Count)) return;
            
            List<IEquipmentControl> toDelete = new List<IEquipmentControl>();
            foreach (IEquipmentControl item in realtimeControls.Controls.Children)
            {
                IID id = item.getIControl.StateOfEquipment.Id;
                if (Data.ContainsKey(id)) continue;
                
                toDelete.Add(item);
                removeRepresented(id);
                
            }
            foreach (var item in toDelete)
            {
                realtimeControls.Controls.Children.Remove((System.Windows.Controls.UserControl)item);              
            }

            
               


        }

        private void removeRepresented(IID identificator)
        {
            ID id = (ID)identificator;
            for (int i = 0; i < represented.Count; i++)
            {
                if (((ID)represented[i]).Equals(id))
                {
                    represented.RemoveAt(i);
                    break;
                }
            }
        }

        public delegate void OnUserFormClosingAction(FormClosingEventArgs e);

        public OnUserFormClosingAction actionOnUserFormClosing;

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            actionOnUserFormClosing?.Invoke(e);
        }

    }

}
