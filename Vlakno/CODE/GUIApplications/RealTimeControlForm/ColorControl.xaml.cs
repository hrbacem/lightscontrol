﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lights.Transport;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for ColorControl.xaml
    /// </summary>
    public partial class ColorControl : UserControl, IControl, IColorControl
    {
        public ColorControl()
        {
            InitializeComponent();
        }

        

        public byte R { get => ColorPicker.SelectedColor.Value.R; set { ColorPicker.R = value; } }
        public byte G { get => ColorPicker.SelectedColor.Value.G; set { ColorPicker.G = value; } }
        public byte B { get => ColorPicker.SelectedColor.Value.B; set { ColorPicker.B = value; } }

        public void SetColor(byte r, byte g, byte b) { this.ColorPicker.SelectedColor = System.Windows.Media.Color.FromRgb(r,g,b); }
        public UserAction ControlChangedData { get; set; }
        public IState StateOfEquipment { get; set; }

        public double ControlHeight { get => this.Height; set { this.Height = value; } }
        public double ControlWidth { get => this.Width; set { this.Width = value; } }
        
        private void ColorPicker_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ControlChangedData?.Invoke(sender);
        }

        private void ColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            ControlChangedData?.Invoke(sender);
        }
    }
}
