﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealTimeControl
{
    /// <summary>
    /// Interaction logic for StateSetter.xaml
    /// </summary>
    public partial class StateSetter : UserControl
    {
        public StateSetter()
        {
            InitializeComponent();
        }


        public StateSetter(string name)
        {
            InitializeComponent();
            usePart.Content = name;
        }

        public Slider Ratio { get => this.slider; }
        public CheckBox UsePart { get => this.usePart; }
        public CheckBox UseInverse { get => this.useInverse; }
    }
}
