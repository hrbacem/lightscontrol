﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
    using Lights;
using Lights.Transport;

namespace GuiComponents
{
    
    public partial class Data<K, T> : DockContent where K : class
    {

        private Dictionary<K,T> data;
        private TextFormater formater;
        Line<K,T>.ActionOfButton action;

        public delegate string TextFormater(K key,T item);

        public delegate void OnCloseAction();

        public OnCloseAction ClosingAction;

        public Data( Dictionary<K, T> data,Line<K, T>.ActionOfButton act,TextFormater t) 
        {
            InitializeComponent();
            formater = t;
            this.data =data;
            this.action += act;
            this.action += ReloadBox;
            ReloadBox();
            
        }
               
        private void DataList_Load(object sender, EventArgs e)
        {         
        }

        private void Data_Load(object sender, EventArgs e)
        {

        }

        public void ReloadBox(K id = default(K),T key = default(T))
        {
            
            if (!(flowLayoutPanel1.Controls.Count == data.Count))
            {
                Line<K, T> look;
                for (int i = flowLayoutPanel1.Controls.Count-1; i>=0; i--)
                {
                    if(flowLayoutPanel1.Controls[i] is Line<K, T>)
                    {
                        look = ((Line<K, T>)flowLayoutPanel1.Controls[i]);
                        if (!data.ContainsKey(look.Index)) { flowLayoutPanel1.Controls.RemoveAt(i); }

                    }
                    
                }



                //for now? 
                Line<K, T>[] Contains = new Line<K, T>[flowLayoutPanel1.Controls.Count];
                for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
                {
                    Contains[i] = (Line<K, T>)flowLayoutPanel1.Controls[i];
                }

                lock (data)
                    foreach (var item in data)
                    {
                        if (!Contains.Where(i => i.Index == item.Key).Any())
                        {
                            Line<K, T> line = new Line<K, T>(item.Key,item.Value, formater(item.Key, item.Value), action);
                            line.Index = item.Key;
                            line.Visible = true;
                            line.Width = this.Width;
                            flowLayoutPanel1.Controls.Add(line);
                        }
                    }

            }
          
        }

        

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

       
    }
}
