﻿using GuiComponents;
using Lights.Parsers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiComponents
{

   
    public partial class CreateContext : Form
    {
        public CreateContext()
        {
            InitializeComponent();
           
        }

        

        private void SaveContext(object sender, EventArgs e)
        {
            if (!FileManager.loadNotEmptyFileName(out string fileName)) return;
            Context context = new Context();
            context.Name = nameBox.Text;
            foreach (string item in NickList.CheckedItems)
            {
                
                    context.Nicknames.Add(item);
                
            }

            foreach (string item in GroupList.CheckedItems)
            {
                                
                   context.Groups.Add(item);
                
            }

            foreach (string item in EquipmentList.CheckedItems)
            {
                
                
                    context.Equipments.Add(item);
                
            }

            ParserJsonList<Context>.TrySaveSingleOne(fileName, context);
        }

      

        bool defaultCheck = true;


        private void NicknamesBroswer_Click(object sender, EventArgs e)
        {
            if (!FileManager.loadExistFileName(out string fileName)) return;

            NickList.Items.Add(fileName, defaultCheck);
        }

        private void GroupsBroswer_Click(object sender, EventArgs e)
        {
            if (!FileManager.loadExistFileName(out string fileName)) return;
            GroupList.Items.Add(fileName, defaultCheck);
        }

        private void EquipmentsBroswer_Click(object sender, EventArgs e)
        {
            if (!FileManager.loadExistFileName(out string fileName)) return;
            EquipmentList.Items.Add(fileName, defaultCheck);
        }

        /// <summary>
        /// Load context file and add files to new context 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextBroswer_Click(object sender, EventArgs e)
        {
            if (!FileManager.loadExistFileName(out string filename)) return;

            Context load = new Context();
            if (load.LoadContext(filename))
            {
                MessageBox.Show("Not context file!");
                return;
            }
            bool defaultState = true; 
            foreach (var item in load.Equipments)
            {
                EquipmentList.Items.Add(item, defaultState);
            }
            foreach (var item in load.Nicknames)
            {
                NickList.Items.Add(item, defaultState);
            }
            foreach (var item in load.Groups)
            {
                GroupList.Items.Add(item, defaultState);
            }

            ContextList.Items.Add(load.Name+"->"+ filename);
            
        }
    }

    public class Context
    {
        public Context()
        {
            Equipments = new List<string>();
            Nicknames = new List<string>();
            Groups = new List<string>();

        }

        public string Name ="UNNAMED";
        public List<string> Equipments=null;
        public List<string> Nicknames = null;
        public List<string> Groups = null;

        public  bool LoadContext(string filename)
        {
            if(ParserJsonList<Context>.TryLoadSingleOne(filename, out Context contextLoaded))
            {
                if (contextLoaded == null) return false;
                if (contextLoaded.Nicknames==null) return false;
                if(contextLoaded.Groups == null) return false;
                if (contextLoaded.Equipments == null) return false;
                foreach (var item in contextLoaded.Nicknames)
                {
                    if(!Nicknames.Contains(item))
                    Nicknames.Add(item);
                }

                foreach (var item in contextLoaded.Equipments)
                {
                    if (!Equipments.Contains(item))
                        Equipments.Add(item);
                }

                foreach (var item in contextLoaded.Groups)
                {
                    if (!Groups.Contains(item))
                        Groups.Add(item);
                }
                Name = contextLoaded.Name;
                return true;
               
            }
            return false;

        }
    }

}
