﻿namespace GuiComponents
{
    partial class AddNewEquipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNewEquipment));
            this.nickName = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.id0 = new System.Windows.Forms.TextBox();
            this.id1 = new System.Windows.Forms.TextBox();
            this.id9 = new System.Windows.Forms.TextBox();
            this.id8 = new System.Windows.Forms.TextBox();
            this.id7 = new System.Windows.Forms.TextBox();
            this.id6 = new System.Windows.Forms.TextBox();
            this.id5 = new System.Windows.Forms.TextBox();
            this.id4 = new System.Windows.Forms.TextBox();
            this.id3 = new System.Windows.Forms.TextBox();
            this.id2 = new System.Windows.Forms.TextBox();
            this.IDlabel = new System.Windows.Forms.Label();
            this.ProtocolLabel = new System.Windows.Forms.Label();
            this.ProtocolBox = new System.Windows.Forms.ComboBox();
            this.AllPartsSetter = new System.Windows.Forms.CheckBox();
            this.OffsetValue = new System.Windows.Forms.NumericUpDown();
            this.ipAddress = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.OffsetValue)).BeginInit();
            this.SuspendLayout();
            // 
            // nickName
            // 
            this.nickName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nickName.Location = new System.Drawing.Point(12, 26);
            this.nickName.Name = "nickName";
            this.nickName.Size = new System.Drawing.Size(322, 20);
            this.nickName.TabIndex = 0;
            // 
            // SaveBtn
            // 
            this.SaveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveBtn.Location = new System.Drawing.Point(12, 143);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(470, 56);
            this.SaveBtn.TabIndex = 11;
            this.SaveBtn.Text = "Create equipment";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.button_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(103, 13);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Name of Equipment:";
            // 
            // id0
            // 
            this.id0.Location = new System.Drawing.Point(12, 81);
            this.id0.Name = "id0";
            this.id0.Size = new System.Drawing.Size(35, 20);
            this.id0.TabIndex = 1;
            this.id0.Text = "0";
            this.id0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id1
            // 
            this.id1.Location = new System.Drawing.Point(53, 81);
            this.id1.Name = "id1";
            this.id1.Size = new System.Drawing.Size(35, 20);
            this.id1.TabIndex = 2;
            this.id1.Text = "0";
            this.id1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id9
            // 
            this.id9.Location = new System.Drawing.Point(381, 81);
            this.id9.Name = "id9";
            this.id9.Size = new System.Drawing.Size(35, 20);
            this.id9.TabIndex = 10;
            this.id9.Text = "0\r\n";
            this.id9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id8
            // 
            this.id8.Location = new System.Drawing.Point(340, 81);
            this.id8.Name = "id8";
            this.id8.Size = new System.Drawing.Size(35, 20);
            this.id8.TabIndex = 9;
            this.id8.Text = "0";
            this.id8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id7
            // 
            this.id7.Location = new System.Drawing.Point(299, 81);
            this.id7.Name = "id7";
            this.id7.Size = new System.Drawing.Size(35, 20);
            this.id7.TabIndex = 8;
            this.id7.Text = "0";
            this.id7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id6
            // 
            this.id6.Location = new System.Drawing.Point(258, 81);
            this.id6.Name = "id6";
            this.id6.Size = new System.Drawing.Size(35, 20);
            this.id6.TabIndex = 7;
            this.id6.Text = "0";
            this.id6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id5
            // 
            this.id5.Location = new System.Drawing.Point(217, 81);
            this.id5.Name = "id5";
            this.id5.Size = new System.Drawing.Size(35, 20);
            this.id5.TabIndex = 6;
            this.id5.Text = "0";
            this.id5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id4
            // 
            this.id4.Location = new System.Drawing.Point(176, 81);
            this.id4.Name = "id4";
            this.id4.Size = new System.Drawing.Size(35, 20);
            this.id4.TabIndex = 5;
            this.id4.Text = "0";
            this.id4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id3
            // 
            this.id3.Location = new System.Drawing.Point(135, 81);
            this.id3.Name = "id3";
            this.id3.Size = new System.Drawing.Size(35, 20);
            this.id3.TabIndex = 4;
            this.id3.Text = "0";
            this.id3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // id2
            // 
            this.id2.Location = new System.Drawing.Point(94, 81);
            this.id2.Name = "id2";
            this.id2.Size = new System.Drawing.Size(35, 20);
            this.id2.TabIndex = 3;
            this.id2.Text = "0";
            this.id2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IDlabel
            // 
            this.IDlabel.AutoSize = true;
            this.IDlabel.Location = new System.Drawing.Point(12, 58);
            this.IDlabel.Name = "IDlabel";
            this.IDlabel.Size = new System.Drawing.Size(82, 13);
            this.IDlabel.TabIndex = 15;
            this.IDlabel.Text = "ID of equipment";
            // 
            // ProtocolLabel
            // 
            this.ProtocolLabel.AutoSize = true;
            this.ProtocolLabel.Location = new System.Drawing.Point(15, 119);
            this.ProtocolLabel.Name = "ProtocolLabel";
            this.ProtocolLabel.Size = new System.Drawing.Size(114, 13);
            this.ProtocolLabel.TabIndex = 17;
            this.ProtocolLabel.Text = "Protocol of Equipment:";
            // 
            // ProtocolBox
            // 
            this.ProtocolBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProtocolBox.FormattingEnabled = true;
            this.ProtocolBox.Location = new System.Drawing.Point(218, 116);
            this.ProtocolBox.Name = "ProtocolBox";
            this.ProtocolBox.Size = new System.Drawing.Size(264, 21);
            this.ProtocolBox.TabIndex = 18;
            // 
            // AllPartsSetter
            // 
            this.AllPartsSetter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AllPartsSetter.AutoSize = true;
            this.AllPartsSetter.Checked = true;
            this.AllPartsSetter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AllPartsSetter.Location = new System.Drawing.Point(367, 28);
            this.AllPartsSetter.Name = "AllPartsSetter";
            this.AllPartsSetter.Size = new System.Drawing.Size(63, 17);
            this.AllPartsSetter.TabIndex = 20;
            this.AllPartsSetter.Text = "All parts";
            this.AllPartsSetter.UseVisualStyleBackColor = true;
            // 
            // OffsetValue
            // 
            this.OffsetValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OffsetValue.Location = new System.Drawing.Point(436, 25);
            this.OffsetValue.Name = "OffsetValue";
            this.OffsetValue.Size = new System.Drawing.Size(46, 20);
            this.OffsetValue.TabIndex = 21;
            // 
            // ipAddress
            // 
            this.ipAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ipAddress.FormattingEnabled = true;
            this.ipAddress.Location = new System.Drawing.Point(217, 50);
            this.ipAddress.Name = "ipAddress";
            this.ipAddress.Size = new System.Drawing.Size(265, 21);
            this.ipAddress.TabIndex = 22;
            // 
            // AddNewEquipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 203);
            this.Controls.Add(this.ipAddress);
            this.Controls.Add(this.OffsetValue);
            this.Controls.Add(this.AllPartsSetter);
            this.Controls.Add(this.ProtocolBox);
            this.Controls.Add(this.ProtocolLabel);
            this.Controls.Add(this.IDlabel);
            this.Controls.Add(this.id2);
            this.Controls.Add(this.id3);
            this.Controls.Add(this.id4);
            this.Controls.Add(this.id5);
            this.Controls.Add(this.id6);
            this.Controls.Add(this.id7);
            this.Controls.Add(this.id8);
            this.Controls.Add(this.id9);
            this.Controls.Add(this.id1);
            this.Controls.Add(this.id0);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.nickName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddNewEquipment";
            this.Text = "New equipment";
            ((System.ComponentModel.ISupportInitialize)(this.OffsetValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nickName;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox id0;
        private System.Windows.Forms.TextBox id1;
        private System.Windows.Forms.TextBox id9;
        private System.Windows.Forms.TextBox id8;
        private System.Windows.Forms.TextBox id7;
        private System.Windows.Forms.TextBox id6;
        private System.Windows.Forms.TextBox id5;
        private System.Windows.Forms.TextBox id4;
        private System.Windows.Forms.TextBox id3;
        private System.Windows.Forms.TextBox id2;
        private System.Windows.Forms.Label IDlabel;
        private System.Windows.Forms.Label ProtocolLabel;
        private System.Windows.Forms.ComboBox ProtocolBox;
        private System.Windows.Forms.CheckBox AllPartsSetter;
        private System.Windows.Forms.NumericUpDown OffsetValue;
        private System.Windows.Forms.ComboBox ipAddress;
    }
}