﻿namespace GUIServer
{
    partial class StringSetterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StringSetterForm));
            this.filledText = new System.Windows.Forms.TextBox();
            this.setButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // filledText
            // 
            this.filledText.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.filledText.Location = new System.Drawing.Point(13, 13);
            this.filledText.Name = "filledText";
            this.filledText.Size = new System.Drawing.Size(630, 49);
            this.filledText.TabIndex = 0;
            // 
            // setButton
            // 
            this.setButton.Location = new System.Drawing.Point(649, 13);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(139, 49);
            this.setButton.TabIndex = 1;
            this.setButton.Text = "SET";
            this.setButton.UseVisualStyleBackColor = true;
            // 
            // StringSetterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 70);
            this.Controls.Add(this.setButton);
            this.Controls.Add(this.filledText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StringSetterForm";
            this.Text = "String setting";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox filledText;
        public System.Windows.Forms.Button setButton;
    }
}