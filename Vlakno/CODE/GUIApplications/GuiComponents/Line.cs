﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lights.Transport;
using Lights;

namespace GuiComponents
{
    public partial class Line<K,T> : UserControl
    {
        
        public Line()
        {
            InitializeComponent();
            
        }

      
        public K Index;

        public T Value;

        private void delete_Click(object sender, EventArgs e)
        {
           actionOfButton(this.Index,this.Value);
        }

        public delegate void ActionOfButton(K id, T val);

        ActionOfButton actionOfButton;
        

        public Line(K id,T value, string str, ActionOfButton act)
        {
            InitializeComponent();
            this.delete.Visible = true;
            this.Label.Text = str;
            this.Index = id;
            this.actionOfButton = act;
            this.Value = value;           
        }


        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.delete = new System.Windows.Forms.Button();
            this.Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // delete
            // 
            this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.delete.Location = new System.Drawing.Point(3, 3);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(80, 53);
            this.delete.TabIndex = 0;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // Label
            // 
            this.Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label.AutoSize = true;
            this.Label.Location = new System.Drawing.Point(89, 23);
            this.Label.Name = "Label";
            this.Label.Size = new System.Drawing.Size(36, 13);
            this.Label.TabIndex = 1;
            this.Label.Text = "Empty";
            // 
            // Line
            // 
            this.Controls.Add(this.Label);
            this.Controls.Add(this.delete);
            this.Name = "Line";
            this.Size = new System.Drawing.Size(496, 59);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Label Label;

    }
}
