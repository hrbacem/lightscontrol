﻿using Lights;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace GuiComponents
{
    public partial class Messenger : DockContent
    {


        //What to do with text
        public delegate void ClickButtonString(string txt);
        ClickButtonString actionOfButton;
        private List<string> data;

        /// <summary>
        /// Safety add data to datalist 
        /// </summary>
        /// <param name="msg">message to add</param>
        public void addToDataList(string msg)
        {
            lock (data)
                data.Add(msg);

        }

        /// <summary>
        /// Displayed text
        /// </summary>
        public IEnumerable<string> Data { get { return data; } }

        public Messenger(List<string> data, ClickButtonString act)
        {
            actionOfButton = act;
            this.data = data;

            InitializeComponent();

        }

        private void DataList_Load(object sender, EventArgs e)
        {
            lock (data)
                foreach (var item in data)
                {
                    this.listBox.Items.Add(item);
                }
        }



        private void sendMsg_Click(object sender, EventArgs e)
        {

            string message = textBox1.Text;
            textBox1.Clear();
            actionOfButton(message);


        }

        public void RefreshText()
        {
            lock (data)
                if (data.Count > this.listBox.Items.Count)
                {
                    for (int i = this.listBox.Items.Count; i < data.Count; i++)
                    {
                        this.listBox.Items.Add(data[i]);
                    }
                }
        }

        public void RefreshText(string msg)
        {

            this.listBox.Items.Add(msg);

        }


        private void listBox_ControlAdded(object sender, ControlEventArgs e)
        {
            throw new Exception();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string message = textBox1.Text;
                actionOfButton(message);
                textBox1.Clear();
            }
        }
    }
}
