﻿using Lights;
using Lights.EquipmentsKinds;
using Lights.Protocols;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GuiComponents
{
    public partial class AddNewEquipment : Form
    {
        public delegate void saveAction();

        public saveAction SaveAction;
        bool AddNickNameNotEquipment;

        public ID ID;

        public string NickName;

        public bool AllParts;

        public int Offset;

        public IProtocol Protocol = null;

        public IState State = null;

        public Dictionary<IID, IState> Data
        {
            get => data; set
            {
                ipAddress.Visible = true;
                data = value;
                updateID();
            }

        }

        Dictionary<IID, IState> data;

        private void updateID()
        {
            if (data == null) return;
            foreach (var item in data)
            {
                ipAddress.Items.Add(item.Key);
            }
            ipAddress.SelectedValueChanged += (s, e) => fillId();
        }
        private void fillId()
        {
            if (ipAddress.SelectedItem == null) return;

            ID id = (ID)ipAddress.SelectedItem;
            id0.Text = id.Parts[0].ToString();
            id1.Text = id.Parts[1].ToString();
            id2.Text = id.Parts[2].ToString();
            id3.Text = id.Parts[3].ToString();
            id4.Text = id.Parts[4].ToString();
            id5.Text = id.Parts[5].ToString();
            id6.Text = id.Parts[6].ToString();
            id7.Text = id.Parts[7].ToString();
            id8.Text = id.Parts[8].ToString();
            id9.Text = id.Parts[9].ToString();

        }

        public void Reload()
        {
            updateID();
        }

        public AddNewEquipment(bool addNickNameNotEquipment = true)
        {
            AddNickNameNotEquipment = addNickNameNotEquipment;

            InitializeComponent();
            if (AddNickNameNotEquipment)
                JustNickNameSetter();
            else
                ProtocolSetter();



        }


        private void JustNickNameSetter()
        {
            this.Text = "Set nickname";
            ProtocolBox.Visible = false;

            ipAddress.Visible = true;

            ProtocolLabel.Visible = false;
            SaveBtn.Text = "Add nickname";
        }

        private void ProtocolSetter()
        {
            AllPartsSetter.Visible = false;
            OffsetValue.Visible = false;
            ipAddress.Visible = false;
            ProtocolBox.Visible = true;
            ProtocolLabel.Visible = true;

            foreach (var item in Enum.GetValues(typeof(ProtocolFactory.ImplementedProtocols)))
            {
                ProtocolBox.Items.Add(item);
            }

        }



        bool idCorrect = false;
        private void setID()
        {

            byte[] id = new byte[ID.IDLength];
            idCorrect = true;
            if (!Byte.TryParse(id0.Text, out id[0])) idCorrect = false;
            if (!Byte.TryParse(id1.Text, out id[1])) idCorrect = false;
            if (!Byte.TryParse(id2.Text, out id[2])) idCorrect = false;
            if (!Byte.TryParse(id3.Text, out id[3])) idCorrect = false;
            if (!Byte.TryParse(id4.Text, out id[4])) idCorrect = false;
            if (!Byte.TryParse(id5.Text, out id[5])) idCorrect = false;
            if (!Byte.TryParse(id6.Text, out id[6])) idCorrect = false;
            if (!Byte.TryParse(id7.Text, out id[7])) idCorrect = false;
            if (!Byte.TryParse(id8.Text, out id[8])) idCorrect = false;
            if (!Byte.TryParse(id9.Text, out id[9])) idCorrect = false;
            if (!idCorrect) return;
            ID = new ID(id);
            IDClear();

        }

        public void IDClear()
        {
            id0.Text = "0";
            id1.Text = "0";
            id2.Text = "0";
            id3.Text = "0";
            id4.Text = "0";
            id5.Text = "0";
            id6.Text = "0";
            id7.Text = "0";
            id8.Text = "0";
            id9.Text = "0";
            ipAddress.SelectedItem = null;
        }

        private bool validID()
        {
            try
            {
                IPEndPoint ipTest = new IPEndPoint(this.ID.ToIPv4(), 6000);
                Socket testSocket = new Socket(ipTest.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                testSocket.Connect(ipTest);

            }
            catch (Exception ex)
            {
                
                return false;
            }
            return true;
        }


        private void button_Click(object sender, EventArgs e)
        {
            setID();

            if (!validID())
            {
                MessageBox.Show("Not valid ID in current context!");
                return;
            }

            NickName = nickName.Text;
            if(NickName=="")
            {
                MessageBox.Show("Empty name!");
                return;
            }
            
            nickName.Text = "";

            AllParts = AllPartsSetter.Checked;
            AllPartsSetter.Checked = true;

            Offset = (int)OffsetValue.Value;
            
       
            if (!AddNickNameNotEquipment)
            {
                var st = KindSerialization.GetState(ImplementedKinds.RGBLight, this.ID);

                if (null!=ProtocolBox.SelectedItem)
                Protocol = ProtocolFactory.GetProtocol((ProtocolFactory.ImplementedProtocols)ProtocolBox.SelectedItem);

                if (null !=Protocol)
                {

                    var kind = Protocol.KindOfEquipment;
                    State = KindSerialization.GetState(kind, this.ID);
                    if (Protocol != null)
                    {                                    
                        Protocol.DefaultMaxValue.CopyTo(State.Parts.Data, 0);
                    }
                }

            }
            SaveAction?.Invoke();

        }

        
    }
}
