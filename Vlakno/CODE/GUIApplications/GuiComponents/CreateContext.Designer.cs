﻿namespace GuiComponents
{
    partial class CreateContext
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateContext));
            this.button1 = new System.Windows.Forms.Button();
            this.NickList = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GroupList = new System.Windows.Forms.CheckedListBox();
            this.EquipmentList = new System.Windows.Forms.CheckedListBox();
            this.NicknamesBroswer = new System.Windows.Forms.Button();
            this.GroupsBroswer = new System.Windows.Forms.Button();
            this.EquipmentsBroswer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ContextBroswer = new System.Windows.Forms.Button();
            this.ContextList = new System.Windows.Forms.ListBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(17, 594);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(547, 72);
            this.button1.TabIndex = 0;
            this.button1.Text = "Save context";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SaveContext);
            // 
            // NickList
            // 
            this.NickList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NickList.CheckOnClick = true;
            this.NickList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NickList.FormattingEnabled = true;
            this.NickList.Location = new System.Drawing.Point(17, 144);
            this.NickList.Name = "NickList";
            this.NickList.Size = new System.Drawing.Size(547, 72);
            this.NickList.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Names of equipments";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Groups of equipments";
            // 
            // GroupList
            // 
            this.GroupList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GroupList.FormattingEnabled = true;
            this.GroupList.Location = new System.Drawing.Point(17, 268);
            this.GroupList.Name = "GroupList";
            this.GroupList.Size = new System.Drawing.Size(547, 72);
            this.GroupList.TabIndex = 4;
            // 
            // EquipmentList
            // 
            this.EquipmentList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EquipmentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EquipmentList.FormattingEnabled = true;
            this.EquipmentList.Location = new System.Drawing.Point(17, 392);
            this.EquipmentList.Name = "EquipmentList";
            this.EquipmentList.Size = new System.Drawing.Size(547, 72);
            this.EquipmentList.TabIndex = 5;
            // 
            // NicknamesBroswer
            // 
            this.NicknamesBroswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NicknamesBroswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NicknamesBroswer.Location = new System.Drawing.Point(306, 98);
            this.NicknamesBroswer.Name = "NicknamesBroswer";
            this.NicknamesBroswer.Size = new System.Drawing.Size(258, 40);
            this.NicknamesBroswer.TabIndex = 6;
            this.NicknamesBroswer.Text = "Browser";
            this.NicknamesBroswer.UseVisualStyleBackColor = true;
            this.NicknamesBroswer.Click += new System.EventHandler(this.NicknamesBroswer_Click);
            // 
            // GroupsBroswer
            // 
            this.GroupsBroswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupsBroswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GroupsBroswer.Location = new System.Drawing.Point(306, 222);
            this.GroupsBroswer.Name = "GroupsBroswer";
            this.GroupsBroswer.Size = new System.Drawing.Size(258, 40);
            this.GroupsBroswer.TabIndex = 7;
            this.GroupsBroswer.Text = "Browser";
            this.GroupsBroswer.UseVisualStyleBackColor = true;
            this.GroupsBroswer.Click += new System.EventHandler(this.GroupsBroswer_Click);
            // 
            // EquipmentsBroswer
            // 
            this.EquipmentsBroswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EquipmentsBroswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EquipmentsBroswer.Location = new System.Drawing.Point(306, 346);
            this.EquipmentsBroswer.Name = "EquipmentsBroswer";
            this.EquipmentsBroswer.Size = new System.Drawing.Size(258, 40);
            this.EquipmentsBroswer.TabIndex = 8;
            this.EquipmentsBroswer.Text = "Browser";
            this.EquipmentsBroswer.UseVisualStyleBackColor = true;
            this.EquipmentsBroswer.Click += new System.EventHandler(this.EquipmentsBroswer_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 346);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Equipments";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 470);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Context";
            // 
            // ContextBroswer
            // 
            this.ContextBroswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContextBroswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ContextBroswer.Location = new System.Drawing.Point(306, 470);
            this.ContextBroswer.Name = "ContextBroswer";
            this.ContextBroswer.Size = new System.Drawing.Size(258, 40);
            this.ContextBroswer.TabIndex = 12;
            this.ContextBroswer.Text = "Browser";
            this.ContextBroswer.UseVisualStyleBackColor = true;
            this.ContextBroswer.Click += new System.EventHandler(this.ContextBroswer_Click);
            // 
            // ContextList
            // 
            this.ContextList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContextList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ContextList.FormattingEnabled = true;
            this.ContextList.ItemHeight = 16;
            this.ContextList.Location = new System.Drawing.Point(17, 516);
            this.ContextList.Name = "ContextList";
            this.ContextList.Size = new System.Drawing.Size(547, 68);
            this.ContextList.TabIndex = 13;
            // 
            // nameBox
            // 
            this.nameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nameBox.Location = new System.Drawing.Point(17, 37);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(547, 38);
            this.nameBox.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 25);
            this.label5.TabIndex = 15;
            this.label5.Text = "Name of context";
            // 
            // CreateContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 678);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.ContextList);
            this.Controls.Add(this.ContextBroswer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.EquipmentsBroswer);
            this.Controls.Add(this.GroupsBroswer);
            this.Controls.Add(this.NicknamesBroswer);
            this.Controls.Add(this.EquipmentList);
            this.Controls.Add(this.GroupList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NickList);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CreateContext";
            this.Text = "Create context";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox NickList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox GroupList;
        private System.Windows.Forms.CheckedListBox EquipmentList;
        private System.Windows.Forms.Button NicknamesBroswer;
        private System.Windows.Forms.Button GroupsBroswer;
        private System.Windows.Forms.Button EquipmentsBroswer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ContextBroswer;
        private System.Windows.Forms.ListBox ContextList;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label label5;
    }
}