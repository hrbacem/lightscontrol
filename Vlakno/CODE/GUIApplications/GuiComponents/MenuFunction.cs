﻿using Lights.Comunication;
using Lights.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace GuiComponents
{
    public class MenuFunctions
    {

        ComunicationClass application;
        DockPanel docking;
        public MenuFunctions(ComunicationClass app, DockPanel dock)
        {
            application = app;
            docking = dock;
        }

        public void Reload()
        {

            if (nicknameDelete != null) nicknameDelete.ReloadBox();

            if (groupDelete != null) groupDelete.ReloadBox();
        }

        Data<string, GroupState> groupDelete;
        public void GroupsDeleter(object sender, EventArgs e)
        {

            if (groupDelete != null && !groupDelete.IsDisposed) { groupDelete.Show(docking, DockState.Float); return; }
            groupDelete = new Data<string, GroupState>(
                   application.Aliases.GroupMembers,
                   act: (id, val) => { application.Aliases.GroupMembers.Remove(id); application.DataCFO = true; application.Aliases.AliasesChanged?.Invoke(); },
                   t: (key, item) => key.ToString()

                   );
            groupDelete.ClosingAction += () => { groupDelete.Dispose(); groupDelete = null; };
            groupDelete.Show(docking, DockState.Float);
            groupDelete.Text = "Groups";
            groupDelete.Name = "Groups";

        }

        Data<string, PartNick> nicknameDelete;
        public void NicknamesDeleter(object sender, EventArgs e)
        {
            if (nicknameDelete != null && !nicknameDelete.IsDisposed) { nicknameDelete.Show(docking, DockState.Float); return; }
            nicknameDelete = new Data<string, PartNick>(
               application.Aliases.MembersNames,
               act: (id, val) => { application.Aliases.MembersNames.Remove(id); application.DataCFO = true; application.Aliases.AliasesChanged?.Invoke(); },
               t: (key, item) =>
               {
                   string val = key.ToString() + " - " + item.Id.ToString();
                   if (!item.All) val += "  " + item.Offset.ToString();
                   return val;
               }

               );
            nicknameDelete.ClosingAction += () => { nicknameDelete.Dispose(); nicknameDelete = null; };
            nicknameDelete.Show(docking, DockState.Float);
            nicknameDelete.Text = "Nicknames";
            nicknameDelete.Name = "Nicknames";
        }


        public void LoadGroups(object sender, EventArgs e)
        {
            if (FileManager.loadExistFileName(out string filename))
            {

                if (application.Aliases.LoadGroups(filename))
                    application.Aliases.AliasesChanged?.Invoke();
                else
                    MessageBox.Show("Not group file!");
            }

        }

        public void SaveGroups(object sender, EventArgs e)
        {
            if (FileManager.loadNotEmptyFileName(out string filename))
                application.Aliases.SaveGroups(filename);
        }

        


        public void AddNickname(object sender, EventArgs e)
        {
            AddNewEquipment aeq = new AddNewEquipment();
            aeq.Data = application.Data;
            aeq.SaveAction += () =>
            {
                application.Aliases.AddUniqueNick(aeq.NickName, aeq.ID, aeq.AllParts, aeq.Offset);
                application.Aliases.AliasesChanged?.Invoke();
            };
            aeq.Show();
        }

        public void NewContext(object sender, EventArgs e)
        {
            CreateContext contextCreator = new CreateContext();
            contextCreator.Show();
        }




        public void LoadNicknames(object sender, EventArgs e)
        {
            if (FileManager.loadExistFileName(out string filename))
            {
                if (application.Aliases.LoadNickNames(filename))
                    application.Aliases.AliasesChanged?.Invoke();
                else
                    MessageBox.Show("Not nickname file!");
            }


        }

        public void SaveNicknames(object sender, EventArgs e)
        {
            if (FileManager.loadNotEmptyFileName(out string filename))
                application.Aliases.SaveNickNames(filename);

        }





    }
}
