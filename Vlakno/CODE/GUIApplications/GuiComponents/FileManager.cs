﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiComponents
{
    public static class FileManager
    {
        public static bool loadExistFileName(out string filename)
        {
            filename = "";
            var opFile = new System.Windows.Forms.OpenFileDialog();
            opFile.ShowDialog();
            if (opFile.FileName == "") return false;
            if (!opFile.CheckFileExists) return false;
            filename = opFile.FileName;
            return true;
        }

        public static bool loadNotEmptyFileName(out string filename)
        {
            filename = "";
            var opFile = new System.Windows.Forms.OpenFileDialog();
            opFile.CheckFileExists = false;
            opFile.ShowDialog();
            if (opFile.FileName == "") return false;
            filename = opFile.FileName;
            return true;
        }
    }
}
