﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIServer
{
    public partial class StartSetting : Form
    {
        public IPAddress IP { get { IPAddress ip; IPAddress.TryParse(eqPort.Text,out ip); return ip; } }

        public int Port { get { int p; int.TryParse(userPort.Text,out p); return p; } }

        public string UserName { get => password.Text; }

        public bool Wait { get=>wait; }
        public bool Log { get=>log; }

        bool wait = true;
        bool log = false;

        SettingData setting;

        public StartSetting(SettingData setting)
        {
            InitializeComponent();
            this.setting = setting;

        }

      
        private void loginButton_Click(object sender, EventArgs e)
        {
                        
            int.TryParse(userPort.Text,out setting.PortUsers);
            int.TryParse(eqPort.Text, out setting.PortEquipments);
            setting.Password = password.Text;
            setting.Start = true;

            this.Close();
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            setting.Filled = true;

        }

    }
}
