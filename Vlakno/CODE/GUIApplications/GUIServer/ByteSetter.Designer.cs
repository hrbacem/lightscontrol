﻿namespace GUIServer
{
    partial class ByteSetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ByteSetter));
            this.TrackBarControl = new System.Windows.Forms.TrackBar();
            this.CurrentState = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarControl)).BeginInit();
            this.SuspendLayout();
            // 
            // TrackBarControl
            // 
            this.TrackBarControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TrackBarControl.Location = new System.Drawing.Point(37, 48);
            this.TrackBarControl.Name = "TrackBarControl";
            this.TrackBarControl.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrackBarControl.Size = new System.Drawing.Size(45, 333);
            this.TrackBarControl.TabIndex = 0;
            this.TrackBarControl.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // CurrentState
            // 
            this.CurrentState.AutoSize = true;
            this.CurrentState.Location = new System.Drawing.Point(47, 9);
            this.CurrentState.Name = "CurrentState";
            this.CurrentState.Size = new System.Drawing.Size(35, 13);
            this.CurrentState.TabIndex = 1;
            this.CurrentState.Text = "label1";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(22, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "APPLY";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ByteSetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(120, 422);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CurrentState);
            this.Controls.Add(this.TrackBarControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ByteSetter";
            this.Text = "Value";
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar TrackBarControl;
        private System.Windows.Forms.Label CurrentState;
        private System.Windows.Forms.Button button1;
    }
}