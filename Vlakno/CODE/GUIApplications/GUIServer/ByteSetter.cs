﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUIServer.EquipmentGUI;

namespace GUIServer
{
    public partial class ByteSetter : Form
    {
        

        public delegate void OnApply();
        public OnApply saveData;

        public byte Value;
        public ByteSetter(byte value, int max_value=255)
        {
            
            InitializeComponent();
            
            this.TrackBarControl.Maximum = max_value;
            this.TrackBarControl.Minimum = 0;
            this.TrackBarControl.Value = value;
            this.Value = value;
            this.CurrentState.Text = this.TrackBarControl.Value.ToString() + "\n" + ((100 * this.TrackBarControl.Value) / max_value) + "%";
            

        }

       

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            this.CurrentState.Text = this.TrackBarControl.Value.ToString()+"\n"+((100*this.TrackBarControl.Value)/255)+"%";
            Value = (byte)this.TrackBarControl.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveData?.Invoke();
            this.Close();
        }
    }
}
