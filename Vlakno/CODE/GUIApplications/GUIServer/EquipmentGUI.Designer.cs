﻿using System.Windows.Forms;


namespace GUIServer
{
    partial class EquipmentGUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            //this.visualStudioToolStripExtender1 = new WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender(this.components);
            this.nameOfEquipment = new System.Windows.Forms.Label();
            this.importanceOfController = new System.Windows.Forms.Label();
            this.protocolOFEquipment = new System.Windows.Forms.Label();
            this.PartsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // visualStudioToolStripExtender1
            // 
            //this.visualStudioToolStripExtender1.DefaultRenderer = null;
            // 
            // nameOfEquipment
            // 
            this.nameOfEquipment.AutoSize = true;
            this.nameOfEquipment.Location = new System.Drawing.Point(8, 10);
            this.nameOfEquipment.Name = "nameOfEquipment";
            this.nameOfEquipment.Size = new System.Drawing.Size(18, 13);
            this.nameOfEquipment.TabIndex = 0;
            this.nameOfEquipment.Text = "ID";
            // 
            // importanceOfController
            // 
            this.importanceOfController.AutoSize = true;
            this.importanceOfController.Location = new System.Drawing.Point(8, 34);
            this.importanceOfController.Name = "importanceOfController";
            this.importanceOfController.Size = new System.Drawing.Size(38, 13);
            this.importanceOfController.TabIndex = 1;
            this.importanceOfController.Text = "Priority";
            // 
            // protocolOFEquipment
            // 
            this.protocolOFEquipment.AutoSize = true;
            this.protocolOFEquipment.Location = new System.Drawing.Point(8, 59);
            this.protocolOFEquipment.Name = "protocolOFEquipment";
            this.protocolOFEquipment.Size = new System.Drawing.Size(46, 13);
            this.protocolOFEquipment.TabIndex = 2;
            this.protocolOFEquipment.Text = "Protocol";
            // 
            // PartsLayout
            // 
            this.PartsLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PartsLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PartsLayout.Location = new System.Drawing.Point(177, 8);
            this.PartsLayout.Name = "PartsLayout";
            this.PartsLayout.Size = new System.Drawing.Size(544, 64);
            this.PartsLayout.TabIndex = 3;
            this.PartsLayout.WrapContents = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(727, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 64);
            this.button1.TabIndex = 4;
            this.button1.Text = "DELETE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // EquipmentGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.PartsLayout);
            this.Controls.Add(this.protocolOFEquipment);
            this.Controls.Add(this.importanceOfController);
            this.Controls.Add(this.nameOfEquipment);
            this.Name = "EquipmentGUI";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Size = new System.Drawing.Size(824, 80);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender visualStudioToolStripExtender1;
        private System.Windows.Forms.Label nameOfEquipment;
        private System.Windows.Forms.Label importanceOfController;
        private System.Windows.Forms.Label protocolOFEquipment;
        public System.Windows.Forms.FlowLayoutPanel PartsLayout;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button button1;
    }
}
