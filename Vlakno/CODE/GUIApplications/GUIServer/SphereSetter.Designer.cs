﻿using RealTimeControl;

namespace GUIServer
{
    partial class SphereSetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SphereSetter));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.joyStickController = new RealTimeControl.WpfJoystick();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.modelOfLight = new RealTimeControl.Model();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(393, 3);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(410, 394);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "Joystick";
            this.elementHost1.ChildChanged += new System.EventHandler<System.Windows.Forms.Integration.ChildChangedEventArgs>(this.elementHost1_ChildChanged);
            this.elementHost1.VisibleChanged += new System.EventHandler(this.elementHost1_VisibleChanged);
            this.elementHost1.Child = this.joyStickController;
            // 
            // elementHost2
            // 
            this.elementHost2.Location = new System.Drawing.Point(12, 12);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(375, 375);
            this.elementHost2.TabIndex = 1;
            this.elementHost2.Text = "Model";
            this.elementHost2.Child = this.modelOfLight;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 364);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(375, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "APPLY";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SphereSetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 399);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.elementHost2);
            this.Controls.Add(this.elementHost1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            this.Name = "SphereSetter";
            this.Text = "Dirrection setting";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private WpfJoystick joyStickController;
        
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private Model modelOfLight;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
    }
}