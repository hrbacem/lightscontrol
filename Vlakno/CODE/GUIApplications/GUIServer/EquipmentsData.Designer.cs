﻿namespace GUIServer
{
    partial class EquipmentsData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EquipmentsData));
            this.CurrentControlledEq = new System.Windows.Forms.FlowLayoutPanel();
            this.RealTimeControl = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CurrentControlledEq
            // 
            this.CurrentControlledEq.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CurrentControlledEq.AutoScroll = true;
            this.CurrentControlledEq.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.CurrentControlledEq.Location = new System.Drawing.Point(2, 40);
            this.CurrentControlledEq.Name = "CurrentControlledEq";
            this.CurrentControlledEq.Size = new System.Drawing.Size(790, 406);
            this.CurrentControlledEq.TabIndex = 0;
            this.CurrentControlledEq.WrapContents = false;
            // 
            // RealTimeControl
            // 
            this.RealTimeControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RealTimeControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RealTimeControl.Location = new System.Drawing.Point(0, 0);
            this.RealTimeControl.Name = "RealTimeControl";
            this.RealTimeControl.Size = new System.Drawing.Size(792, 34);
            this.RealTimeControl.TabIndex = 5;
            this.RealTimeControl.Text = "Open interactive mode";
            this.RealTimeControl.UseVisualStyleBackColor = true;
            this.RealTimeControl.Click += new System.EventHandler(this.realTimeSetter);
            // 
            // EquipmentsData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 446);
            this.Controls.Add(this.RealTimeControl);
            this.Controls.Add(this.CurrentControlledEq);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EquipmentsData";
            this.Text = "Main";
            this.SizeChanged += new System.EventHandler(this.Main_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel CurrentControlledEq;
        private System.Windows.Forms.Button RealTimeControl;
    }
}