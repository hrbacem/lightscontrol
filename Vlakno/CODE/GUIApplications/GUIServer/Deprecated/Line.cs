﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lights.Transport;
using Lights;

namespace GUIServer
{
    public partial class Line : UserControl
    {
        
        public Line()
        {
            InitializeComponent();
        }

        public IID Index;

        private void delete_Click(object sender, EventArgs e)
        {
           actionOfButton(this.Index);
        }

        public delegate void ActionOfButton(IID id);

        ActionOfButton actionOfButton;
        public static Line newLine(IID id, string str, ActionOfButton act)
        {
            Line newLine = new Line();
            newLine.delete.Visible=true;
            newLine.Label.Text = str;
            newLine.Index = id;
            newLine.actionOfButton = act;
            return newLine;

        }

        




        private void Line_Load(object sender, EventArgs e)
        {

        }
    }
}
