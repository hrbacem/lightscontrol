﻿using GuiComponents;
using Lights;
using Lights.EquipmentsKinds;
using Lights.Protocols;
using Lights.Transport;
using RealTimeControl;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;




namespace GUIServer
{
    public delegate void deleteAction(EquipmentGUI controller);
    public delegate void updateAction();
    public delegate void reload(IState state);

    public interface IEquipment
    {
        deleteAction callOnDelete { get; set; }

        updateAction callOnUpdate { get; set; }
        UserAction WasChanged { get; set; }
        reload Reload { get; set; }
        List<Control> Controled { get; }

        void UpdateWidth(int width);

        FlowLayoutPanel LayoutOfParts { get; }
    }
    public partial class EquipmentGUI : UserControl, IEquipment
    {
        public IID Index;

        public deleteAction callOnDelete { get; set; }

        public updateAction callOnUpdate { get; set; }
        public UserAction WasChanged { get; set; }
        public reload Reload { get; set; }
        public List<Control> Controled { get; }
        public FlowLayoutPanel LayoutOfParts { get => PartsLayout; }

        /// <summary>
        /// How to reload this equipment.
        /// </summary>
        /// <param name="state">IState with actual data.</param>


        public bool delete = false;

        CreateParts PartsFactory;

        public EquipmentGUI(IState state, IProtocol protocol = null)
        {

            PartsFactory = new CreateParts(this);
            PartsFactory.Parent = this;
            Index = state.Id;
            Controled = new List<Control>();
            InitializeComponent();
            setInformation(state, protocol);

            dynamic st = Convert.ChangeType(state, state.GetType());
            
            PartsFactory.makeVisual(st);
            
        }

        public void setInformation(IState state, IProtocol protocol)
        {
            nameOfEquipment.Text = state.Id.ToString();

                     

            importanceOfController.Text = "Importance "+state.Importance.Importance.ToString();
            if (protocol != null)
            {
                protocolOFEquipment.Text = protocol.GetType().ToString();
            }
            else
            {
                protocolOFEquipment.Text = "Unknown";
            }
        }


        public void update(IState state,IProtocol protocol)
        {
            setInformation(state, protocol);
            dynamic stateReal = Convert.ChangeType(state, state.GetType());
            PartsFactory.Parent = this;
            PartsFactory.updateVisual(stateReal);
        }


        private void buttonDelete_Click(object sender, EventArgs e)
        {
            delete = true;
            callOnDelete?.Invoke(this);
        }

     
        public void UpdateWidth(int width)
        {
            this.Width = width;
        }
    }

    public interface ICreateParts
    {
        void updateVisual(IState state);
        void makeVisual(IState state);

        

        IEquipment Parent{ get; set; }
    }

    public class CreateParts : ICreateParts
    {

        public CreateParts(IEquipment Parent)
        {
            this.Parent = Parent;
        }


        public IEquipment Parent { get; set; }
       
        public void updateVisual(IState state)
        {
            for (int i = 0; i < Parent.LayoutOfParts.Controls.Count ; i++)
            {
                ((StateProgressBar)Parent.LayoutOfParts.Controls[i]).Value = state.Parts.Data[i];
            }

        }
        public void updateVisual(State<RGBLight> state)
        {
            StateButton PB = (StateButton)Parent.LayoutOfParts.Controls[0];
            PB.StateOfButton = state;
            PB.BackColor = Color.FromArgb(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
        }

        public void updateVisual(State<SpyderLight> state)
        {
            StateButton PB = (StateButton)Parent.LayoutOfParts.Controls[0];
            PB.StateOfButton = state;
            PB.BackColor = Color.FromArgb(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);

            StateModel stateModel = (StateModel)Parent.LayoutOfParts.Controls[1];
            stateModel.StateOfEquipment = state;
            
            stateModel.WpfModel.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            stateModel.WpfModel.moveLightTo(stateModel.StateOfEquipment.Parts.Data[4] - 90, stateModel.StateOfEquipment.Parts.Data[3] - 90);

        }

        private void makeVisual(object state)
        {
            Label NL = new Label();
            NL.Text = state.ToString();
            Parent.LayoutOfParts.Controls.Add(NL);
            Parent.Reload += makeVisual;
        }
        public void makeVisual(IState state)
        {
            Parent.LayoutOfParts.Controls.Clear();
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                StateProgressBar PB = new StateProgressBar();

                // PB.Style = ProgressBarStyle.Continuous;
                PB.Minimum = 0;
                PB.Maximum = 256;
                PB.Width = PB.Height;
                PB.Height = this.Parent.LayoutOfParts.Height;

                PB.Enabled = true;
                PB.Value = state.Parts.Data[i];
                PB.Update();
                Parent.LayoutOfParts.Controls.Add(PB);
                PB.StateOfEquipment = state;

                PB.index = i;
                PB.Click += new System.EventHandler(SetValue);
                Parent.Reload += updateVisual;

                

            }
        }

        public class StateProgressBar : VerticalProgressBar
        {
            public IState StateOfEquipment;
            public int index;
        }

        public class StateModel : RealTimeControl.ModelForm
        {
            public StateModel() : base() { }
            //public StateModel(int x, int y) : base(x, y) { }

            public IState StateOfEquipment;

            public int index;
        }

      

        public void makeVisual(State<SpyderLight> state)
        {
            StateButton PB;
            StateModel stateModel;
            {
                PB = new StateButton();
                PB.BackColor = Color.FromArgb(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
                PB.Visible = true;
                PB.Enabled = true;
                PB.Update();
                PB.Height = this.Parent.LayoutOfParts.Height - 6;
                PB.Width = this.Parent.LayoutOfParts.Height - 6;
                PB.Click += new System.EventHandler(SetColor);
                Parent.Reload += (actualState => {
                    PB.StateOfButton = state;
                    PB.BackColor = Color.FromArgb(actualState.Parts.Data[0], actualState.Parts.Data[1], actualState.Parts.Data[2]);
                });
                PB.StateOfButton = state;
                Parent.LayoutOfParts.Controls.Add(PB);
                PB.Text = "COLOR";

            }

            {
                
                stateModel = new StateModel(); // Place to define move of light
                
                
               stateModel.Width= this.Parent.LayoutOfParts.Height - 6;
                stateModel.Height = this.Parent.LayoutOfParts.Height - 6;

                // Keep logic of button, but use it to call SetAngle with model
                stateModel.WpfModel.AddModelTouchAction( new System.Windows.RoutedEventHandler((s, a) => SetAngle(stateModel, a)));

                stateModel.StateOfEquipment = state;
                stateModel.WpfModel.moveLightTo(stateModel.StateOfEquipment.Parts.Data[3] - 90, stateModel.StateOfEquipment.Parts.Data[4] - 90);

                stateModel.WpfModel.ButtonEnable(true);
                
                Parent.Reload += ((actualState) => {
                    stateModel.WpfModel.moveLightTo(stateModel.StateOfEquipment.Parts.Data[3] - 90, stateModel.StateOfEquipment.Parts.Data[4] - 90);
                    
                    stateModel.WpfModel.LightColor = System.Windows.Media.Color.FromRgb(actualState.Parts.Data[0], actualState.Parts.Data[1], actualState.Parts.Data[2]);
                });


                Parent.LayoutOfParts.Controls.Add(stateModel);
            }
        }
        public void makeVisual(State<RGBLight> state)
        {
            StateButton PB = new StateButton();
            PB.BackColor = Color.FromArgb(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            PB.Visible = true;
            PB.Enabled = true;
            PB.Update();
            PB.Height = this.Parent.LayoutOfParts.Height-6;
            PB.Width = this.Parent.LayoutOfParts.Height-6;
            PB.Click += new System.EventHandler(SetColor);
            PB.StateOfButton = state;
            Parent.LayoutOfParts.Controls.Add(PB);
            PB.Text = "COLOR";
            Parent.Reload += (actualState => {
                PB.StateOfButton = state;
                PB.BackColor = Color.FromArgb(actualState.Parts.Data[0], actualState.Parts.Data[1], actualState.Parts.Data[2]);
            });
            Parent.Controled.Add(PB);
        }
        private void SetColor(object sender, EventArgs e)
        {
            ColorDialog CD = new ColorDialog();
            CD.ShowDialog();
            StateButton B = (StateButton)sender;

            B.BackColor = CD.Color;
            B.StateOfButton.Parts.Data[0] = CD.Color.R;
            B.StateOfButton.Parts.Data[1] = CD.Color.G;
            B.StateOfButton.Parts.Data[2] = CD.Color.B;
            B.StateOfButton.PartImportance.Set(0,true);
            B.StateOfButton.PartImportance.Set(1, true);
            B.StateOfButton.PartImportance.Set(2, true);
            Parent.Reload(B.StateOfButton);
            Parent.callOnUpdate?.Invoke();
            Parent.WasChanged?.Invoke(null);
        }
        private void SetValue(object sender, EventArgs e)
        {
            StateProgressBar SPB = (StateProgressBar)sender;
            ByteSetter BS = new ByteSetter(SPB.StateOfEquipment.Parts.Data[SPB.index]);
            BS.saveData =
                () => {
                    SPB.StateOfEquipment.Parts.Data[SPB.index] = BS.Value;
                    SPB.StateOfEquipment.PartImportance[SPB.index] = true; 
                    SPB.Value = BS.Value;
                    Parent.callOnUpdate?.Invoke();
                    Parent.WasChanged?.Invoke(null);
                    Parent.Reload(SPB.StateOfEquipment);
                };

            BS.Show();

        }

        private void SetAngle(object sender, EventArgs e)
        {
            StateModel stateModel = (StateModel)sender;
            SphereSetter SS = new SphereSetter(stateModel.WpfModel, stateModel.StateOfEquipment);
            SS.saveData =
                () => {
                    Parent.Reload(stateModel.StateOfEquipment);
                    Parent.callOnUpdate?.Invoke();
                    Parent.WasChanged?.Invoke(null);
                };

           SS.Show();
            
        }


        public class StateButton : Button
        {
            public IState StateOfButton;
        }




    }
}
