﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Lights.Transport;
using Lights;
using System.Threading;
using Lights.Comunication;

namespace GUIServer
{
    /// <summary>
    /// Basic data to start server,
    /// Has to be get from user
    /// </summary>
    public class SettingData
    {
        public int PortUsers;
        public int PortEquipments;
        public string Password;
        public bool Start=false;
        public bool Filled = false;
    }

    static class Program
    {


        /// <summary>
        /// Hlavní vstupní bod aplikace.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            /// Basic data container
            SettingData set = new SettingData();
            
            /// Login 
            var login = new StartSetting(set);
            Application.Run(login);
           
            /// Check if user is going to start server
            if (set.Start)
            {
                /// Create correct server
                var server = new Server(set.PortUsers, set.PortEquipments);
                server.ServerPassword = set.Password;
                 
                ///Send message to all users
                server.SendBroadCast(new Lights.Messages.IntroducingServer());
               
                ///Start GUI SERVER
                Application.Run(new GUIServerForm(server));
                server.Shutdown();
            }   
          
            
        }

        /// <summary>
        /// For Console start
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
    }
}