﻿using Lights;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lights.Messages;
using Lights.Comunication;
using Lights.Protocols;
using RealTimeControl;

namespace GUIServer
{
    public partial class EquipmentsData : DockContent
    {

        /// <summary>
        /// server for comunication
        /// </summary>
        Server server;
        public UserAction DataWasChanged { get; set; }
        
        Dictionary<IID, IState> Data;

        public EquipmentsData(Dictionary<IID, IState> data, Server server)
        {

            this.server = server;
            Data = data;
            InitializeComponent();
            ReloadData();
            Main_SizeChanged(null,null);
            DataWasChanged += DataWasChangedRealTime;

        }

        /// <summary>
        /// Create or Rereate view from actual Data
        /// This Function is recreate all Equipments
        /// </summary>
        public void ReloadData(Scene scene = null)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            
            
            //Invoke all Update function
            DataWasChanged?.Invoke(null);
            //clear old view  
            CurrentControlledEq.Controls.Clear();
            lock (server.Data)
                foreach (var item in server.Data)
                {
                    server.Map.TryGetValue(item.Key, out IProtocol protocol);
                    EquipmentGUI eq = new EquipmentGUI(item.Value, protocol)
                    {
                        callOnDelete = DelteEq
                    };
                    eq.WasChanged += (s) => { server.DataCFO = true; };
                    eq.callOnUpdate +=  server.DataWasChangedFromServerPlatform;
                    eq.Width = CurrentControlledEq.Width;

                    CurrentControlledEq.Controls.Add(eq);
                }
            DataWasChanged?.Invoke(null);
            Cursor.Current = cursor;
        }

        /// <summary>
        /// Reload view from actual Data
        /// Just actualize data 
        /// </summary>
        public void ReloadChangedData()
        {

            DataWasChanged?.Invoke(null);
            //Has to remove data deleted data
            List<EquipmentGUI> toDelete = new List<EquipmentGUI>();
            //Has to show new data 
            List<EquipmentGUI> toAdd = new List<EquipmentGUI>();
            List<IID> displayed = new List<IID>();
            lock (Data)
                foreach (EquipmentGUI item in CurrentControlledEq.Controls)
                {
                    //showed item is marked to delete
                    if (item.delete)
                    {
                        Data.Remove(item.Index); //remove from data on server
                        toDelete.Add(item);
                    }
                    else
                    {
                        Data.TryGetValue(item.Index, out IState newState);
                        if (newState != null)
                        {
                            server.Map.TryGetValue(newState.Id, out IProtocol protocol);
                            item.update(newState, protocol); // View werent created or was deleted
                            displayed.Add(item.Index);
                        }
                        else
                        {
                            // deleted from Data
                            toDelete.Add(item);
                        }
                    }
                }

            var comparer = new ID.EqualityComparer();
            foreach (var item in Data)
            {
                
                if (!displayed.Contains(item.Key, comparer))
                {
                    server.Map.TryGetValue(item.Key, out IProtocol protocol);
                    EquipmentGUI eq = new EquipmentGUI(item.Value, protocol)
                    {
                        callOnDelete = DelteEq
                    };
                    eq.WasChanged += (s) => { server.DataCFO = true; };
                    eq.callOnUpdate += server.DataWasChangedFromServerPlatform;
                    eq.Width = CurrentControlledEq.Width;

                    CurrentControlledEq.Controls.Add(eq);
           
                }
            }
            
            Main_SizeChanged(null, null);
        }

        private void DelteEq(EquipmentGUI controller)
        {
            CurrentControlledEq.Controls.Remove(controller);
            Data.Remove(controller.Index);
            server.DataCFO = true;
        }

      
        private void DataWasChangedRealTime(object sender)
        {
            if (RealTimeSetter != null && RealTimeVisible)
                RealTimeSetter.DataWasChanged?.Invoke(sender);
        }

        RealTimeSetting RealTimeSetter = null;
        bool RealTimeVisible = false;
        private void realTimeSetter(object sender, EventArgs e)
        {
            if(RealTimeSetter == null)
            {
                
                var cursor = Cursor.Current;
                RealTimeVisible = true;
                Cursor.Current = Cursors.WaitCursor;
                RealTimeSetter = new RealTimeSetting(server.Data, server.Aliases, (s) => { server.DataCFO = true; server.DataWasChangedFromServerPlatform(); });
                RealTimeSetter.Visible = RealTimeVisible;
                RealTimeSetter.actionOnUserFormClosing += (ev) =>
                {
                    // Stop closing, just hide
                    RealTimeVisible = false;
                    RealTimeSetter.Visible = RealTimeVisible;
                    RealTimeSetter.Hide();
                    ev.Cancel = true;
                };

                RealTimeSetter.Show();
                RealTimeSetter.Disposed += (se,ev) => { RealTimeSetter = null; };
                Cursor.Current = cursor;
                
            }
            else
            {
                RealTimeVisible = true;
                RealTimeSetter.Visible = RealTimeVisible;
                RealTimeSetter.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                RealTimeSetter.Show();
                DataWasChanged(this);
            }

        }


        
        private void Main_SizeChanged(object sender, EventArgs e)
        {
            changeSize();
        }

        bool scrollEnable = false;
        public void changeSize()
        {
            int width = CurrentControlledEq.Width;
            if (CurrentControlledEq.VerticalScroll.Visible)
             scrollEnable = true;
            if (scrollEnable|| CurrentControlledEq.VerticalScroll.Visible)
                width -= System.Windows.Forms.SystemInformation.VerticalScrollBarWidth+6; 

            foreach (var item in CurrentControlledEq.Controls)
            {
                if (item is IEquipment)
                    ((IEquipment)item)?.UpdateWidth(width);
            }
        }
    }
}
