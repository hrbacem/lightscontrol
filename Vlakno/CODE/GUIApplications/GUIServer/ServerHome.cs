﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GuiComponents;
using Lights.Transport;
using Lights;


namespace GUIServer
{
    using WeifenLuo.WinFormsUI.Docking;
    using WeifenLuo;
    using Lights;
    using Lights.Transport;
    using Lights.Comunication;
    using Lights.Protocols;
    using Lights.Parsers;

    public partial class GUIServerForm : Form
    {
        Server server;
        /// <summary>
        /// Window with messages
        /// </summary>
        Messenger chat;
        Messenger errors;
        Data<IID,string> connected;
        Data<IID,IProtocol> map;
        EquipmentsData DataToLights;
        MenuFunctions menuSupport;
       

        public GUIServerForm(Server server)
        {
            //server, 
            this.server = server;

            InitializeComponent();
            updateViewTimer.Interval = 500;
            /// Create docking system
            this.dockPanel1.Theme = new VS2015BlueTheme();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            dockPanel1.DocumentStyle = DocumentStyle.DockingWindow;
            dockPanel1.AllowEndUserDocking = true;
            dockPanel1.AllowEndUserNestedDocking = true;
            this.Controls.Add(dockPanel1);

            /// Chat setting
            {
                chat = new Messenger(server.Conversation, msg => { server.MessageToAll("Server :" + msg); chat.addToDataList("Server: " + msg); });

                chat.Show(dockPanel1, DockState.DockRight);
                chat.Name = "Chat";
                chat.Text = "Chat";
            }

            /// Error setting
            {
                errors = new Messenger(server.Errors, msg => { errors.addToDataList("Server: " + msg); });
                errors.Show(dockPanel1, DockState.DockRightAutoHide);
                errors.Name = "Information";
                errors.Text = "Information";
            }

            /// Connected Users
            {
                connected = new Data<IID,string>(server.Connected,
                     (id, val) => { server.Connected.Remove(id); },
                     (key, item) => item.ToString() + "\n" + key.ToString());

                connected.Show(dockPanel1, DockState.DockLeft);
                connected.Text = "Clients";
                connected.Name = "Clients";

            }

            /// Map of equipment
            {
                map = new Data<IID, IProtocol>(
                   server.Map,
                   act: (id, val) => { server.Map.Remove(id); server.DataCFO = true; },
                   t: (key, item) => key.ToString() + " use protocol: " + item.ToString()
                   );

                map.Show(dockPanel1, DockState.DockBottom);
                map.Text = "Protocols";
                map.Name = "Protocols";
            }

           

            /// Data
            {
                DataToLights = new EquipmentsData(server.Data, server)
                {
                    AutoScaleMode = AutoScaleMode.Font,
                    Text = "Equipments states",
                    Name = "Equipments states"
                };
                DataToLights.Show(dockPanel1, DockState.Document);
            }

            menuSupport = new MenuFunctions(server, dockPanel1);

            updateViewTimer.Start();

        }

       

        /// <summary>
        /// Change View of app. Time to possibility of reguling layout. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateTimeLayout(object sender, EventArgs e)
        {
            if (server.DataCFO)
            {
                server.DataCFO = false;
                this.DataToLights.ReloadChangedData();
               
            }
                
            this.chat.RefreshText();            
            this.errors.RefreshText();
            this.connected.ReloadBox();
            this.map.ReloadBox();
            menuSupport.Reload();


        }



       

       
        private void informationToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Equipment's port: " + server.EquipmentPort);
            sb.AppendLine("User's port: " + this.server.ClientPort);
            sb.AppendLine("Server IP address: " + this.server.ServerID.ToString());
            sb.AppendLine("Current password: \"" + server.ServerPassword + "\"");
            MessageBox.Show(sb.ToString());
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var setter = new StringSetterForm();
            setter.Text = "CHANGE PASSWORD";
            setter.setButton.Click += (send, ev) => { this.server.ServerPassword = setter.filledText.Text; setter.Close(); };
            setter.Show();
        }






        private void saveMap(object sender, EventArgs e)
        {
            if (FileManager.loadNotEmptyFileName(out string filename))
                lock (server.Map)
                {
                    if (DefultExtractorMap.TrySaveProtocolData(filename, server.Map))
                        return;
                    MessageBox.Show("Data not saved!");

                }
        }

        private void loadEquipments(object sender, EventArgs e)
        {
            if (FileManager.loadExistFileName(out string fileName))
            {
                if (!LoadEquipment(fileName))
                {
                    MessageBox.Show("Equipments not loaded!");
                }
            }

        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewEquipment aeq = new AddNewEquipment(false);
            aeq.Data = server.Data;

            aeq.SaveAction += () =>
            {
                server.Aliases.AddUniqueNick(aeq.NickName, aeq.ID, aeq.AllParts, aeq.Offset);


                if (aeq.State != null)
                    if (!server.Data.ContainsKey(aeq.ID))
                    {

                        server.Data.Add(aeq.ID, aeq.State);
                    }

                if (aeq.Protocol != null)
                    if (!server.Map.ContainsKey(aeq.ID))
                    {
                        server.Map.Add(aeq.ID, aeq.Protocol);
                    }
                server.DataCFO = true;
                server.Aliases.AliasesChanged?.Invoke();
            };
            aeq.Show();
        }



        private void loadContextFile(object sender, EventArgs e)
        {

            if (FileManager.loadExistFileName(out string fileName))
            {
                if (contextLoader(fileName))

                    return;
                else
                    MessageBox.Show("Not context file!");
            }

        }

        private bool contextLoader(string filename)
        {
            var cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            Context contextToLoad = new Context();

            if (!contextToLoad.LoadContext(filename))
            {
                Cursor.Current = cursor;
                return false;
            }
            if (contextToLoad.Nicknames == null) return false;
            if (contextToLoad.Groups == null) return false;
            if (contextToLoad.Equipments == null) return false;
            foreach (var item in contextToLoad.Nicknames)
            {
                server.Aliases.LoadNickNames(item);
            }

            foreach (var item in contextToLoad.Groups)
            {
                server.Aliases.LoadGroups(item);
            }
            foreach (var item in contextToLoad.Equipments)
            {
                LoadEquipment(item);

            }

            

            server.DataCFO = true;
            Cursor.Current = cursor;
            server.Aliases.AliasesChanged?.Invoke();
            server.DataWasChangedFromServerPlatform();
            return true;
        }

        private bool LoadEquipment(string filename)
        {

            if (
            DefultExtractorMap.TryReadProtocolData(filename, out Dictionary<IID, IProtocol> map, out Dictionary<IID, IState> vals)
            )
            {

                lock (server.Map)
                {
                    foreach (var item in map)
                    {
                        if (!server.Map.ContainsKey(item.Key))
                            server.Map.Add(item.Key, item.Value);
                    }
                }

                lock (server.Data)
                {
                    foreach (var item in vals)
                    {
                        if (!server.Data.ContainsKey(item.Key))
                            server.Data.Add(item.Key, item.Value);
                    }

                }
                server.DataCFO = true;
                return true;

            }
            return false;
        }












        private void createNewContext(object sender, EventArgs e)
        {
            menuSupport.NewContext(sender, e);
        }

        private void loadNicknamesMenu(object sender, EventArgs e)
        {
            menuSupport.LoadNicknames(sender,e);
        }

        private void saveCurrentNicknamesMenu(object sender, EventArgs e)
        {
            menuSupport.SaveNicknames(sender, e);

        }

        private void loadGroupMenu(object sender, EventArgs e)
        {
            menuSupport.LoadGroups(sender,e);
        }

        private void saveCurrentGroupsMenu(object sender, EventArgs e)
        {
            menuSupport.SaveGroups(sender, e);
        }

       

        private void addNicknameMenu(object sender, EventArgs e)
        {
            menuSupport.AddNickname(sender, e);
        }

        private void deleteGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            menuSupport.GroupsDeleter(sender, e);

        }

        private void deleteNicknamesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            menuSupport.NicknamesDeleter(sender, e);
        }
    }
}




