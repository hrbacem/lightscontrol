﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUIServer.EquipmentGUI;
using Lights;
using Lights.Transport;
using GuiComponents;
using RealTimeControl;

namespace GUIServer
{
    public partial class SphereSetter : Form
    {

        public delegate void OnApply();
        public OnApply saveData;

        public SphereSetter(Model axis, IState StateOfEquipment)
        {

            InitializeComponent();
            this.joyStickController.enableTouch(false);
            this.state = StateOfEquipment;
            model = axis;
            
            this.joyStickController.setX(state.Parts.Data[3] - 90);
            this.joyStickController.setY(state.Parts.Data[4] - 90);
            modelOfLight.moveLightTo(joyStickController.getY(), joyStickController.getX());
            this.modelOfLight.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            this.timer1.Start();
        }
        Model model;
        IState state;

        private void reload()
        {

            if (model != null)
            {
                modelOfLight.moveLightTo(joyStickController.getY(), joyStickController.getX());
                this.modelOfLight.LightColor = System.Windows.Media.Color.FromRgb(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);

            }


        }
        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {
            reload();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            state.Parts.Data[3] = (byte)(joyStickController.getX() + 90);
            state.Parts.Data[4] = (byte)(joyStickController.getY() + 90);
            state.PartImportance.Set(3,true);
            state.PartImportance.Set(4, true);
            model.moveLightTo(joyStickController.getY(), joyStickController.getX());
            model.SetColor(state.Parts.Data[0], state.Parts.Data[1], state.Parts.Data[2]);
            saveData?.Invoke();
            this.Close();
        }

        private void elementHost1_VisibleChanged(object sender, EventArgs e)
        {
            reload();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            reload();
        }
    }
}
