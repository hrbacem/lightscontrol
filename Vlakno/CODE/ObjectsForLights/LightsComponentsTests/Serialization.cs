using Lights.EquipmentsKinds;
using Lights.Messages;
using Lights.Transport;
using NUnit.Framework;
using System;

namespace Tests
{
    public class Tests
    {

        [TestCase("jmeno")]
        [TestCase("")]
        [TestCase("jme3&$25345&$5345&$")]
        public void ChatMessage(string str)
        {
            Chat send = new Chat(str);
            byte[] bytes = send.ToBytes();
            Chat reciev = new Chat(bytes);
            Assert.AreEqual(send.Text, reciev.Text);
        }

        [TestCase("","")]
        [TestCase("jmeno","")]
        [TestCase("", "nick")]
        [TestCase("jmeno", "nick")]
        [TestCase("jme3&$2534jme3&$25345&$5345&$5&$5345&$", "jme3&$jme3&$25345&$5345&$25345&$5345&$")]
        [TestCase("jme3&$25345&$5345&$","")]
        [TestCase("", "jme3&$jme3&$25345&$5345&$25345&$5345&$")]
        public void LogInMessage(string nick,string password)
        {
            LogIn send = new LogIn(nick,password);
            byte[] bytes = send.ToBytes();
            LogIn reciev = new LogIn(bytes);
            Assert.AreEqual(send.Nick, reciev.Nick);
            Assert.AreEqual(send.Password, reciev.Password);
        }

        [TestCase("jmeno")]
        [TestCase("jmend235&$432&$asd556")]
        public void ErrorMessage(string str)
        {
            Error send = new Error(str);
            byte[] bytes = send.ToBytes();
            Error reciev = new Error(bytes);
            Assert.AreEqual(send.MSG, reciev.MSG);
        }

        
        public void HeaderOfMessage() // Lot of other messages just change the type
        {
            Message send = new Message();
            send.Recipient = new ID(new byte[] { 1, 2, 3, 4, 7, 8, 9, 10 });
            send.Sender = new ID(new byte[] { 15, 52, 53, 45, 57, 85, 95, 150 });
            byte[] bytes = send.ToBytes();
            Message reciev = new Message(bytes);
            var comparer = new ID.EqualityComparer();
            ///Hand compare
            for (int i = 0; i < ID.IDLength; i++)
            {
                Assert.AreEqual(send.Recipient.Parts[i], reciev.Recipient.Parts[i]);
                Assert.AreEqual(send.Sender.Parts[i], reciev.Sender.Parts[i]);
            }

            Assert.True(comparer.Equals(send.Sender, reciev.Sender));
            Assert.True(comparer.Equals(send.Recipient, reciev.Recipient));
            Assert.False(comparer.Equals(send.Sender, reciev.Recipient));
            Assert.False(comparer.Equals(send.Recipient, reciev.Sender));

        }


        [TestCase (5,typeof(SpyderLight))]
        [TestCase (3, typeof(RGBLight))]
        [TestCase (12, typeof(DIMMLight))]
        public void ComponentSerializationTest(byte size, Type t) 
        {
            var genericBase = typeof(Components<>);
            var StetOfGenericType = genericBase.MakeGenericType(t);
            IParts container = (IParts)Activator.CreateInstance(StetOfGenericType);
            IParts empty = (IParts)Activator.CreateInstance(StetOfGenericType);
            container.ReAllocData();

            Assert.NotNull(container);
            Assert.NotNull(empty);
            for (int i = 0; i < container.Data.Length; i++)
            {
                container.Data[i] = (byte)((i*2)%256); 
            }

            Assert.AreEqual(container.Size, size);
            var bytes = container.ToBytes();

            empty.ReAllocData();
            empty.FillParts(bytes);
            Assert.AreEqual(empty.Size, size);

            for (int i = 0; i < container.Data.Length; i++)
            {
                Assert.AreEqual(container.Data[i], empty.Data[i]);
            }


            Assert.Pass();
        }



        [TestCase(5,5, typeof(SpyderLight))]
        [TestCase(3,3, typeof(RGBLight))]
        [TestCase(12,12, typeof(DIMMLight))]
        public void StateSerializationTest(byte size,byte typeNumber, Type t)
        {
            var genericBase = typeof(Components<>);
            var StetOfGenericType = genericBase.MakeGenericType(t);
            IParts container = (IParts)Activator.CreateInstance(StetOfGenericType);
             container.ReAllocData();

            Assert.NotNull(container);
           
            for (int i = 0; i < container.Data.Length; i++)
            {
                container.Data[i] = (byte)((i * 2) % 256);
            }

            Assert.AreEqual(container.Size, size);



            IState state = null;
            IState newState = null;
            switch (typeNumber)
            {
                case 3:
                    state = new State<RGBLight>(new ID(true), 22, ((Components<RGBLight>)container), null);
                    newState  = new State<RGBLight>();
                    break;

                case 5:
                    state = new State<SpyderLight>(new ID(true), 22, ((Components<SpyderLight>)container), null);
                    newState = new State<SpyderLight>();
                    break;

                case 12:
                    state = new State<DIMMLight>(new ID(true), 22, ((Components<DIMMLight>)container), null);
                    newState = new State<DIMMLight>();
                    break;
            }

            Assert.NotNull(state);
            
            byte[] bytes = state.ToBytes();
            Assert.NotNull(bytes);
            int off = 0;
            newState.FillState(bytes,ref off);
            Assert.AreEqual(state.Importance.Importance, newState.Importance.Importance);
            for (int i = 0; i < state.Parts.Data.Length; i++)
            {
                Assert.AreEqual(newState.Parts.Data[i], state.Parts.Data[i]);
            }


            Assert.Pass();
        }


        [TestCase(5)]
        [TestCase(3)]
        [TestCase(12)]
        public void SceneSerializationTest(byte size)
        {
            Scene scene = new Scene();
            for (byte i = 0; i < size; i++)
            {
                IState stateRGB = new State<RGBLight>(new ID(true), i, new Components<RGBLight>(true), null);
                for (int k = 0; k < stateRGB.Parts.Data.Length; k++)
                {
                    stateRGB.Parts.Data[k] = (byte)(55+k * i);
                }
                scene.EquipmentsData.Add(stateRGB);

                IState stateDIMM = new State<DIMMLight>(new ID(true), i, new Components<DIMMLight>(true), null);
                for (int k = 0; k < stateDIMM.Parts.Data.Length; k++)
                {
                    stateDIMM.Parts.Data[k] = (byte)(55 + k * i);
                }
                scene.EquipmentsData.Add(stateDIMM);

                IState stateSPYDER = new State<SpyderLight>(new ID(true), i, new Components<SpyderLight>(true), null);
                for (int k = 0; k < stateSPYDER.Parts.Data.Length; k++)
                {
                    stateSPYDER.Parts.Data[k] = (byte)(55 + k * i);
                }
                scene.EquipmentsData.Add(stateSPYDER);

            }
            
            byte[] bytes = scene.ToBytes();
           
            Scene deScene = new Scene(bytes);
            
            for (int i = 0; i < scene.EquipmentsData.Count; i++)
            {
                var before = scene.EquipmentsData[i];
                var after = deScene.EquipmentsData[i];
                
                Assert.AreEqual(before.Importance.Importance, after.Importance.Importance);
                
                for (int k = 0; k < before.Parts.Data.Length; k++)
                {
                    Assert.AreEqual(after.Parts.Data[k], before.Parts.Data[k]);
                }
                
            }
           
        }
    }
}