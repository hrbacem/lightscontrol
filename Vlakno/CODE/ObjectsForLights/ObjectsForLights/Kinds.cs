﻿using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lights
{
    /// <summary>
    /// Struct for keeping pure data 
    /// </summary>
    namespace EquipmentsKinds
    {
   
        /// <summary>
        /// Serialization & Deserialization of Kinds
        /// </summary>
        public static class KindSerialization
        {

            public static string Serialize(IState state)
            {


                if (state.GetType() == typeof(State<RGBLight>)) return "RGBLight";

                if (state.GetType() == typeof(State<SpyderLight>)) return "SpyderLight";

                if (state.GetType() == typeof(State<DIMMLight>)) return "DIMMLight";

                return "unknown";
            }

            public static IState Deserialize(string state)
            {
                if (state == "RGBLight") return new State<RGBLight>(); ;

                if (state == "SpyderLight") return new State<SpyderLight>(); ;

                if (state == "DIMMLight") return new State<DIMMLight>(); ;

                throw new Exception("Unknown kind.");
            }

            public static IState GetState(ImplementedKinds state,ID id)
            {
                
                switch (state)
                {
                    case ImplementedKinds.RGBLight: return new State<RGBLight>(id,1,new Components<RGBLight>(true));

                    case ImplementedKinds.SpyderLight: return new State<SpyderLight>(id, 1, new Components<SpyderLight>(true));

                    case ImplementedKinds.DIMMLight: return new State<DIMMLight>(id, 1, new Components<DIMMLight>(true));

                    default: return null;
                }
                

            }


            /// <summary>
            /// Choose Kind by typeNumber of Parts
            /// </summary>
            /// <param name="typeNumber">Count of Parts in this equipment</param>
            /// <returns></returns>
            public static Type TypeNumberToKind(byte typeNumber)
            {   // In this moment is good enought to choose Kind of equipment just by count of Parts  
                switch ((ImplementedKinds)typeNumber)
                {
                    case (ImplementedKinds.RGBLight):
                        return typeof(RGBLight);
                    case (ImplementedKinds.SpyderLight):
                        return typeof(SpyderLight);
                    case (ImplementedKinds.DIMMLight):
                        return typeof(DIMMLight);

                        //in case somebody try use a wrong data
                    default: throw new Exception("Unknown Type of Equipment "+typeNumber);
                }


            }

           
        }


        /// <summary>
        /// Kinds of equipment
        /// </summary>
        public enum ImplementedKinds
        {
            RGBLight = 3,
            SpyderLight = 5,
            DIMMLight = 12,
        }

        /// <summary>
        /// Equipment where main Parts represents Light
        /// </summary>
        public interface ILight
        {

        }

        /// <summary>
        /// Equipment with RGB Parts
        /// </summary>
        public interface IColored:ILight
        {

        }

        /// <summary>
        /// Equipment wich can move
        /// </summary>
        public interface IMovable : ILight
        {

        }

        /// <summary>
        /// Equipment with subparts
        /// </summary>
        public interface IWhite:ILight
        {

        }


        /// <summary>
        /// Kinds of equipments 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public abstract class Kind<T>
        where T : Kind<T>
        {
            /// <summary>
            /// Number of byte Parts
            /// </summary>
            internal static byte Size;
            internal static byte TypeNumber;

        }


        /// <summary>
        /// RGB contains 3 components - part RGB
        /// </summary>
        public sealed class RGBLight : Kind<RGBLight> 
        {
            static RGBLight()
            {
                Size = 3;
                TypeNumber = (byte)ImplementedKinds.RGBLight;
            }
        }



        

        /// <summary>
        /// RGB contains 12 components 
        /// </summary>
        public sealed class DIMMLight : Kind<DIMMLight>
        {
            static DIMMLight()
            {
                Size = 12;
                TypeNumber = (byte)ImplementedKinds.DIMMLight;
            }
        }


        /// <summary>
        /// Spyders contains 5 components -  part RGB and axis XY
        /// </summary>
        public sealed class SpyderLight : Kind<SpyderLight>
        {
            static SpyderLight()
            {
                Size = 5;
                TypeNumber = (byte)ImplementedKinds.SpyderLight;
            }
        }

       


    }

}