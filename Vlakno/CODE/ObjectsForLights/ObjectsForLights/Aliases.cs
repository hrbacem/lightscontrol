﻿using Lights.Parsers;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lights
{

    namespace Parsers
    {
        /// <summary>
        /// Translation of name to ID 
        /// </summary>
        public class Aliases
        {
            public delegate void Changed();

            public Changed AliasesChanged;
            public Dictionary<string, PartNick> MembersNames { get; internal set; }

            public Dictionary<string, GroupState> GroupMembers;

            public Aliases()
            {

                MembersNames = new Dictionary<string, PartNick>();
                GroupMembers = new Dictionary<string, GroupState>();
            }

            public bool TryGetNickName(PartNick identita, out string nick)
            {
                nick = default(string);
                //NameOfpart
                if (!identita.All)
                    nick = MembersNames.FirstOrDefault(v => v.Value.Id.Equals(identita.Id) && v.Value.Offset.Equals(identita.Offset) && !v.Value.All).Key;


                if (nick == default(string))
                {
                    //Name of equipment
                    nick = MembersNames.FirstOrDefault(v => v.Value.Id.Equals(identita.Id) && v.Value.All).Key;
                    if (nick == default(string)) return false;
                    return true;
                }
                return true;
            }

            public void TryAdd(GroupState group)
            {

                if (GroupMembers.ContainsKey(group.Name))
                {
                    int i = 0;
                    while (GroupMembers.ContainsKey(group.Name + " " + i))
                    {
                        i++;
                    }
                    group.Name += " " + i;

                }
                GroupMembers.Add(group.Name, group);

            }

            public bool TryGetID(string nick, out PartNick id)
            {
                if (!MembersNames.ContainsKey(nick))
                {
                    id = default(PartNick);
                    return false;
                }

                id = MembersNames[nick];

                return true;

            }

            private bool TryAddMember(string nick, ID id, bool all = true, int offset = 0)
            {
                if (MembersNames.ContainsKey(nick)) return false;
                MembersNames.Add(nick, new PartNick(id, all, offset));
                return true;

            }

            public void AddUniqueNick(string nick, ID id, bool all = true, int offset = 0)
            {
                string name = nick;
                int i = 0;
                while (MembersNames.ContainsKey(name))
                {
                    name = nick +" "+ i;
                    i++;
                }
                MembersNames.Add(name, new PartNick(id, all, offset));
                
            }

            public bool LoadGroups(string NameOfFile)
            {
                bool success = GroupSerializer.TryLoadGroupData(NameOfFile, out List<GroupState> loadedGroup);
                if (!success) return success;
                if (loadedGroup == null) return false;

                foreach (var item in loadedGroup)
                {
                    if (!GroupMembers.ContainsKey(item.Name))
                    {
                        GroupMembers.Add(item.Name, item);
                    }

                }
                return success;
            }

            public void SaveGroups(string NameOfFile)
            {
                List<GroupState> container = new List<GroupState>();
                foreach (var item in GroupMembers)
                {
                    container.Add(item.Value);
                }

                GroupSerializer.TrySaveGroupData(NameOfFile, container);
            }



            public bool LoadNickNames(string NameOfFile)
            {
                bool success = ParserJsonDictionary<string, PartNick>.LoadData(NameOfFile, out Dictionary<string, PartNick> loaded);
                if (!success) return success;
                if (loaded == null) return false;
                foreach (var item in loaded)
                {
                    if (!MembersNames.ContainsKey(item.Key))
                    {
                        MembersNames.Add(item.Key, item.Value);
                    }
                }
                return success;
            }

            public void SaveNickNames(string NameOfFile)
            {
                ParserJsonDictionary<string, PartNick>.SaveData(NameOfFile, MembersNames);
            }

        }
    }
}
