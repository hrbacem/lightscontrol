﻿using Lights.Messages;
using Lights.Transport;
using Lights;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Lights.Parsers;

namespace Lights
{
    namespace Comunication
    {
        /// <summary>
        /// Common methods for users and servers
        /// </summary>
        public abstract class ComunicationClass
        {

            /// <summary>
            /// Data in conversiton was Changed From Outside
            /// </summary>
            public bool ConversationCFO = false;

            /// <summary>
            /// Data in errors was Changed From Outside
            /// </summary>
            public bool ErrorsCFO = false;


            /// <summary>
            /// Data in data was Changed From Outside
            /// </summary>
            public bool DataCFO = false;


            /// <summary>
            /// Errors from main server - Fails/Exceptons happend on server or Client side 
            /// </summary>
            public List<string> Errors { get; }

            /// <summary>
            /// Data for equipments
            /// </summary>
            public Dictionary<IID, IState> Data { get; }


            /// <summary>
            /// Text Messages got from server
            /// </summary>
            public List<string> Conversation { get; }



            /// <summary>
            /// Recieved new string message 
            /// </summary>
            /// <param name="msg"></param>
            public delegate void reciveNewStringMsg(string msg);

            /// <summary>
            /// Recieved new message  to conversation
            /// </summary>
            public reciveNewStringMsg conversationUpdate;

            /// <summary>
            /// Recieved new message  to Errors
            /// </summary>
            public reciveNewStringMsg errorsUpdate;

            /// <summary>
            /// Recieved new Scene message 
            /// </summary>
            /// <param name="msg"></param>
            public delegate void reciveNewSceneMsg(Scene rec_scene);

            /// <summary>
            /// Recieved new message  to Data
            /// </summary>
            public reciveNewSceneMsg dataUpdate;

            /// <summary>
            /// Manager groups and nick names of equipments
            /// </summary>
            public Aliases Aliases = new Aliases();

            /// <summary>
            /// Get IP of computer and create ID
            /// </summary>
            /// <returns></returns>
            public IID getMyIID()
            {
                return CreateID(Dns.GetHostEntry(Dns.GetHostName()).AddressList
               .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork));
            }





            /// <summary>
            /// Make ID from IPv4 [0-3]=IPv4 [4-9]=0
            /// </summary>
            /// <param name="ip"></param>
            /// <returns></returns>
            public static ID CreateID(IPAddress ip)
            {
                // Just set the most basic ID with IP in first four bytes 
                ID id = new ID
                {
                    Parts = new byte[ID.IDLength]
                };
                if (ip != null)
                    ip.GetAddressBytes().CopyTo(id.Parts, 0);
                return id;
            }



            

            public ComunicationClass()
            {
                Data = new Dictionary<IID, IState>(new ID.EqualityComparer());
                Conversation = new List<string>();
                Errors = new List<string>();  
                communicationID = new Dictionary<IID, IID>(new ID.EqualityComparer());
            }




           

            /// <summary>
            /// Findout IPaddres to comunicate 
            /// </summary>
            /// <param name="Aim"></param>
            /// <returns></returns>
            protected IPAddress GetIPAddressFor(IPAddress Aim,int port)
            {

                IPEndPoint ip = new IPEndPoint(Aim, port);
                Socket testSocket = new Socket(ip.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                testSocket.Connect(ip);

                return ((IPEndPoint)testSocket.LocalEndPoint).Address;
            }

            private Dictionary<IID, IID> communicationID;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="Aim"></param>
            /// <returns></returns>
            protected IID GetIDForCommunication(IID Aim,int port)
            {
                if (communicationID.ContainsKey(Aim))
                    return communicationID[Aim];
                IPAddress ip = GetIPAddressFor(Aim.ToIPv4(),port);
                ID identification = CreateID(ip);
                communicationID.Add(Aim, identification);
                return identification;
            }

            

        }
    }
    
}
