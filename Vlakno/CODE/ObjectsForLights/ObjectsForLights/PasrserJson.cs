﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Lights.Transport;
using Lights;
using Newtonsoft.Json;
using Lights.EquipmentsKinds;

namespace Lights
{

    namespace Parsers
    {
        /// <summary>
        /// Class to seralize Dictionary data to file
        /// </summary>
        /// <typeparam name="T">Serializeble type</typeparam>
        public static class ParserJsonDictionary<K, V>
        {
            /// <summary>
            /// Container holding tuple 
            /// </summary>
            class Term
            {
                public Term(K key, V value)
                {
                    Key = key;
                    Value = value;
                }

                public K Key;
                public V Value;
            }

            /// <summary>
            /// Try save data to given file,
            /// do not throw exception
            /// </summary>
            /// <param name="outputFileName"></param>
            /// <param name="dataToSave"></param>
            /// <returns></returns>
            public static bool SaveData(string file, Dictionary<K, V> data)
            {
                List<Term> map = new List<Term>();
                lock (data)
                {
                    foreach (var item in data)
                    {
                        map.Add(new Term(item.Key, item.Value));
                    }
                }
               
                return ParserJsonList<Term>.TrySave(file, map);

            }

            /// <summary>
            /// Try load data from given file, do not throw exception
            /// </summary>
            /// <param name="inputFileName">File with with data</param>
            /// <param name="dataToLoad">container to fill with data</param>
            /// <returns></returns>
            public static bool LoadData(string file, out Dictionary<K, V> data)
            {

                
                if (ParserJsonList<Term>.TryLoad(file, out List<Term> map))
                {

                    data = new Dictionary<K, V>();
                    if (map == null) return false;
                    foreach (var item in map)
                    {
                        data.Add(item.Key, item.Value);
                    }
                    return true;
                }

                data = null;
                return false;

            }



        }

        
        /// <summary>
        /// Class to seralize List data to file
        /// </summary>
        /// <typeparam name="T">Serializeble type</typeparam>
        public static class ParserJsonList<T> 
        {
            /// <summary>
            /// Container to keep data
            /// </summary>
            class DataContainer
            {
                public List<T> dataList;
            }


            /// <summary>
            /// Try save T to file
            /// </summary>
            /// <param name="outputFileName"></param>
            /// <param name="dataToSave"></param>
            /// <returns></returns>
            public static bool TrySaveSingleOne(string outputFileName, T dataToSave)
            {
                try
                {
                    string serializedText = JsonConvert.SerializeObject(dataToSave);
                    File.WriteAllText(outputFileName, serializedText);
                }
                catch (Exception e)
                {
                    return false;
                }
                return true;
            }


            /// <summary>
            /// Try load T from file 
            /// </summary>
            /// <param name="inputFileName"></param>
            /// <param name="dataToLoad"></param>
            /// <returns></returns>
            public static bool TryLoadSingleOne(string inputFileName, out T dataToLoad)
            {
                try
                {
                    string inputText = System.IO.File.ReadAllText(inputFileName);
                    dataToLoad = JsonConvert.DeserializeObject<T>(inputText);

                }
                catch (Exception e)
                {
                    dataToLoad = default(T);
                    return false;
                }
                return true;
            }

            /// <summary>
            /// Try load data from given file, do not throw exception
            /// </summary>
            /// <param name="inputFileName">File with with data</param>
            /// <param name="dataToLoad">container to fill with data</param>
            /// <returns></returns>
            public static bool TryLoad(string inputFileName, out List<T> dataToLoad)
            {
                bool success = true;
                try
                {
                    /// Read inputfile and let JasonConvert Deserialize the object
                    string inputText = System.IO.File.ReadAllText(inputFileName);
                    DataContainer holder = JsonConvert.DeserializeObject<DataContainer>(inputText);
                    dataToLoad = holder.dataList;
                }
                catch (Exception e)
                {
                    dataToLoad = null;
                    success = false;

                }
                return success;
            }

            /// <summary>
            /// Try save data to given file,
            /// do not throw exception
            /// </summary>
            /// <param name="outputFileName"></param>
            /// <param name="dataToSave"></param>
            /// <returns></returns>
            public static bool TrySave(string outputFileName, List<T> dataToSave)
            {
                try
                {
                    DataContainer holder = new DataContainer
                    {
                        dataList = dataToSave
                    };
                    string serializedText = JsonConvert.SerializeObject(holder);
                    File.WriteAllText(outputFileName, serializedText);
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
        }

    }

}