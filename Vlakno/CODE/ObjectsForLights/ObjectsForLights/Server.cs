﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lights;
using Lights.Gate;
using Lights.Messages;
using Lights.Protocols;
using Lights.Transport;


namespace Lights
{

    namespace Comunication
    {
        /// <summary>
        /// Server- connecting Clients and equipments 
        /// </summary>
        public class Server : ComunicationClass
        {
            /// <summary>
            /// Maximal priority value set by Client 
            /// </summary>
            public const byte MaximalPriorityValue = 127;

            /// <summary>
            /// Communication with Clients 
            /// </summary>
            GateMessage GateClients;

            /// <summary>
            /// Communications with equipments
            /// </summary>
            GateByteMessage GateEquipments;

            


            /// <summary>
            /// Data in connected was Changed From Outside
            /// </summary>
            public bool ConnectedCFO = false;


           


            


            /// <summary>
            /// Data prepared to send to avoid long locks
            /// </summary>
            private Dictionary<IID, IState> DataToSend;



            /// <summary>
            /// TimeStamp of last recieved data 
            /// </summary>
            private Dictionary<IID, TimeStamp> DateTimeStampMap;

            /// <summary>
            /// Map of equipments and protocols
            /// </summary>
            public Dictionary<IID, IProtocol> Map { get; }

            /// <summary>
            /// List of Clients  
            /// </summary>
            public Dictionary<IID, string> Connected { get; }

            /// <summary>
            /// Flag of working 
            /// </summary>
            bool isActive;


            /// <summary>
            /// Flag of working 
            /// </summary>
            bool IsActive { get => isActive; }

            /// <summary>
            /// Port of communication with controllers
            /// </summary>
            int PortUP;
            public int ClientPort { get => PortUP; }


            /// <summary>
            /// Port of commuinication with equipments
            /// </summary>
            int PortDOWN;
            public int EquipmentPort { get => PortDOWN; }


           

            ///ID of Server
            public IID ServerID { get; internal set; }


            /// <summary>
            /// Start server
            /// </summary>
            /// <param name="Port"></param>
            public Server(int PortUP = 6666, int PortDOWN = 5000)
            {
                isActive = true;
                ServerID = getMyIID();

                //Avoiding to have communication on same port
                if (PortDOWN == PortUP) throw new Exception("Communication cannot be on the same port!");

                this.PortDOWN = PortDOWN;
                this.PortUP = PortUP;

                // Inicialiyation of all - Where is IID has to be Equality comparer
                Connected = new Dictionary<IID, string>(new ID.EqualityComparer());
                Map = new Dictionary<IID, IProtocol>(new ID.EqualityComparer());
                DateTimeStampMap = new Dictionary<IID, TimeStamp>(new ID.EqualityComparer());               
                DataToSend = new Dictionary<IID, IState>(new ID.EqualityComparer());
                

                GateClients = new GateMessage(PortUP);
                GateEquipments = new GateByteMessage(PortDOWN);

                
            

                //Start thread for Processing accepted messages
                processor = new Thread(Processor);
                processor.Start();

                //Start thread which just send prepared data
                sceneMaker = new Thread(DataSender);
                sceneMaker.Start();


                //Start thread updating Clients
                clientUpdator = new Thread(UpdateConnectedThread);
                clientUpdator.Start();

            }

            /// <summary>
            /// Password for Clients to be connected 
            /// </summary>
            public string ServerPassword { set; get; }


            /// <summary>
            /// Thread sorting
            /// </summary>
            Thread processor;

            /// <summary>
            /// Threads making scene
            /// </summary>
            Thread sceneMaker;

            /// <summary>
            /// Threads refreshing consoles
            /// </summary>
            Thread clientUpdator;

            /// <summary>
            /// Turn off the Server
            /// </summary>
            public void Shutdown()
            {
                //There should be killing off all proces, threads... 
                isActive = false;
                GateEquipments.Shutdown();
                GateClients.Shutdown();
                processor.Abort();
                sceneMaker.Abort();
                clientUpdator.Abort();

            }

            /// <summary>
            /// Classify messeges and sort them to correct Lists
            /// </summary>
            private void Processor()
            {
                Message got;

                //if server is still working - nobody kill him
                while (isActive)
                {

                    //try Get message
                    got = GateClients.Get();

                    if (got != null)
                    {
                        //converting messgae - because of calling corect sort method 
                        callCorrectProcessor(got);
                        
                    }
                    else
                    {
                        Thread.Yield();
                    }
                }
            }

            private void callCorrectProcessor(Message msg)
            {
                if (msg.GetType() == typeof(Scene))
                {
                    processMsg(((Scene)msg)); return;
                }

                if (msg.GetType() == typeof(Chat))
                {
                    processMsg(((Chat)msg)); return;
                }

                if (msg.GetType() == typeof(LogIn))
                {
                    processMsg(((LogIn)msg)); return;
                }

                if (msg.GetType() == typeof(LogOut))
                {
                    processMsg(((LogOut)msg)); return;
                }


                if (msg.GetType() == typeof(IntroducingClient))
                {
                    processMsg(((IntroducingClient)msg)); return;
                }

                if (msg.GetType() == typeof(IntroducingServer))
                {
                    processMsg(((IntroducingServer)msg)); return;
                }

                if (msg.GetType() == typeof(Error))
                {
                    processMsg(((Error)msg)); return;
                }
                
                // Try Dynamic 
                dynamic m = Convert.ChangeType(msg, msg.GetType());
                processMsg(m);
            }

            /// <summary>
            /// From recieved messages build Scene to send
            /// </summary>
            private void DataSender()
            {
                
                Dictionary<IID, IState> dataToSend;

                while (isActive)
                {

                    dataToSend = DataToSend;

                        lock (dataToSend)
                            if (dataToSend.Any())
                            {

                                {
                                   
                                    foreach (var item in dataToSend)
                                    {
                                        //try Get protocol, if is avaible
                                        Map.TryGetValue(item.Value.Id, out IProtocol protocol);
                                        if (protocol != null)
                                        {
                                            //protocol is avaible -> data can be transform by protocol
                                            byte[] send = protocol.ToBytes(item.Value);
                                            ByteMessage msg;



                                            //low message dont care about id-> just IP

                                            msg.ip = item.Value.Id.ToIPv4();
                                            msg.message = send;

                                            // put data to Get to send

                                            GateEquipments.Send(msg);

                                        }
                                    }
                                }
                            }

                    Thread.Yield();
                    



                }
            }

            /// <summary>
            /// The most general- catch all unknown messages 
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Message msg)
            {
                Errors.Add("Try to process " + msg.GetType());

            }

            /// <summary>
            /// Login of  Clients - If Client is logged just log error 
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(LogIn msg)
            {
                ErrorsCFO = true;

                if (Connected.ContainsKey(msg.Sender))
                {
                    //Client is logged - Same ID is loged 
                    string ERmsg = "This Client is already Logged! " + msg.Nick + " " + msg.Sender;
                    Errors.Add(ERmsg);
                    SendClient(new Error(ERmsg), msg.Sender);
                    SendClient(new LogInConfirm(), msg.Sender);
                    return;

                }
                else
                {
                    string ERmsg;
                    if (ServerPassword != msg.Password)
                    {
                        ERmsg = "Wrong password login denied: " + msg.Nick + " " + msg.Sender;
                        Errors.Add(ERmsg);
                    }
                    else
                    {
                        ERmsg = "New client logged " + msg.Nick + " " + msg.Sender;
                        Errors.Add(ERmsg);
                        Connected.Add(msg.Sender, msg.Nick);

                        // Send information about loging 
                        SendClient(new LogInConfirm(), msg.Sender);

                        // send current state to new logged
                        sendGivenState(msg.Sender, MakeCurrentState());
                    }

                    ///Info about login
                    Errors.Add(ERmsg);
                    Error err = new Error(ERmsg);
                    SendClient(err, msg.Sender);

                }

            }

            /// <summary>
            /// Logout of Equipment
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(LogOut msg)
            {
                ErrorsCFO = true;
                //client can be unloged by server befor
                if (Connected.ContainsKey(msg.Sender))
                {
                    //if si client logged, will be unloged to make space for other


                    Connected.TryGetValue(msg.Sender, out string nm);
                    string ERmsg = "Client unlogged " + nm + " " + msg.Sender;
                    Errors.Add(ERmsg);
                    Connected.Remove(msg.Sender);

                    // Send information about unloging 
                    SendClient(new LogOutConfirm(), msg.Sender);
                }
                else
                {
                    string ERmsg = "Unloged client try unlog " + msg.Sender;
                    Errors.Add(ERmsg);
                    SendClient(new LogOutConfirm(), msg.Sender);
                }


            }

            /// <summary>
            /// IntroducingClient - recive introduce from server and send introduce back
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(IntroducingClient msg)
            {
                SendClient(new IntroducingServer(), msg.Sender);
            }

            /// <summary>
            /// IntroducingServer - ignore
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(IntroducingServer msg)
            {
            }

            /// <summary>
            /// Chat- sorting of text messages
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Chat msg)
            {
                
                ConversationCFO = true;
                string message;
                lock (Conversation)
                {

                    {
                        // ading new text message to List
                        if (!Connected.ContainsKey(msg.Sender))
                            message = msg.Sender + " : " + msg.Text;
                        else
                        {
                            Connected.TryGetValue(msg.Sender, out string nick);
                            message = nick + " : " + msg.Text;
                        }

                    }

                    this.Conversation.Add(message);
                }
                ///broadcast to connected Client
                MessageToAll(message);

                
            }


            /// <summary>
            /// Procces scene of logged messages
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Scene msg)
            {
                DataCFO = true;
                /// Sender of this message must be accepted
                if (Connected.ContainsKey(msg.Sender))
                {
                    bool useIncomingScene = true;
                    if (!DateTimeStampMap.ContainsKey(msg.Sender))
                    {
                        ///Create first Time Stamp For klient
                        DateTimeStampMap.Add(msg.Sender, new TimeStamp(msg.DateTimeStamp, msg.MilisecondStamp));
                    }
                    else
                    {
                        TimeStamp currentStamp = DateTimeStampMap[msg.Sender];
                        if (currentStamp.NewerOrTheSame(msg.DateTimeStamp, msg.MilisecondStamp))
                        {
                            DateTimeStampMap[msg.Sender].Set(msg.DateTimeStamp, msg.MilisecondStamp);
                        }
                        else
                        {
                            useIncomingScene = false;
                        }

                    }

                    if(useIncomingScene)
                    lock (Data)
                    {
                        /// Foreach Istate in messga 
                        foreach (var newItem in msg.EquipmentsData)
                        {
                            if (!Data.ContainsKey(newItem.Id))
                            {
                                ///no data to be change -> new data
                                Data.Add(newItem.Id, newItem);
                                
                                DataCFO = true;
                            }
                            else
                            {
                                /// Value has to be in Data
                                Data.TryGetValue(newItem.Id, out IState old);

                                    ///Kind check
                                    if (old.GetType() != newItem.GetType()) return;
                                
                                ///Check Importace
                                if (old.Importance.Importance <= newItem.Importance.Importance)
                                {
                                    /// For each part check if this state contain value to set
                                    for (int i = 0; i < old.PartImportance.Length; i++)
                                    {
                                        /// higher priority -> set
                                        if (newItem.PartImportance[i] && !old.PartImportance[i])
                                        {
                                            /// Change data and update PartImportance
                                            old.PartImportance[i] = newItem.PartImportance[i];
                                            old.Parts.Data[i] = newItem.Parts.Data[i];

                                        }
                                        else
                                        {
                                            /// same priority -> check time stamp
                                            if (newItem.PartImportance[i] == old.PartImportance[i])
                                            {
                                                    /// Change data and update PartImportance
                                                    old.PartImportance[i] = newItem.PartImportance[i];
                                                    old.Parts.Data[i] = newItem.Parts.Data[i];
                                            }
                                        }

                                        old.Importance = newItem.Importance;
                                    }                                   
                                    DataCFO = true;
                                }
                            }

                        }

                        ///Data was changed, toSend actual data
                        Dictionary<IID, IState> DataPrepared = new Dictionary<IID, IState>(new ID.EqualityComparer());
                        foreach (var item in Data)
                        {
                            //make new Dictionary with actual data 
                            DataPrepared.Add(item.Key, item.Value);
                        }

                        DataToSend = DataPrepared; //Atomic Operation

                        //Somebody is accepting messages about update
                        dataUpdate?.Invoke(msg);
                        updateConnected(msg.Sender);
                    }
                }
                else
                {
                    ///Not connected Client
                    string ERmsg = "Not logged client tried change the data! <" + msg.Sender;
                    Errors.Add(ERmsg);
                    Error err = new Error("This client is not logged -> this Client cannot change the data");
                    SendClient(err, msg.Sender);
                }

            }

            /// <summary>
            /// Create Scene message containing current state of lights
            /// </summary>
            /// <returns></returns>
            private Scene MakeCurrentState()
            {
                Scene IntroducingMsg = new Scene();
                IntroducingMsg.DateTimeStamp = TimeStamp.GetTimeStampDateTime();
                IntroducingMsg.MilisecondStamp = TimeStamp.GetTimeStampMilisecond();
                var IntroduceData = DataToSend;
                lock (IntroduceData)
                    foreach (var item in IntroduceData)
                    {
                        IntroducingMsg.EquipmentsData.Add(item.Value);
                    }
                return IntroducingMsg;
            }


            /// <summary>
            /// Send given state to Client
            /// </summary>
            private void sendGivenState(IID Client, Scene state)
            {
                Scene toSend = new Scene
                {
                    EquipmentsData = state.EquipmentsData
                };
                SendClient(toSend, Client);
            }





            /// <summary>
            /// Set flag  to update all registred Clients
            /// </summary>
            private void updateConnected()
            {

                UpdateConnectedFlag = true;
            }


            /// <summary>
            /// Update all registred Clients
            /// </summary>
            private void updateConnected(IID updater)
            {
                lock (Connected)
                {
                    // Send current state to all connected Clients =! updator
                    Scene actualState = MakeCurrentState();
                    foreach (var member in Connected)
                    {
                        if (member.Key != updater)
                            sendGivenState(member.Key, actualState);
                    }
                }
            }

            /// <summary>
            /// update all registred Clients
            /// </summary>
            bool UpdateConnectedFlag = false;

            long UpdateTime = 5000;

            /// <summary>
            /// Update connected 
            /// </summary>
            private void UpdateConnectedThread()
            {
                
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                while (isActive)
                {
                    if (UpdateConnectedFlag || stopWatch.ElapsedMilliseconds > UpdateTime )
                    {
                        stopWatch.Reset();
                        UpdateConnectedFlag = false;
                        lock (Connected)
                        {
                            // Send current state to Clients
                            Scene actualState = MakeCurrentState();
                            foreach (var member in Connected)
                            {
                                sendGivenState(member.Key, actualState);
                            }
                        }
                        
                    }
                    Thread.Yield();
                }


            }

            /// <summary>
            /// toSend text message to all logged Client
            /// </summary>
            /// <param name="message"></param>
            public void MessageToAll(string message)
            {
                ///toSend to all connected
                lock (Connected)
                    foreach (var item in Connected)
                    {
                        SendClient(new Chat(message, (ID)item.Key), item.Key);
                    }


            }



            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// Set Sender - Server
            /// NEED to set Recipient
            /// </summary>
            /// <param name="msg"></param>
            public void SendClient<T>(T msg, IID recipient) where T : Message
            {

                msg.Sender = GetIDForCommunication(recipient,ClientPort); // ServerID
                msg.Recipient = recipient;
                GateClients.Send(msg);
            }

            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// Set Sender - Client
            /// Set Recipient - Broadcast
            /// </summary>
            /// <param name="msg"></param>
            public void SendBroadCast<T>(T msg) where T : Message
            {

                msg.Sender = ServerID;
                msg.Recipient = CreateID(System.Net.IPAddress.Broadcast);
                GateClients.Send(msg);
            }


            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// DO NOT SET SENDER AND RECIPIENT
            /// </summary>
            /// <param name="msg"></param>
            public void PureSendClient<T>(T msg) where T : Message
            {
                GateClients.Send(msg);
            }


            /// <summary>
            /// Tell server, that data was updated on GUI
            /// </summary>
            public void DataWasChangedFromServerPlatform()
            {
                //Because of posibility to add data right on server lvl 
                if (Data.Count > DataToSend.Count)
                {
                    ///Data was changed, toSend actual data - comented down
                    Dictionary<IID, IState> DataPrepared = new Dictionary<IID, IState>(new ID.EqualityComparer());
                    lock (Data)
                        foreach (var item in Data)
                        {
                            DataPrepared.Add(item.Key, item.Value);
                        }

                    DataToSend = DataPrepared;

                }

                updateConnected();
            }


        }
    }

}
