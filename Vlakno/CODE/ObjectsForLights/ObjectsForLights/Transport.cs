﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lights.EquipmentsKinds;
using System.Collections;
using System.Net;

namespace Lights
{    
    /// <summary>
    /// Basic blocks for comunication
    /// </summary>
    namespace Transport
    {

        

       
        /// <summary>
        /// Package of byte to avoid wrong understanding 
        /// </summary>
        public struct byteValue
        {
            public byte value;

        }

        /// <summary>
        /// Identical identification of each equipment
        /// </summary>
        public interface IID :IComparable
        {
            byte[] Parts { get; set; }

            /// <summary>
            /// Transform IID to byte[] - sendable
            /// </summary>
            /// <returns></returns>
            byte[] ToBytes();


            /// <summary>
            /// Length oh ID
            /// </summary>
            byte Length { get; }

             IPAddress ToIPv4();

            string ToString();
        }

       /// <summary>
       /// IID in format [0-3]IPv4 [4-7]Empty [8-9]NumberOfSubEquipment 
       /// </summary>
        public struct ID :IID,IComparer,IComparable, IEquatable<ID>

        { 
            /// <summary>
            /// Length of Identificator
            /// </summary>
            public const int IDLength = 10;

            /// <summary>
            /// Count of the bytes in IID
            /// </summary>
            public byte Length { get=>IDLength; }

            /// <summary>
            /// Values of IID
            /// </summary>
            public byte[] Parts { get; set; }

            /// <summary>
            /// Take size of ID from values
            /// </summary>
            /// <param name="values"></param>
            public ID(byte[] values)
            {
                
                Parts = values.Take(ID.IDLength).ToArray();
                    
            }

            /// <summary>
            /// Alloc correct length of Parts -  if alloc = true  
            /// </summary>
            /// <param name="alloc">If alloc Parts</param>
            public ID(bool alloc=true)
            {
                if (alloc)
                    Parts = new byte[ID.IDLength];
                else
                    Parts = null;
            }

            /// <summary>
            /// Transform IID to byte[] like array
            /// </summary>
            /// <returns></returns>
            public byte[] ToBytes()
            {
                if (Parts == null)
                    Parts = new byte[ID.IDLength];
                return Parts ;
            }

            /// <summary>
            /// Create string in format: numberOfbyte:byte  
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < Parts.Length; i++)
                {
                    sb.Append(Parts[i]+" ");
                }
                return sb.ToString();  
            }

            /// <summary>
            /// Make IPv4 from first 4 bytes of Parts
            /// </summary>
            /// <returns></returns>
            public IPAddress ToIPv4()
            {
                byte[] ID = this.Parts;
                byte[] ip = { ID[0], ID[1], ID[2], ID[3] };
                IPAddress IP = new IPAddress(ip);
                return IP;
            }

            /// <summary>
            /// Compare with other ID by Parts
            /// </summary>
            /// <param name="other"></param>
            /// <returns></returns>
            public bool Equals(ID other)
            {
                for (int i = 0; i < other.Length; i++)
                {
                    if (this.Parts[i] != other.Parts[i]) return false;

                }
                return true;
            }

            /// <summary>
            /// Compare with object by Parts
            /// </summary>
            /// <param name="objv"></param>
            /// <returns></returns>
            public int CompareTo(object objv)
            {
                
                ID obj = (ID)objv;
                for (int i = 0; i < Parts.Length; i++)
                {
                    if (obj.Parts[i] > this.Parts[i]) return -1;
                    if (obj.Parts[i] < this.Parts[i]) return 1;
                }
                return 0;
            }

            

            /// <summary>
            /// Compare ID's by Parts
            /// </summary>
            /// <param name="xv"></param>
            /// <param name="yv"></param>
            /// <returns></returns>
            public int Compare(object xv, object yv)
            {
                
                ID x = (ID)xv;
                ID y = (ID)yv;
                for (int i = 0; i < Parts.Length; i++)
                {
                    if (x.Parts[i] > y.Parts[i]) return 1;
                    if (x.Parts[i] < y.Parts[i]) return -1;
                }

                return 0;

            }


            /// <summary>
            /// Comparer based on Parts 
            /// </summary>
            public class EqualityComparer : IEqualityComparer<IID>
            {
                /// <summary>
                /// Compare if all Parts of IID are equal
                /// </summary>
                /// <param name="x"></param>
                /// <param name="y"></param>
                /// <returns></returns>
                public  bool Equals(IID x, IID y)
                {
                   
                    
                    for (int i = 0; i < x.Length; i++)
                    {
                        if (x.Parts[i] != y.Parts[i]) return false;

                    }
                    return true;
                }


                /// <summary>
                /// Made hashCode based on IPv4
                /// </summary>
                /// <param name="val"></param>
                /// <returns></returns>
                public int GetHashCode(IID val)
                {
                    /*
                     *Hash code based on ip is good enought for this situation,
                     * by time if show up, that there is better subleveling 
                     * (A lot of equipments with same IP but different IID)
                     * can be changed
                     */
                    int hash = val.Parts[0];
                    hash = (hash * 256) + val.Parts[1];
                    hash = (hash * 256) + val.Parts[2];
                    hash = (hash * 256) + val.Parts[3];
                    return hash;
                }
            }

        }

      /// <summary>
      /// Byte of Priority, higher is more important then lower.
      /// </summary>
        public struct Priority
        {
            public byte Importance;
        }


        /// <summary>
        /// Packet for sending of data through the internet
        /// </summary>
        public interface IState
        {
            /// <summary>
            /// Id of equipment
            /// </summary>
            IID Id { get; }

            /// <summary>
            /// Data of equipment
            /// </summary>
            IParts Parts { get; }


            /// <summary>
            /// Priority of Part
            /// </summary>
            BitArray PartImportance { get; }

            /// <summary>
            /// Priority of this State for equipment
            /// </summary>
            Priority Importance { get; set; }

           
            /// <summary>
            /// Transform to sendable byte[]  
            /// </summary>
            /// <returns></returns>
            byte[] ToBytes();

            /// <summary>
            /// Fill current state by data 
            /// </summary>
            /// <param name="data"></param>
            byte[] FillState(byte[] data);


            /// <summary>
            /// Fill current state by data from offset byte   
            /// </summary>
            /// <param name="data"></param>
            /// <param name="offset"></param>
            void FillState(byte[] data, ref int offset);

            /// <summary>
            /// Set value at given position and all neccesarry flags
            /// </summary>
            /// <param name="position"></param>
            /// <param name="value"></param>
            /// <returns></returns>
            bool SetValue(int position, byte value);


            /// <summary>
            /// Set given importance to all parts or given range.
            /// Wrong range or out of boundary is cut.
            /// numbers -1 is untill end
            /// </summary>
            /// <param name="value"></param>
            void SetImportanceAll(bool value,int start = 0, int numbers=-1 );



        }


        /// <summary>
        /// State of one equipment 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public struct State<T> : IState where T : Kind<T>
        {
            /// <summary>
            /// Container of current values
            /// </summary>
            public Components<T> values;

            /// <summary>
            /// Priority of using this setting
            /// </summary>
            public Priority imp;

            /// <summary>
            /// IID of equipment
            /// </summary>
            IID id;
           

            /// <summary>
            /// IID of equipment
            /// </summary>
            public IID Id { get => id; }

            /// <summary>
            /// Parts with data for Equipment
            /// </summary>
            public IParts Parts { get => values; }

            /// <summary>
            /// Priority of Part
            /// </summary>
            public BitArray PartImportance { get; internal set; }

            

            /// <summary>
            /// Priority of this package for the equipment
            /// </summary>
            public Priority Importance { get => imp; set { imp = value; } }
                      
            

            /// <summary>
            /// Construct State by given data
            /// </summary>
            /// <param name="id"></param>
            /// <param name="imp"></param>
            /// <param name="data"></param>
            public State(ID id, byte imp, Components<T> data, BitArray partImportance = null)
            {
                this.id = new ID(id.Parts);
                this.imp.Importance = imp;
                values = data;

                
                if (partImportance == null)
                    PartImportance = new BitArray(values.Size, false);
                else
                    PartImportance = partImportance;
            }

            /// <summary>
            /// Transform struc in byte[] -> [TypeNumber][ID][Importance][Length of Parts][Parts][PartImportance]
            /// </summary>
            /// <returns></returns>
            public byte[] ToBytes()
            {
                //ID in Byte[]
                byte[] idB = id.ToBytes();
                //Parts in Byte[]
                byte[] partsB = Parts.ToBytes();

                // count of byte
                int bytesPartImportace = (PartImportance.Length-1)/8+1;


                //Size of data =   idB.Length+IMPORTANCE+SizeOfParts+BytesOfPartImportance+KindNumber 
                byte[] line = new byte[idB.Length + 1 + partsB.Length+ bytesPartImportace+1];
                line[0] = getTypeNumber();
                idB.CopyTo(line, 1);
                line[idB.Length+1] = imp.Importance;
                partsB.CopyTo(line, idB.Length + 2);

                PartImportance.CopyTo(line, line.Length  - bytesPartImportace);


                // Format of bytes is [ID Importance Parts] 
                return line;
            }

            /// <summary>
            /// return TypeNumber of T
            /// </summary>
            /// <returns></returns>
            private static byte getTypeNumber()
            {
                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(T).TypeHandle);
                return Kind<T>.TypeNumber;
            }
        
            /// <summary>
            /// Fill struct with data from 0-byte and return byte[] without used data 
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public byte[] FillState(byte[] data)
            {
                int offset = 1;
                id = new ID(data.Skip(offset).Take(ID.IDLength).ToArray());
                offset += ID.IDLength;
                this.imp.Importance = data[offset];
                offset++;
                values = new Components<T>(data.Skip(offset).Take(data[offset] + 1).ToArray());
                offset += data[offset] + 1;
                int bytesPartImportace = (values.Size - 1) / 8 + 1;
                PartImportance = new BitArray(data.Skip(offset).Take(bytesPartImportace).ToArray());
                offset += bytesPartImportace;
                return data.Skip(offset).ToArray();

            }

            /// <summary>
            /// Fill State by data from byte[] from offset and increment ofsset by lenght of used bytes
            /// </summary>
            /// <param name="data"></param>
            /// <param name="offset"></param>
            public void FillState(byte[] data, ref int offset)
            {
                offset++;
                id = new ID(data.Skip(offset).Take(ID.IDLength).ToArray());
                offset += ID.IDLength;
                this.imp.Importance = data[offset];
                offset++;
                values = new Components<T>(data.Skip(offset).Take(data[offset]+1).ToArray());
                offset += data[offset] + 1;
                int bytesPartImportace = (values.Size - 1) / 8 + 1;
                PartImportance = new BitArray(data.Skip(offset).Take(bytesPartImportace).ToArray());
                offset += bytesPartImportace;

            }

            /// <summary>
            /// Create string informat: ID Importance values 
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.Id.ToString()+" "+this.Importance.Importance+" "+values.ToString();
            }

            /// <summary>
            /// Set value at given position and all neccesarry flags
            /// </summary>
            /// <param name="position"></param>
            /// <param name="value"></param>
            /// <returns></returns>
            public bool SetValue(int position, byte value)
            {
                if (0 <= position &&position < this.Parts.Size)
                {
                    Parts.Data[position] = value;
                    PartImportance[position] = true;
                }
                return false;
            }

            /// <summary>
            /// Set given importance to all parts or given range.
            /// Wrong range or out of boundary is cut 
            /// </summary>
            /// <param name="value"></param>
            public void SetImportanceAll(bool value, int start = 0, int numbers = -1)
            {
                if (start < 0) return;
                    if(numbers == -1)
                    {
                        numbers = this.Parts.Size-start;
                    }
                    for (int i = start; i < start+numbers && i < this.Parts.Size; i++)
                    {
                        PartImportance[i] = value;
                    }
            }

        }


        /// <summary>
        /// Interface to define basic container with values of each tool
        /// </summary>
        public interface IParts
        {

            /// <summary>
            /// Get a whole new data
            /// </summary>
            /// <returns></returns>
            byte[] GetData();


            /// <summary>
            /// Set a whole new data
            /// </summary>
            /// <returns></returns>
            bool TrySetData(byte[] newData);

            /// <summary>
            /// Work with each byte 
            /// </summary>
            byte[] Data { get; set; }

            /// <summary>
            /// Hard set size of the data length 
            /// </summary>
            int Size { get; }

            /// <summary>
            /// Return whole message in byte[]
            /// </summary>
            /// <returns></returns>p[;
            byte[] ToBytes();


            /// <summary>
            /// Fill given Ipart wit data from bytes
            /// </summary>
            /// <param name="bytes"></param>
            /// <param name="offset"></param>
            void FillParts(byte[] bytes);

            /// <summary>
            /// Realloc space for data
            /// </summary>
            void ReAllocData();
        }



        /// <summary>
        /// Basic struct for all eqipments in the project. Size of array depends on kind.
        /// </summary>
        public struct Components<T> : IParts where T : Kind<T>
        {

            /// <summary>
            /// Length of Parts
            /// </summary>
            static byte size;

            /// <summary>
            /// Just set size, don't alloc memory.
            /// </summary>
            static Components()
            {
                // Getting information from generic type 
                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(T).TypeHandle);
                size = Kind<T>.Size;


            }
            /// <summary>
            /// Static constructor, set size and alloc memmory-IF alloc=true
            /// </summary>
            public Components(bool alloc = true)
            {
                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(T).TypeHandle);
                size = Kind<T>.Size;
                if (alloc)
                    Data = new byte[size];
                else
                    Data = null;
            }

            /// <summary>
            /// Fill Components Data from arguments by Kind size from the begining 
            /// </summary>
            /// <param name="data">Data for Component</param>
            public Components(byte[] data)
            {

                System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(T).TypeHandle);
                size = Kind<T>.Size;
                this.Data = data.Skip(1).Take(size).ToArray();

            }

            /// <summary>
            /// Fill by data from bytes
            /// </summary>
            /// <param name="bytes"></param>
            public void FillParts(byte[] bytes)
            {                
                this.Data = bytes.Skip(1).Take(size).ToArray();
            }

            /// <summary>
            /// Realloc data
            /// </summary>
            public void ReAllocData()
            {
                Data = new byte[size];
            }

            /// <summary>
            /// Transform Component to byte[] in format (byte)Length[Parts]
            /// </summary>
            /// <returns></returns>
            public byte[] ToBytes()
            {
                //Make byte[], where on 1. position is length of data Parts 
                byte[] line = new byte[size + 1];
                line[0] = size;            
                Data.CopyTo(line, 1);
                return line;

            }

            /// <summary>
            /// Data for current equipment
            /// </summary>
            public byte[] Data { get; set; }

            /// <summary>
            /// Size of the data length 
            /// </summary>
            public int Size { get => size; }


            public byte[] GetData() { return Data; }

            /// <summary>
            /// If argument can be used as data for this equipment -> true and set data, else -> false and nothing
            /// </summary>
            /// <param name="newData"></param>
            /// <returns></returns>
            public bool TrySetData(byte[] newData)
            {
                if (newData.Length != Size)
                    return false;
                Data = newData;
                return true;
            }

            /// <summary>
            /// Make string with information from Component in format nm:part
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                int i = 0;
                foreach (var item in this.Data)
                {
                    sb.Append(i + ":" + item + " ");

                }
                return sb.ToString();
            }

        }

    }



    public struct TimeStamp
    {
        public Int32 MilisecondsStamp;
        public Int64 DateTimeStamp;


        public TimeStamp(Int64 dateTime, Int32 miliseconds)
        {
            MilisecondsStamp = miliseconds;
            DateTimeStamp = dateTime;
        }

        public void Set(Int64 dateTime, Int32 miliseconds)
        {
            MilisecondsStamp = miliseconds;
            DateTimeStamp = dateTime;
        }

        public bool NewerOrTheSame(Int64 dateTime, Int32 miliseconds)
        {

            if (DateTimeStamp < dateTime) return true;
            if (DateTimeStamp > dateTime) return false;
            if (MilisecondsStamp > miliseconds) return false;
            return true;

        }

        public bool NewerOrTheSame(TimeStamp st)
        {

            if (DateTimeStamp < st.DateTimeStamp) return true;
            if (DateTimeStamp > st.DateTimeStamp) return false;
            if (MilisecondsStamp > st.MilisecondsStamp) return false;
            return true;

        }

        /// <summary>
        /// Create Int64 with year, month, day, hour, minute, second
        /// </summary>
        /// <returns></returns>
        public static Int64 GetTimeStampDateTime()
        {
            Int64 timestamp = DateTime.Now.ToBinary();
            return timestamp;
        }

        /// <summary>
        /// Create Int32 with miliseconds
        /// </summary>
        /// <returns></returns>
        public static Int32 GetTimeStampMilisecond()
        {
            Int32 timestamp = DateTime.Now.Millisecond;
            return timestamp;

        }

        /// <summary>
        /// Create whole time stamp
        /// </summary>
        /// <returns></returns>
        public TimeStamp GetTimeStamp()
        {
            return new TimeStamp(DateTime.Now.ToBinary(), DateTime.Now.Millisecond);
        }

        public byte[] Tobytes()
        {
            return null;
        }

    }

}
