﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lights;
using Lights.Gate;
using Lights.Messages;
using Lights.Transport;


namespace Lights
{

    namespace Comunication
    {
        /// <summary>
        /// Client data linea - Prepared background for  comunication between server and one Client 
        /// </summary>
        public class Client : ComunicationClass
        {

            /// <summary>
            /// 
            /// </summary>
            public string lastUsedName { get; internal set; }
            
            /// <summary>
            /// 
            /// </summary>
            public string lastUsedPassword { get; internal set; }

            /// <summary>
            /// Client is connected to server
            /// </summary>
            public bool ConnectedToServer { get; internal set; }

            /// <summary>
            /// IID of server  
            /// </summary>
            public IID ServerID { get; internal set; }

            /// <summary>
            /// IID of Client using this background
            /// </summary>
            public IID ClientID { get; internal set; }

            /// <summary>
            /// Set Client id
            /// </summary>
            /// <param name="id"></param>
            public void SetClientID(IID id)
            {
                ClientID = id;
            }

            public void SetServerID(IID id)
            {
                ServerID = id;
            }

            /// <summary>
            /// Communication with server 
            /// </summary>
            GateMessage GateServer;


           
            /// <summary>
            /// Servers which send introduce message
            /// </summary>
            public List<IID> Servers { get; }

           

            /// <summary>
            /// Flag of working 
            /// </summary>
            bool isActive;


            /// <summary>
            /// Flag of working 
            /// </summary>
            bool IsActive { get => isActive; }

            /// <summary>
            /// Port of communication with server
            /// </summary>       
            public int Port { get; internal set; }



            /// <summary>
            /// Start server
            /// </summary>
            /// <param name="Port"></param>
            public Client(IPAddress serverIP, int Port = 6666)
            {
                ConnectedToServer = false;
                isActive = true;
                //Get my IP
                ClientID = CreateID(Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork));
                ServerID = CreateID(serverIP);
                this.Port = Port;

              


               
                Servers = new List<IID>();
                
                GateServer = new GateMessage(Port);

                processor = new Thread(Processor);
                processor.Start();

                UpdateServer = new Thread(updateServer);
                UpdateServer.Start();

            }

            /// <summary>
            /// Thread sorting
            /// </summary>
            Thread processor;

            /// <summary>
            /// Thread updating data on server
            /// </summary>
            Thread UpdateServer;

            /// <summary>
            /// Turn off the Server
            /// </summary>
            public void Shutdown()
            {
                isActive = false;
                GateServer.Shutdown();
                processor.Abort();
                UpdateServer.Abort();



            }

            int connectTime = 5000;

            /// <summary>
            /// Classify messeges and sort them to correct Lists
            /// </summary>
            private void Processor()
            {
                Message got;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                while (isActive)
                {
                    got = GateServer.Get();

                    if (got != null)
                    {
                        stopWatch.Reset();                        
                        callCorrectAction(got);
                    }
                    else
                    {
                        if(stopWatch.Elapsed.Milliseconds> connectTime)
                        {
                            Login(lastUsedName, lastUsedPassword);
                        }
                        if (stopWatch.Elapsed.Milliseconds > 2*connectTime)
                        {
                            ConnectedToServer = false;
                        }
                        Thread.Yield();
                    }

                }
            }


            private void callCorrectAction(Message msg)
            {
                if (msg.GetType() == typeof(Scene))
                {
                    processMsg(((Scene)msg)); return;
                }

                if (msg.GetType() == typeof(Chat))
                {
                    processMsg(((Chat)msg)); return;
                }

                if (msg.GetType() == typeof(LogInConfirm))
                {
                    processMsg(((LogInConfirm)msg)); return;
                }

                if (msg.GetType() == typeof(LogOutConfirm))
                {
                    processMsg(((LogOutConfirm)msg)); return;
                }


                if (msg.GetType() == typeof(IntroducingClient))
                {
                    processMsg(((IntroducingClient)msg)); return;
                }

                if (msg.GetType() == typeof(IntroducingServer))
                {
                    processMsg(((IntroducingServer)msg)); return;
                }

                if (msg.GetType() == typeof(Error))
                {
                    processMsg(((Error)msg)); return;
                }

                // Try Dynamic 
                dynamic m = Convert.ChangeType(msg, msg.GetType());
                processMsg(m);
            }


            /// <summary>
            /// IMessage - general message - not supported 
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(IMessage msg)
            {
                ErrorsCFO = true;
                Errors.Add("Try to process" + msg.GetType());
            }

            /// <summary>
            /// Login of  Equipment
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Error msg)
            {
                ErrorsCFO = true;
                string errMessage = msg.MSG + " < " + msg.Sender;
                Errors.Add(errMessage);

                //Somebody is accepting messages about update
                errorsUpdate?.Invoke(errMessage);
            }


            /// <summary>
            /// Server confirm connection
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(LogInConfirm msg)
            {
                ConnectedToServer = true;
                ErrorsCFO = true;
                Errors.Add("Successful Login");
            }


            /// <summary>
            /// Server confirm disconection
            /// </summary>
            /// <param name="msg"></param>
            private void Nastaví (LogOutConfirm msg)
            {
                ConnectedToServer = false;
                ErrorsCFO = true;
                Errors.Add("Succesful Logout");
            }

            /// <summary>
            /// IntroducingClient - ignore 
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(IntroducingClient msg)
            {
            }

            /// <summary>
            /// IntroducingServer - add server to list of servers
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(IntroducingServer msg)
            {
                lock (Servers)
                {
                    Servers.Add(msg.Sender);
                }
            }

            /// <summary>
            /// Add message to privite list with chat between Clients and server
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Chat msg)
            {
                ConversationCFO = true;
                Conversation.Add(msg.Text);

                //Somebody is accepting messages about update
                conversationUpdate?.Invoke(msg.Text);
            }

            TimeStamp LastRecievedDataStamp = new TimeStamp(0, 0);

            /// <summary>
            /// Update data for equipments by recieved data from server 
            /// </summary>
            /// <param name="msg"></param>
            private void processMsg(Scene msg)
            {
                if (LastRecievedDataStamp.NewerOrTheSame(msg.DateTimeStamp, msg.MilisecondStamp))

                {
                    DataCFO = true;
                    lock (Data)
                    {
                        foreach (var item in msg.EquipmentsData)
                        {


                            if (Data.ContainsKey(item.Id))
                            {
                                item.Parts.Data.CopyTo(Data[item.Id].Parts.Data, 0);
                            }
                            else
                                Data.Add(item.Id, item);
                            // 
                            Data[item.Id].PartImportance.SetAll(false);

                        }
                    }
                }
                //Somebody is accepting messages about update
                dataUpdate?.Invoke(msg);


            }


            /// <summary>
            /// Send text message to server
            /// </summary>
            /// <param name="message"></param>
            public void MessageToServer(string msg)
            {
                var message = new Chat(msg);
                Send(message);
            }

            /// <summary>
            /// Send Login request to Server 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="password"></param>
            public void Login(string name = "None", string password = "")
            {
                lastUsedName = name;
                lastUsedPassword= password;
                LogIn message = new LogIn(name, password);
                Send(message);
            }

            

            /// <summary>
            /// Send Lougout request to Client
            /// </summary>
            public void Logout()
            {
                LogOut message = new LogOut();
                this.Send(message);
            }

            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// Set Sender - Client
            /// Set Recipient - Server
            /// </summary>
            /// <param name="msg"></param>
            public void Send<T>(T msg) where T : Message
            {
                msg.Sender = GetIDForCommunication(ServerID,Port);
                msg.Recipient = ServerID;
                GateServer.Send(msg);
            }


            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// Set Sender - Client
            /// Set Recipient - Broadcast
            /// </summary>
            /// <param name="msg"></param>
            public void SendBroadCast<T>(T msg) where T : Message
            {
                msg.Sender = ClientID;
                msg.Recipient = CreateID(IPAddress.Broadcast);
                GateServer.Send(msg);
            }

            /// <summary>
            /// Give data to Gate (Gate Enqueue the data and send as soon as possible) 
            /// DO NOT SET SENDER AND RECIPIENT
            /// </summary>
            /// <param name="msg"></param>
            public void PureSend<T>(T msg) where T : Message
            {
                GateServer.Send(msg);
            }



            /// <summary>
            /// Create Scene message containing current state of lights and send it to server
            /// </summary>
            /// <returns></returns>
            public void SendUpdateToServer()
            {
                flagSendUpdate = true;
            }

            bool flagSendUpdate = false;
            private void updateServer()
            {
                while (isActive)
                {
                    if (flagSendUpdate)
                    {
                        flagSendUpdate = false;
                        Scene IntroducingMsg = new Scene();
                        IntroducingMsg.DateTimeStamp = TimeStamp.GetTimeStampDateTime();
                        IntroducingMsg.MilisecondStamp = TimeStamp.GetTimeStampMilisecond();
                        var IntroduceData = Data;
                        lock (IntroduceData)
                            foreach (var item in IntroduceData)
                            {
                                IntroducingMsg.EquipmentsData.Add(item.Value);
                            }

                        Send(IntroducingMsg);
                    }
                    else
                    {
                        Thread.Yield();
                    }

                }

            }


            const int TimeConstant = 3000;
            public static IID GetServerID(int port, int time =TimeConstant)
            {
                
                /// Unsafe Client just to get ip of Client
                var Client = new Client(null,port);
               
                Client.SendBroadCast(new Lights.Messages.IntroducingClient());
                //Wait - trying find server
                for (int i = 0; i < 10; i++)
                {
                    System.Threading.Thread.Sleep(time / 10);
                    if (Client.Servers.Any())
                    {
                        break;
                    }
                }

                
                IID ServerID= null;
                if (Client.Servers.Any())
                    ServerID =Client.Servers.First();
                Client.Shutdown();
                Client = null;

                return ServerID;
            } 

        }
    }
}
