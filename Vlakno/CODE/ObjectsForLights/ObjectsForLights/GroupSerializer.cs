﻿using Lights.EquipmentsKinds;
using Lights.Transport;
using Lights.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lights
{
    namespace Parsers
    {
        

        /// <summary>
        /// Struct to containing information about nickname
        /// </summary>
        public struct PartNick
        {
            public bool All;
            public int Offset;
            public ID Id;

            /// <summary>
            /// Create PartNick and fill with default values
            /// </summary>
            /// <param name="id"></param>
            /// <param name="all"></param>
            /// <param name="offset"></param>
            public PartNick(ID id, bool all = true, int offset = 0)
            {
                Id = id;
                All = all;
                Offset = offset;
            }
        }


        /// <summary>
        /// State of group container
        /// </summary>
        public sealed class GroupState
        {

            public GroupState(Type typeOfGroup)
            {
                members = new List<Equipment>();
                TypeOfGroup = typeOfGroup;
            }
            public struct Equipment
            {
                public ID id;
                public int level;
                public string name;
                public int offset;
                public bool IsInverse;
            }

            public string Name { get; set; }
            public Type TypeOfGroup { get; set; }
            public List<Equipment> Members { get => members; }


            List<Equipment> members;

            public string kind;
            public void AddMember(ID id, string name, int offset = 0, bool inverse = false, int level = 100)
            {
                Equipment newMember;
                newMember.id = id;
                newMember.level = level;
                newMember.name = name;
                newMember.offset = offset;
                newMember.IsInverse = inverse;
                members.Add(newMember);
            }

        }


        /// <summary>
        /// Serialization of GRoupSatate to Json
        /// </summary>
        public static class GroupSerializer
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="mapa"></param>
            /// <returns></returns>
            public static bool TrySaveGroupData(string filename, List<GroupState> groups)
            {
                
                return ParserJsonList<GroupState>.TrySave(filename, groups);

            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="groups"></param>
            /// <returns></returns>
            public static bool TryLoadGroupData(string filename, out List<GroupState> groups)
            {
                
                return ParserJsonList<GroupState>.TryLoad(filename, out groups);
            }
        }
    }
}
