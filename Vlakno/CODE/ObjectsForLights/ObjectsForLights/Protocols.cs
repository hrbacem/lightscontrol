﻿using Lights.EquipmentsKinds;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lights
{
    namespace Protocols
    {


        /// <summary>
        /// Transform string to correct protocol
        /// </summary>
        public static class ProtocolFactory
        {
            /// <summary>
            /// Transform string to correct protocol,
            /// Add default Value to defaults Values if it is possible create default state for this equipment
            /// </summary>
            /// <param name="protocolTXT"></param>
            /// <param name="id"></param>
            /// <param name="defaultValues"></param>
            /// <returns></returns>
            public static IProtocol ChooseProtocol(string protocolTXT, ID id = default(ID), Dictionary<IID, IState> defaultValues = null)
            {
                IProtocol protocol = null;
                if (protocolTXT == null || protocolTXT == "") return protocol;
                string protocolName = protocolTXT.Split('.').Last();
                switch (protocolName)
                {

                    case "AsciiRGB":
                        {

                            protocol = new AsciiRGB();
                            if (defaultValues == null) return protocol;
                            Components<RGBLight> Data = new Components<RGBLight>(true);
                            protocol.DefaultMaxValue.CopyTo(Data.Data, 0);

                            State<RGBLight> state = new State<RGBLight>(id, 1, Data);
                            defaultValues.Add(id, state);
                        }
                        break;

                    case "AsciiRGBWrong":
                        {

                            protocol = new AsciiRGBWrong();
                            if (defaultValues == null) return protocol;
                            Components<RGBLight> Data = new Components<RGBLight>(true);
                            protocol.DefaultMaxValue.CopyTo(Data.Data, 0);

                            State<RGBLight> state = new State<RGBLight>(id, 1, Data);
                            defaultValues.Add(id, state);
                        }
                        break;

                    case "AsciiSpyder":
                        {
                            protocol = new AsciiSpyder();
                            if (defaultValues == null) return protocol;
                            Components<SpyderLight> Data = new Components<SpyderLight>(true);
                            protocol.DefaultMaxValue.CopyTo(Data.Data, 0);

                            State<SpyderLight> state = new State<SpyderLight>(id, 1, Data);
                            defaultValues.Add(id, state);
                        }
                        break;

                    case "AsciiDIMM":
                        {
                            protocol = new AsciiDIMM();
                            if (defaultValues == null) return protocol;
                            Components<DIMMLight> Data = new Components<DIMMLight>(true);
                            protocol.DefaultMaxValue.CopyTo(Data.Data, 0);
                            State<DIMMLight> state = new State<DIMMLight>(id, 1, Data);
                            defaultValues.Add(id, state);

                        }
                        break;

                    default:
                        {

                            return null;

                        }



                }
                return protocol;
            }

            public static IProtocol GetProtocol(ImplementedProtocols protocol)
            {
                switch (protocol)
                {
                    case ImplementedProtocols.AsciiRGB: return new AsciiRGB();

                    case ImplementedProtocols.AsciiDIMM: return new AsciiDIMM();

                    case ImplementedProtocols.AsciiSpyder: return new AsciiSpyder();

                    case ImplementedProtocols.AsciiRGBWrong: return new AsciiRGBWrong();                    

                    default:
                        return null;
                }

            }

            /// <summary>
            /// Create string representation of protocol
            /// </summary>
            /// <param name="protocol"></param>
            /// <returns></returns>
            public static string ProtocolToString(IProtocol protocol)
            {
                return protocol.GetType().ToString();
            }

            public enum ImplementedProtocols
            {
                AsciiRGB,
                AsciiSpyder,
                AsciiDIMM,
                AsciiRGBWrong
            }

        }

        /// <summary>
        /// Technical implementations  
        /// </summary>
        public interface IProtocol
        {
            /// <summary>
            /// Create minimal Value for equipment with this protocol
            /// </summary>
            byte[] DefaultMinValue { get; }

            /// <summary>
            /// Create maximal Value for equipment with this protocol
            /// </summary>
            byte[] DefaultMaxValue { get; }
            /// <summary>
            /// How to make byte[] from State 
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            byte[] ToBytes(IState data);


            /// <summary>
            /// 
            /// </summary>
            ImplementedKinds KindOfEquipment { get; }

        }

        /// <summary>
        /// Basic Equipments with ASCII comunication 
        /// </summary>
        public abstract class Ascii
        {
            /// <summary>
            /// Making string value from byte - 3 char
            /// </summary>
            /// <param name="val"></param>
            /// <returns></returns>
            public static string byteTostring(byte val)
            {
                string valTXT = val.ToString();
                if (valTXT.Length == 1) return "00" + valTXT;
                if (valTXT.Length == 2) return "0" + valTXT;
                if (valTXT.Length == 3) return valTXT;

                throw new Exception("UNKNOWN VALUE");
            }
            ///In ths family are equipments controll by message starting with letter
            ///and then by bytes in strings, where byte is on three "place"
            /// 0->"000", 12->"012" ,122->"122"  
        }


        /// <summary>
        ///  ASCII RGB FAMILY
        /// </summary>
        public class AsciiRGB : Ascii, IProtocol
        {
            

            public  byte[] DefaultMinValue { get => new byte[] { 0, 0, 0 }; }

            public byte[] DefaultMaxValue { get => new byte[] { 255, 255, 255 }; }

            public ImplementedKinds KindOfEquipment => ImplementedKinds.RGBLight;

            /// <summary>
            /// Make bytes from State -> Make string "byteInStringInHunderedForm (1 -> "001", 254->"254" )  
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public byte[] ToBytes(IState data)
            {

                string R = byteTostring(data.Parts.Data[0]);
                string G = byteTostring(data.Parts.Data[1]);
                string B = byteTostring(data.Parts.Data[2]);
                string sb = "R" + R + G + B;
                
                return Encoding.ASCII.GetBytes(sb);


            }
        }





        /// <summary>
        ///  ASCII RGB FAMILY
        /// </summary>
        public class AsciiDIMM : Ascii, IProtocol
        {

            public byte[] DefaultMinValue { get => new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; }
            public byte[] DefaultMaxValue { get => new byte[] { 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 }; }

            public ImplementedKinds KindOfEquipment => ImplementedKinds.DIMMLight;

            /// <summary>
            /// Make bytes from State -> Make string "byteInStringInHunderedForm (1 -> "001", 254->"254" )  
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public byte[] ToBytes(IState data)
            {

                string P1 = byteTostring(data.Parts.Data[0]);
                string P2 = byteTostring(data.Parts.Data[1]);
                string P3 = byteTostring(data.Parts.Data[2]);
                string P4 = byteTostring(data.Parts.Data[3]);
                string P5 = byteTostring(data.Parts.Data[4]);
                string P6 = byteTostring(data.Parts.Data[5]);
                string P7 = byteTostring(data.Parts.Data[6]);
                string P8 = byteTostring(data.Parts.Data[7]);
                string P9 = byteTostring(data.Parts.Data[8]);
                string P10 = byteTostring(data.Parts.Data[9]);
                string P11 = byteTostring(data.Parts.Data[10]);
                string P12 = byteTostring(data.Parts.Data[11]);
                string sb = "D" + P1 + P2 + P3 + P4 + P5 + P6 + P7 + P8 + P9 + P10 + P11 + P12;
                return Encoding.ASCII.GetBytes(sb);


            }
        }


        /// <summary>
        ///  ASCII RGB FAMILY - forgot flag R
        /// </summary>
        public class AsciiRGBWrong : Ascii, IProtocol
        {
            public byte[] DefaultMinValue { get => new byte[] { 0, 0, 0 }; }

            public byte[] DefaultMaxValue { get => new byte[] { 255, 255, 255 }; }

            public ImplementedKinds KindOfEquipment => ImplementedKinds.RGBLight;

            /// <summary>
            /// Make bytes from State -> Make string "byteInStringInHunderedForm (1 -> "001", 254->"254" )  
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public byte[] ToBytes(IState data)
            {

                string R = byteTostring(data.Parts.Data[0]);
                string G = byteTostring(data.Parts.Data[1]);
                string B = byteTostring(data.Parts.Data[2]);
                string sb = R + G + B;

                return Encoding.ASCII.GetBytes(sb);


            }
        }

        public class AsciiSpyder : Ascii, IProtocol
        {
            public byte[] DefaultMinValue { get => new byte[] { 0, 0, 0, 90, 90 }; }

            public byte[] DefaultMaxValue { get => new byte[] { 255, 255, 255, 90, 90 }; }

            public ImplementedKinds KindOfEquipment => ImplementedKinds.SpyderLight;


            /// <summary>
            /// Make bytes from State -> Make string "S"+byteInStringInHunderedForm (1 -> "001", 254->"254" )  
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public byte[] ToBytes(IState data)
            {
                string R = byteTostring(data.Parts.Data[0]);
                string G = byteTostring(data.Parts.Data[1]);
                string B = byteTostring(data.Parts.Data[2]);

                string X = byteTostring(data.Parts.Data[3] < (byte)180 ? data.Parts.Data[3] : ((byte)180)); // Check value
                string Y = byteTostring(data.Parts.Data[4] < (byte)180 ? data.Parts.Data[4] : ((byte)180)); // Check value
                StringBuilder sb = new StringBuilder('S' + R + G + B + X + Y);

                return Encoding.ASCII.GetBytes(sb.ToString());
            }


        }
    }
}
