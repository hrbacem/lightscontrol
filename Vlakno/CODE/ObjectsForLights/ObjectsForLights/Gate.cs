﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lights.Messages;
using Lights.Transport;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using Lights.Gate;


namespace Lights
{
    namespace Gate
    {
        /// <summary>
        /// Generic version of  Gate - Accepting and sending generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public abstract class Gate<T>
        {
            /// <summary>
            /// Function which Get IPAddress from sended Message
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected abstract IPAddress GetIpAddress(T msg);


            /// <summary>
            /// Make byte[] from generic message
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected abstract byte[] ToByteArray(T msg);

            /// <summary>
            ///  Make generic message from byte[]
            /// </summary>
            /// <param name="data"></param>
            /// <param name="msg"></param>
            protected abstract void MessageMaker(byte[] data, out T msg, IPAddress IPA);


            /// <summary>
            /// Used connection
            /// </summary>
            Dictionary<IPAddress, IPEndPoint> Database;

            /// <summary>
            /// Port of comunication on this level
            /// </summary>
            int Port { get; }

            /// <summary>
            /// Messages to send 
            /// </summary>
            Queue<T> toSend = new Queue<T>();



            /// <summary>
            /// Recieved messages
            /// </summary>
            Queue<T> recieved = new Queue<T>();

            /// <summary>
            /// Thread gradually recieving messages
            /// </summary>
            Thread InComing;

            /// <summary>
            /// Thread gradually sending message
            /// </summary>
            Thread OutGoing;

            /// <summary>
            /// Flag, if this gate is still working
            /// </summary>
            bool isActive;

            bool IsActive { get => isActive; }

            UdpClient Client;
            /// <summary>
            /// While is gate active-> Recieve packets, transform them in generic messages and Enqueue them. 
            /// </summary>
            void inComing()
            {

                //Creates a UdpClient for reading incoming data.
                while (isActive)
                {

                    var epoint = new IPEndPoint(IPAddress.Any, Port);

                    byte[] receivedData;
                    receivedData = Client.Receive(ref epoint);


                    if (receivedData != null)
                    {
                        /* MakeMessage
                         * by specific method for generic type
                         * make message from bytes 
                         * different message for all types
                         */

                        MessageMaker(receivedData, out T msg, epoint.Address);
                        if(msg != null)
                        {
                            lock (recieved)
                                recieved.Enqueue(msg);
                        }
                        
                    }
                    else
                    {
                        Thread.Yield();
                    }

                }
            }

            /// <summary>
            /// While is gate active -> Dequeuq messages, transform them in byte[] and send them. 
            /// </summary>
            void outGoing()
            {

                while (isActive)
                {
                    lock (toSend)
                        if (toSend.Any())
                        {
                            T msg = toSend.Dequeue();
                            ///generic - Get IP from message
                            IPAddress IPaddress = GetIpAddress(msg);

                            ///Reusing connection if there already was
                            Database.TryGetValue(IPaddress, out IPEndPoint EP);
                            if (EP == null)
                            {
                                EP = new IPEndPoint(IPaddress, Port);
                                Database.Add(IPaddress, EP);
                            }

                            ///Transform message to byte array by special method
                            byte[] MsgInByte = ToByteArray(msg);



                            lock (Client)
                            {
                                Client.Send(MsgInByte, MsgInByte.Length, EP);
                            }
                        }
                        else
                        {
                            Thread.Yield();
                        }
                }
            }



            /// <summary>
            /// Constructor of gate -> Start all Threads 
            /// </summary>
            /// <param name="port"></param>
            public Gate(int port = 6666)
            {
                Database = new Dictionary<IPAddress, IPEndPoint>();
                this.Port = port;
                isActive = true;
                InComing = new Thread(inComing);
                OutGoing = new Thread(outGoing);


                Client = new UdpClient(Port);
                OutGoing.Start();
                InComing.Start();
            }

            /// <summary>
            /// Enqueue generic message in Que prepared to send
            /// </summary>
            /// <param name="msg"></param>
            public void Send(T msg)
            {
                lock (toSend)
                    toSend.Enqueue(msg);
            }

            /// <summary>
            /// Abort all threads and set Active flag on false  
            /// </summary>
            public void Shutdown()
            {
                isActive = false;
                OutGoing.Abort();
                InComing.Abort();
                Client.Close();

            }

            /// <summary>
            /// If there is message -> return Dequeued message else return default value 
            /// </summary>
            /// <returns></returns>
            public T Get()
            {
                lock (recieved)
                    if (recieved.Any())
                    {

                        return recieved.Dequeue();

                    }
                    else
                        return default(T);
            }

        }


        /// <summary>
        /// Implementation of abstract Generic<T> gate for IMessages
        /// </summary>
        public class GateMessage: Gate<Message>
        {


            /// <summary>
            /// Return IPv4 made from ID
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override IPAddress GetIpAddress(Message msg)
            {
                return msg.IIDtoIPv4(msg.Recipient);
            }

            /// <summary>
            /// Made byte[] from message by message method
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override byte[] ToByteArray(Message msg)
            {
                return msg.ToBytes();
            }

            /// <summary>
            /// Make message from byte by type of mssg
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            protected override void MessageMaker(byte[] data, out Message msg, IPAddress IPA)
            {
                Header.CreateCorrectMessage(data, out msg, IPA);
            }

            /// <summary>
            /// Setting methods for Getting IP, Convert to bytes, Convert from bytes 
            /// </summary>
            /// <param name="Port"></param>
            public GateMessage(int Port) : base(Port)
            {

            }
        }


        /// <summary>
        /// Implementation of abstract Generic<T> gate for ByteMessage
        /// </summary>
        public class GateByteMessage : Gate<ByteMessage>
        {

            /// <summary>
            /// Use IP part of message
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override IPAddress GetIpAddress(ByteMessage msg)
            {
                return msg.ip;
            }


            /// <summary>
            /// Use byte[] part of message
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override byte[] ToByteArray(ByteMessage msg)
            {
                return msg.message;
            }


            /// <summary>
            /// Create LowMessage where IP is IP recieved from sender and message is just recieved byte[] 
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            protected override void MessageMaker(byte[] data, out ByteMessage msg, IPAddress IPA)
            {
                msg.ip = IPA;
                msg.message = data;
            }

            /// <summary>
            /// Setting methods for Getting IP, Convert to bytes, Convert from bytes 
            /// </summary>
            /// <param name="Port"></param>
            public GateByteMessage(int Port) : base(Port)
            {

            }
        }


        public interface IGateSendable
        {
            /// <summary>
            /// Get ip addres of recipient from message
            /// </summary>
            /// <returns></returns>
            IPAddress GetIpAddressOfRecipient();

            /// <summary>
            /// Serialize message to byte arrya for transmit
            /// </summary>
            /// <returns></returns>
            byte[] MessageToByteArray();

            /// <summary>
            ///  From serialized data and IP address of sender fill information in message 
            /// </summary>
            /// <param name="data"></param>
            /// <param name="IPA"></param>
            void FillMessageData(byte[] data, IPAddress IPA);
        }


        /// <summary>
        /// Implementation of abstract Generic<T> gate for IGateSendable
        /// </summary>
        public class GateMessage<T> : Gate<T> where T : IGateSendable, new()
        {


            /// <summary>
            /// Return IPv4 made from ID
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override IPAddress GetIpAddress(T msg)
            {
                return msg.GetIpAddressOfRecipient();
            }

            /// <summary>
            /// Made byte[] from message by message method
            /// </summary>
            /// <param name="msg"></param>
            /// <returns></returns>
            protected override byte[] ToByteArray(T msg)
            {
                return msg.MessageToByteArray();
            }

            /// <summary>
            /// Make message from byte 
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            protected override void MessageMaker(byte[] data, out T msg, IPAddress IPA)
            {
                msg = new T();
                msg.FillMessageData(data, IPA);
            }

            /// <summary>
            /// Setting methods for Getting IP, Convert to bytes, Convert from bytes 
            /// </summary>
            /// <param name="Port"></param>
            public GateMessage(int Port) : base(Port)
            {

            }
        }

    }
}
