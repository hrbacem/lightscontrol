﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lights
{

    
    /// <summary>
    /// Objects for communication on the Client level - server-Clients, Clients-Clients
    /// </summary>
    namespace Messages
    {

        using EquipmentsKinds;
        using System.Net;
        using Transport;

        /// <summary>
        /// Messages for communication on Client level
        /// </summary>
        public interface IMessage
        {
            /// <summary>
            /// Id of equipment that sent this message
            /// </summary>
            IID Sender { get; set; }

            /// <summary>
            /// Id of recipient of this message
            /// </summary>
            IID Recipient { get; set; }

            
            /// <summary>
            /// Extract IP address from  IID
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            IPAddress IIDtoIPv4(IID id);
           
            

            /// <summary>
            /// Return whole message in byte[]
            /// </summary>
            /// <returns></returns>p[;
            byte[] ToBytes();


        }

        public abstract class Header
        {
            

            protected enum ImplementedMessages : byte
            {
            TextComunication = 200,
            LoginMessage = 10,
            LogoutMessage = 11,
            IntrducingClientMessage = 12,
            LogInConfirmMessage = 20,
            LogOutConfirmMessage = 21,
            IntrducingServerMessage = 22,
            ErrorMessage = 9,
            SceneDataMessage = 1,
            DefaultMessage = 0
            }

            
            public static void CreateCorrectMessage(byte[] data, out Message msg, IPAddress IPA)
            {
                try
                {
                    switch ((ImplementedMessages)(data[ID.IDLength * 2]))
                    {


                        case ImplementedMessages.ErrorMessage:
                            {
                                msg = new Error(data);
                            }
                            break;
                        case ImplementedMessages.LoginMessage:
                            {
                                msg = new LogIn(data);
                            }
                            break;

                        case ImplementedMessages.LogInConfirmMessage:
                            {
                                msg = new LogInConfirm(data);
                            }
                            break;

                        case ImplementedMessages.LogoutMessage:
                            {
                                msg = new LogOut(data);
                            }
                            break;

                        case ImplementedMessages.LogOutConfirmMessage:
                            {
                                msg = new LogOutConfirm(data);
                            }
                            break;

                        case ImplementedMessages.IntrducingServerMessage:
                            {
                                msg = new IntroducingServer(data);
                            }
                            break;

                        case ImplementedMessages.IntrducingClientMessage:
                            {
                                msg = new IntroducingClient(data);
                            }
                            break;


                        case ImplementedMessages.TextComunication:
                            {
                                msg = new Chat(data);
                            }
                            break;

                        //Scene option
                        case ImplementedMessages.SceneDataMessage:
                            {
                                msg = new Scene(data);
                            }
                            break;

                        default:
                        case ImplementedMessages.DefaultMessage:
                            {
                                msg = new Message(data);
                            }
                            break;


                    }
                }
                catch (Exception e)
                {
                    msg = new Error("Deserialization: "+e.ToString());
                }

            }

        }


        /// <summary>
        /// Basic box - just  Sender and Recipient 
        /// </summary>
        public class Message :Header, IMessage
        {
            /// <summary>
            /// Length of Identificator
            /// </summary>
            public int IDLength = ID.IDLength;

           
            /// <summary>
            /// Sender of this message
            /// </summary>
            public IID Sender { get; set; }

            /// <summary>
            /// Recipient of this message
            /// </summary>
            public IID Recipient { get; set; }

            /// <summary>
            /// Kind of message - need know from table
            /// </summary>
            internal byte Type = 0;

            /// <summary>
            /// Alocation of all Parts
            /// </summary>
           public Message()
            {
                ID IS = new ID
                {
                    Parts = new byte[ID.IDLength]
                };
                this.Sender =IS;
                ID IR = new ID
                {
                    Parts = new byte[ID.IDLength]
                };
                this.Recipient = IR;
            }


            /// <summary>
            /// Constructor of message from byte[]
            /// </summary>
            /// <param name="data"></param>
            public Message(byte[] data)
            {
                // Make message by knowing structure 

                ID IS = new ID
                {
                    Parts = new byte[ID.IDLength]
                };
                this.Sender = IS;
                ID IR = new ID
                {
                    Parts = new byte[ID.IDLength]
                };
                this.Recipient = IR;
                //Read byte by byte ID 

               
                for (int i = 0; i < IDLength; i++)
                {
                    this.Sender.Parts[i] = data[i];
                    this.Recipient.Parts[i] = data[i + IDLength];
                }
                
                //The type is the last byte of recieved data
                this.Type = data[IDLength + IDLength];

            }
            


            /// <summary>
            /// Make IPv4 from ID (from first 4 byte of ID)
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public IPAddress IIDtoIPv4(IID id)
            {
                //Just transform first four bytes to IP
                byte[] ID = id.Parts;
                byte[] ip = { ID[0] , ID[1] , ID[2], ID[3] };
                IPAddress IP = new IPAddress(ip);
                return IP;
            }

            
            /// <summary>
            /// Make byte[] from class in order - [sender][recipient][Type]
            /// </summary>
            /// <returns></returns>
            public virtual byte[] ToBytes()
            { 
                //Knowing structure - 2*ID + TYPE
                byte[] bytes = new byte[IDLength+ IDLength+1];
                Sender.Parts.CopyTo(bytes, 0);
                Recipient.Parts.CopyTo(bytes, IDLength);
                bytes[IDLength + IDLength] = Type;
                return bytes; 
                 
            }

            /// <summary>
            /// Create string: Sender: SenderIID Recipient: RecipientIID Type: [KindOfMessage];
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return "Sender: "+this.Sender.ToString()+" Recipient: "+this.Recipient.ToString()+" Type: "+this.Type;
            }

            

        }

        /// <summary>
        /// Message with data for equipments 
        /// </summary>
        public class Scene : Message, IMessage
        {
            public List<IState> EquipmentsData;

            /// <summary>
            /// Constructor- Set Type and allocate place for data
            /// </summary>
            public Scene() :base()
            {
                Type = (byte)ImplementedMessages.SceneDataMessage;
                EquipmentsData= new List<IState>();
            }


            /// <summary>
            /// Make class from byte[]
            /// </summary>
            /// <param name="data"></param>
            public Scene(byte[] data) :base(data)
            {
                //Base Construct the ID of sender and recipieant + type 
                EquipmentsData = new List<IState>();
                int offset = base.IDLength * 2 + 1;
                byte[] leftover = data.Skip(offset).ToArray();
                offset = 0;

                //WORK
                //insert reading of time stamp
                DateTimeStamp = BitConverter.ToInt64(leftover, offset);
                offset += 8;
                MilisecondStamp = BitConverter.ToInt32(leftover, offset);
                offset += 4;

                //Must read all states coded in byte[]
                while (offset<leftover.Length)
                {
                    //Create in run time Instance of IState just by type byte
                    var dataType = new Type[] { KindSerialization.TypeNumberToKind(TypeFromBytes(ref leftover,ref offset)) };
                    var genericBase = typeof(State<>);
                    var StetOfGenericType = genericBase.MakeGenericType(dataType);
                    var StateOfGenType = Activator.CreateInstance(StetOfGenericType);                                       
                    IState equipment = (IState)StateOfGenType;

                    //Fill state and move offset 
                    equipment.FillState(leftover, ref offset);
                    
                    EquipmentsData.Add(equipment);
                    
                }
            }

            /// <summary>
            /// Extract from data length of Parts
            /// </summary>
            /// <param name="data"></param>
            /// <param name="offset"></param>
            /// <returns></returns>
            private byte TypeFromBytes(ref byte[] data,ref int offset)
            {
                return data[offset];
            }
            

            /// <summary>
            /// Make byte[] from Scene, prepared to send by udp
            /// </summary>
            /// <returns></returns>
            public override byte[] ToBytes()
            {
                /*bytes
                 Make byte[] by protocol
                 - head - IIDs + type by parent
                 -Transform each state in scene to bytes
                 -Connect all states in bytes
                 -Connect all together 
                 */
                byte[] head= base.ToBytes();

                //WORK
                //insert witing down of time stamp
                byte[] DateTimeBytes = BitConverter.GetBytes(DateTimeStamp);
                byte[] MilisecondBytes = BitConverter.GetBytes(MilisecondStamp);
                

                List<byte[]> Bytes = new List<byte[]>();
                int size = 0;
                //Transform each state in scene to bytes
                foreach (var item in EquipmentsData)
                {
                    byte[] itemInBytes=item.ToBytes();
                    size += itemInBytes.Length;
                    Bytes.Add(itemInBytes);
                }
                byte[] body = new byte[size];
                int gone = 0;
                for (int i = 0; i < Bytes.Count; i++)
                {
                    Bytes[i].CopyTo(body,gone);
                    gone += Bytes[i].Length;
                }
                //connect
                byte[] packet = new byte[head.Length + body.Length+ DateTimeBytes.Length+ MilisecondBytes.Length];
                head.CopyTo(packet, 0);
                DateTimeBytes.CopyTo(packet, head.Length);
                MilisecondBytes.CopyTo(packet, head.Length + DateTimeBytes.Length);
                body.CopyTo(packet, head.Length + DateTimeBytes.Length + MilisecondBytes.Length);
                return packet;
                
            }
            /// <summary>
            /// Make string with data in Client-friendly form
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                StringBuilder sb=new StringBuilder(base.ToString() + "\n DATA: \n");
                foreach (var item in this.EquipmentsData)
                {
                    sb.Append(item.ToString()+"\n");
                }
                return sb.ToString();
            }

            public Int64 DateTimeStamp { get; internal set; }

            public Int32 MilisecondStamp { get; internal set; }

            
            public void SetTimeStamp(Int64 dateTimeStamp, Int32 milisecondStamp)
            {
                DateTimeStamp = dateTimeStamp;
                MilisecondStamp = milisecondStamp;
            }

        }

        /// <summary>
        /// Clients Comunication
        /// </summary>
        public class Chat : TextMessages
        {
            /// <summary>
            /// Message for Client
            /// </summary>
            public string Text;

            /// <summary>
            /// Set Type of message
            /// </summary>
            public Chat() : base()
            {
                Type = (byte)ImplementedMessages.TextComunication;
            }

            /// <summary>
            /// Set Type of message and text to send
            /// </summary>
            public Chat(string txt) : base()
            {
                Text = txt;
                Type = (byte)ImplementedMessages.TextComunication;
            }

            /// <summary>
            /// Set Type of message, text to send, Id of reciever
            /// </summary>
            public Chat(string txt,ID reviever) : base()
            {
                Recipient = reviever;
                Text = txt;
                Type = (byte)ImplementedMessages.TextComunication;
            }

            /// <summary>
            /// Make byte[] from data of this class
            /// </summary>
            /// <returns></returns>
            public override byte[] ToBytes()
            {
                // make head like parent
                byte[] head = base.ToBytes();
                byte[] body = Encoding.ASCII.GetBytes(Text);
                // body is just Encoded text
                
                //Connected
                byte[] packet=new byte[head.Length+body.Length];
                head.CopyTo(packet,0);
                body.CopyTo(packet,head.Length);
                return packet;
            }

            
            /// <summary>
            /// Construct class from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public Chat(byte[] data) : base(data)
            {
                //just Encoded rest of bytes to text
                this.Text = Encoding.ASCII.GetString(data.Skip(1 + 2 * IDLength).ToArray());                                
            }

        }


        public abstract class TextMessages : Message, IMessage
        {
            public TextMessages(byte[] data) : base(data)
            {

            }

            public TextMessages() : base()
            {

            }
            protected const char separator = '$';
            /// <summary>
            /// Escape all symbols in given word
            /// </summary>
            /// <param name="word"></param>
            /// <returns></returns>
            protected string ecapeSymbols(string word)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var character in word)
                {
                    if (character == separator) sb.Append('\\');
                    sb.Append(character);
                }
                return sb.ToString();
            }

            protected List<string> extractTextFields(string text)
            {
                
                List<string> fields = new List<string>();

                StringBuilder wordBuilder = new StringBuilder();

                int i = 0;
                for (; i < text.Length ; i++)
                {
                    if (text[i] == separator)
                    {
                        fields.Add(wordBuilder.ToString());
                        wordBuilder.Clear();
                    }
                    else
                    {
                        if (text[i] == '\\' && i + 1 < text.Length && text[i+1] ==separator)
                        {
                            i++; // read symbol after \
                        }
                        wordBuilder.Append(text[i]);
                    }
                    
                }
                fields.Add(wordBuilder.ToString());
                


                return fields;
            }
        }

        /// <summary>
        /// Login to server for admissing of data
        /// </summary>
        public class LogIn : TextMessages
        {

            /// <summary>
            /// Construct message with Nick and password
            /// (default Nick:"None") and (default password:"")
            /// </summary>
            /// <param name="Nick"></param>
            public LogIn(string Nick="None",string Password = "") : base()
            {
                Type = (byte)ImplementedMessages.LoginMessage;

                this.Nick = Nick;
                this.Password = Password;
            }

            
            /// <summary>
            /// Nick of Client 
            /// </summary>
            /// Clients can use same Nicknames
            public string Nick;

            /// <summary>
            /// Passwor of Client
            /// </summary>
            /// Client sing in with password
            public string Password;

            /// <summary>
            /// Make byte[] from class
            /// </summary>
            /// <returns></returns>
            public override byte[] ToBytes()
            {
                //Basic message with text - Nick - for easier human work with data
                byte[] head = base.ToBytes();
                byte[] body = Encoding.ASCII.GetBytes(ecapeSymbols(Nick)+ separator + ecapeSymbols(Password));
                byte[] packet = new byte[head.Length + body.Length];
                head.CopyTo(packet, 0);
                body.CopyTo(packet, head.Length);
                return packet;
            }

            

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public LogIn(byte[] data) : base(data)
            {
                string text = Encoding.ASCII.GetString(data.Skip(1 + 2 * IDLength).ToArray());
                List<string> fields = extractTextFields(text);
                this.Nick = fields[0];
                this.Password = fields[1];

            }
        }

        /// <summary>
        /// End of comunication- Logout from from Communication with server
        /// </summary>
        public class LogOut: Message, IMessage
        {
            /// <summary>
            /// Set type of message
            /// </summary>
            public LogOut() : base()
            {
                Type = (byte)ImplementedMessages.LogoutMessage;
            }

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public LogOut(byte[] data) : base(data)
            {
                
            }

        }


        /// <summary>
        /// End of comunication- Logout from from Communication with server
        /// </summary>
        public class LogInConfirm : Message, IMessage
        {
            /// <summary>
            /// Set type of message
            /// </summary>
            public LogInConfirm() : base()
            {
                Type = (byte)ImplementedMessages.LogInConfirmMessage;
            }

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public LogInConfirm(byte[] data) : base(data)
            {

            }

        }



        /// <summary>
        /// End of comunication- Logout from from Communication with server
        /// </summary>
        public class LogOutConfirm : Message, IMessage
        {
            /// <summary>
            /// Set type of message
            /// </summary>
            public LogOutConfirm() : base()
            {
                Type = (byte)ImplementedMessages.LogOutConfirmMessage;
            }

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public LogOutConfirm(byte[] data) : base(data)
            {

            }

        }

        /// <summary>
        /// End of comunication- Logout from from Communication with server
        /// </summary>
        public class IntroducingClient: Message, IMessage
        {
            /// <summary>
            /// Set type of message
            /// </summary>
            public IntroducingClient() : base()
            {
                Type = (byte)ImplementedMessages.IntrducingClientMessage;
            }

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public IntroducingClient(byte[] data) : base(data)
            {

            }

        }


        /// <summary>
        /// End of comunication- Logout from from Communication with server
        /// </summary>
        public class IntroducingServer : Message, IMessage
        {
            /// <summary>
            /// Set type of message
            /// </summary>
            public IntroducingServer() : base()
            {
                Type = (byte)ImplementedMessages.IntrducingServerMessage;
            }

            /// <summary>
            /// Construct message from recieved byte[]
            /// </summary>
            /// <param name="data"></param>
            public IntroducingServer(byte[] data) : base(data)
            {

            }

        }

        /// <summary>
        /// Error of comunication
        /// </summary>
        public class Error : TextMessages
        {
            public Error(string MSG = "UNKNOWN") : base()
            {
                Type = (byte)ImplementedMessages.ErrorMessage;
                this.MSG = MSG;
            }


            /// <summary>
            /// Message of error
            /// </summary>
            
            public string MSG;

            /// <summary>
            /// Make byte[] from this
            /// </summary>
            /// <returns></returns>
            public override byte[] ToBytes()
            {
                byte[] head = base.ToBytes();
                byte[] body = Encoding.ASCII.GetBytes(MSG);
                byte[] packet = new byte[head.Length + body.Length];
                head.CopyTo(packet, 0);
                body.CopyTo(packet, head.Length);
                return packet;
            }


            /// <summary>
            /// Construct Error from byte[]
            /// </summary>
            /// <param name="data"></param>
            public Error(byte[] data) : base(data)
            {

                this.MSG = Encoding.ASCII.GetString(data.Skip(1 + 2 * IDLength).ToArray());

            }
        }
        /// <summary>
        /// Container just for carrying byte[] for equipments
        /// </summary>
        public struct ByteMessage
        {
            public IPAddress ip;
            public byte[] message;
        }

    }

}