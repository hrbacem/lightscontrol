﻿using Lights;
using Lights.Parsers;
using Lights.Protocols;
using Lights.Transport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lights
{

    namespace Parsers
    {
        /// <summary>
        /// Class to serialize Mapping of Equipment and protocols
        /// </summary>
        public static class DefultExtractorMap
        {

            /// <summary>
            /// Try save mapping of id and protocols 
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="mapa"></param>
            /// <returns></returns>
            public static bool TrySaveProtocolData(string filename, Dictionary<IID, IProtocol> mapa)
            {
                Dictionary<ID, string> map = new Dictionary<ID, string>();
                foreach (var item in mapa)
                {
                    map.Add((ID)item.Key, ProtocolFactory.ProtocolToString(item.Value));
                }
                return ParserJsonDictionary<ID, string>.SaveData(filename, map);

            }

            /// <summary>
            /// Try Load protocols and create sates
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="mapa"></param>
            /// <param name="values"></param>
            /// <returns></returns>
            public static bool TryReadProtocolData(string filename, out Dictionary<IID, IProtocol> mapa, out Dictionary<IID, IState> values)
            {
                mapa = null;
                values = null;

                if (!File.Exists(filename))
                {
                    return false;
                }

                if (!ParserJsonDictionary<ID, string>.LoadData(filename, out Dictionary<ID, string> map))
                    return false;



                if (map != null)
                {
                    Dictionary<IID, IProtocol> maping = new Dictionary<IID, IProtocol>();
                    Dictionary<IID, IState> defaultValues = new Dictionary<IID, IState>();


                    foreach (var item in map)
                    {

                        var id = item.Key;


                        IProtocol protocol = ProtocolFactory.ChooseProtocol(item.Value, id, defaultValues);


                        maping.Add(id, protocol);

                    }
                    mapa = maping;
                    values = defaultValues;
                    return true;

                }
                mapa = null;
                values = null;
                return false;
            }
        }
    }
}