﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Lights;
using Lights.Messages;
using Android.Util;
using Android.Graphics;
using Lights.Transport;

namespace Mobile_Real_Time_User.Assets
{

    /// <summary>
    /// Prvek držící dva buttony nad sebou
    /// </summary>
    public class TopDownButton : LinearLayout
        {
        Context context;
        public delegate void buttonAction(object sender, EventArgs eventArgs);

        public buttonAction topAction;
        public buttonAction downAction;
        public TopDownButton(Context context ) :
        base(context)
        {
            Init(context);            
        }
        public TopDownButton(Context context, IAttributeSet attrs) :
        base(context, attrs)
        {
            Init(context);
        }

        public TopDownButton(Context context, IAttributeSet attrs, int defStyle) :
        base(context, attrs, defStyle)
        {
            Init(context);
        }
       
       
       public Button topButton;
        public TextView contentText;
       public Button downButton;
       

           
        private void Init(Context ctx)
        {
            context = ctx;
            this.Orientation = Orientation.Vertical;
            topButton = new Button(context);
            contentText = new TextView(context);
            contentText.Text = "LABEL";
            
            downButton = new Button(context);
            topButton.Click += TopOnClick;
            downButton.Click += DownOnClick;
            this.AddView(topButton);
            this.AddView(contentText);
            this.AddView(downButton);
            
        }

        private void GenerateController(LinearLayout container, IState state)
        {
            foreach (var item in state.Parts.Data)
            {
                SeekBar bar = new SeekBar(context);
                container.AddView(bar);
            }
        }

        private void TopOnClick(object sender, EventArgs eventArgs)
        {
            topAction?.Invoke(sender, eventArgs);
        }

        private void DownOnClick(object sender, EventArgs eventArgs)
        {
            downAction?.Invoke(sender, eventArgs);
        }



    }
}