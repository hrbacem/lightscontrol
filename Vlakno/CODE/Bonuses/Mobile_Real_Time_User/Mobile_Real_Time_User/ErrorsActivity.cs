﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Lights.Messages;
using static GlobalAppState;



namespace Mobile_Real_Time_User.Assets
{

    [Activity(Label = "Errors")]
    public class ErrorsActivity:MessengerActivity 
    {
        EditText message_edit;
        TextView show_chat;
        ScrollView text_scroll;
        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState,true);
            SetContentView(Resource.Layout.activity_messenger);
            FloatingActionButton buttonSend = FindViewById<FloatingActionButton>(Resource.Id.button_send);
            buttonSend.Click += SendOnClick;

            message_edit = FindViewById<EditText>(Resource.Id.message_txt);
            show_chat = FindViewById<TextView>(Resource.Id.show_txt);
            text_scroll = FindViewById<ScrollView>(Resource.Id.view_scroll);

            // set update of conversation
            user.errorsUpdate += RecievedMsgConversation;

            //Write old messages - from connecting to now
            lock (user.Errors)
                foreach (var message in user.Errors)
                {
                    show_chat.Text += message + "\n";
                }
            // show the newest 
            text_scroll.FullScroll(FocusSearchDirection.Down);

        }


        private void SendOnClick(object sender, EventArgs eventArgs)
        {                      
            user.Errors.Add(message_edit.Text);
            show_chat.Text += message_edit.Text + "\n";
            message_edit.Text = string.Empty;
        }

        private  void RecievedMsgConversation(string newmsg)
        {
            // just update textview 
            try
            {
                show_chat.Text += newmsg + "\n";
            }
            catch(Exception e){}

            text_scroll.FullScroll(FocusSearchDirection.Down);                           
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
        }
    }
}