﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Lights.Messages;
using Lights.Transport;
using static Android.Graphics.Pdf.PdfDocument;
using Android.Net.Wifi;
using Mobile_Real_Time_User.Assets;
using static GlobalAppState;
using Android.Support.V7.App;


namespace Mobile_Real_Time_User.Assets
{

    [Activity(Label = "Control")]
    public class ControllerActivity : AppCompatActivity
    {
        
        Context context;
        internal LinearLayout linLayout;
        internal List<EqControl> controlled;

        //messenger setting
        FloatingActionButton fabMessenger;
        Android.Graphics.ColorFilter defaultFilter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            controlled = new List<EqControl>();

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_controller);


            fabMessenger = FindViewById<FloatingActionButton>(Resource.Id.fab_messenger);


            fabMessenger.Click += Fab2OnClick;

            

            defaultFilter = fabMessenger.Background.ColorFilter;
            user.conversationUpdate += (string s) => {
                fabMessenger.Background.SetColorFilter(Android.Graphics.Color.Green, Android.Graphics.PorterDuff.Mode.Multiply);
            };



           
            context = this;
            linLayout = FindViewById<LinearLayout>(Resource.Id.holder_equipments);
            linLayout.SetBackgroundColor(Android.Graphics.Color.Black);
            FirstInitialize();
            
            user.dataUpdate += UpdateData;

        }
        protected override void OnPostResume()
        {
            base.OnPostResume();
            fabMessenger.Background.SetColorFilter(defaultFilter);
        }

        internal void FirstInitialize()
        {
            //First create
            int order = 0;
            lock (user.Data)
                foreach (var item in user.Data)
                {
                    var eq = new EqControl(this, item.Value)
                    {
                        fathersControl = controlled
                    };
                    eq.update += ReCreateData;
                    eq.sendUpdate += RefresUpdate;
                    eq.position = order++;
                    controlled.Add(eq);
                    HorizontalScrollView scrollView = new HorizontalScrollView(context);
                    scrollView.AddView(eq);
                    scrollView.SetPadding(10, 10, 10, 10);
                    linLayout.AddView(scrollView);
                }
        }

        internal void UpdateData(Scene scene)
        {
                       
                foreach (EqControl item in controlled)
                {
                item.state = user.Data[item.state.Id];
                item.UpdateView();
                }
        }

        internal void ReCreateData(Scene scene)
        {

            linLayout.RemoveAllViews();

            lock (user.Data)
            {
                for (int i = 0; i < controlled.Count; i++)
                {
                    IState st = user.Data[controlled[i].state.Id];
                    controlled[i].Dispose();
                    EqControl eq = new EqControl(this, st)
                    {
                        position = i,
                        fathersControl = controlled
                    };
                    eq.update += ReCreateData;
                    eq.sendUpdate += RefresUpdate;
                    HorizontalScrollView scrollView = new HorizontalScrollView(context);
                    scrollView.AddView(eq);
                    controlled[i] = eq;
                    scrollView.SetPadding(10, 10, 10, 10);
                    linLayout.AddView(scrollView);
                }



            }
        }

        private void Fab2OnClick(object sender, EventArgs eventArgs)
        {
            var intent = new Intent(this, typeof(MessengerActivity));
            fabMessenger.Background.SetColorFilter(defaultFilter);
            this.StartActivity(intent);

        }

        private void RefreshOnClick(object sender, EventArgs eventArgs)
        {
            if (isDemo) return;
            Scene msg = new Scene
            {
                Recipient = user.ServerID,
                Sender = user.UserID
            };
            foreach (var item in user.Data)
            {
                msg.EquipmentsData.Add(item.Value);
            }
            user.Send(msg);

        }

        private void RefresUpdate(Scene scene)
        {
            if (isDemo) return;
            user.SendUpdateToServer();

        }
    }
}