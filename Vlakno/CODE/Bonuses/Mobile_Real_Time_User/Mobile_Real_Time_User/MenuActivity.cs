﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Lights;
using Lights.EquipmnetsPureData;
using Lights.Messages;
using Lights.Transport;
using static Android.Graphics.Pdf.PdfDocument;
using Android.Net.Wifi;
using Mobile_Real_Time_User.Assets;
using static GlobalAppState;
using Android.Support.V7.App;
using Lights.Comunication;

namespace Mobile_Real_Time_User.Assets
{
    [Activity(Label = "Menu")]
    public class MenuActivity : AppCompatActivity
    {
        // Messenger
        Android.Graphics.ColorFilter defaultFilter;
        FloatingActionButton fab;


        CoordinatorLayout coorLayout;



        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_menu);
            coorLayout = FindViewById<CoordinatorLayout>(Resource.Id.menu_activity);

            
            //create Floating button -> messenger
            fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;
            defaultFilter = fab.Background.ColorFilter;
            // set update of conversation 
            user.conversationUpdate += (string s) => {
                fab.Background.SetColorFilter(Android.Graphics.Color.Green, Android.Graphics.PorterDuff.Mode.Multiply);
            };

            Button buttonControl = FindViewById<Button>(Resource.Id.button_controller);
            buttonControl.Click += ControlOnClick;

            Button buttonNotReady = FindViewById<Button>(Resource.Id.button_nr);
            buttonNotReady.Click += NROnClick;

            Button buttonCreateDefault = FindViewById<Button>(Resource.Id.button_script);
            buttonCreateDefault.Click += CreateDemoOnClick;
        }

        protected override void OnPostResume()
        {
            base.OnPostResume();
            // reset messenger - because of message sending
            fab.Background.SetColorFilter(defaultFilter);
        }
        protected override void OnDestroy()
        {
            // Lougout from server
            user.MessageToServer("User " + nameOfUser + " left this server.");
            user.Logout();

            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(500);
                if (!user.ConnectedToServer)
                {
                    user.Shutdown();
                    base.OnDestroy();

                    
                    return;
                     
                    
                }
                user.Logout();
            }

            //Dissconnect failed
            {
                try
                {
                    View view = (View)this.coorLayout;
                    Snackbar.Make(view, "STILL CONNECTED", Snackbar.LengthLong)
                    .SetAction("Close", v => { }).Show();
                }
                catch (Exception e) { }
            }

        }



        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            var intent = new Intent(this, typeof(MessengerActivity));
            // reset messenger
            fab.Background.SetColorFilter(defaultFilter);
            this.StartActivity(intent);
        }

        private void ControlOnClick(object sender, EventArgs eventArgs)
        {
            //open Control of Equipments
            var intent = new Intent(this, typeof(ControllerActivity));
            this.StartActivity(intent);
        }

        private void CreateDemoOnClick(object sender, EventArgs eventArgs)
        {

            
            isDemo = true;
            user = new 
                User(IPAddress.Any,6666);
            
            user.Data.Clear();
            {

                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 108, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);




                    Components<RGBLight> Data = new Components<RGBLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<RGBLight> state = new State<RGBLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }
                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 10, 203, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<RGBLight> Data = new Components<RGBLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<RGBLight> state = new State<RGBLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }

                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 107, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<RGBLight> Data = new Components<RGBLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<RGBLight> state = new State<RGBLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }


                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 105, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<SpyderLight> Data = new Components<SpyderLight>(true);
                    for (int i = 0; i < Data.Size - 2; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }
                    for (int i = Data.Size - 2; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)90;
                    }
                    State<SpyderLight> state = new State<SpyderLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }


                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 106, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<SpyderLight> Data = new Components<SpyderLight>(true);
                    for (int i = 0; i < Data.Size - 2; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }
                    for (int i = Data.Size - 2; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)90;
                    }
                    State<SpyderLight> state = new State<SpyderLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }

                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 81, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 

                    ID = new ID(id);
                    Components<DIMMLight> Data = new Components<DIMMLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<DIMMLight> state = new State<DIMMLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }

                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 102, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<RGBLight> Data = new Components<RGBLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<RGBLight> state = new State<RGBLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }

                { //real one
                    ID ID;
                    byte[] id = new byte[ID.IDLength] { 192, 168, 1, 104, 0, 0, 0, 0, 0, 0 }; //NAPSAT ADRESU ZARIZENI 
                    ID = new ID(id);

                    Components<RGBLight> Data = new Components<RGBLight>(true);
                    for (int i = 0; i < Data.Size; i++)
                    {
                        Data.Data[i] = (byte)250;
                    }

                    State<RGBLight> state = new State<RGBLight>(ID, 12, Data);
                    user.Data.Add(ID, state);


                }

                user.DataCFO = true;
            }
            try
            {
                View view = (View)sender;
                Snackbar.Make(view, "BASIC DEMO WAS GENERATED\nPhone simulate server and client", Snackbar.LengthLong)
                .SetAction("Close", v => { }).Show();
            }
            catch (Exception e){}



        }



        private void NROnClick(object sender, EventArgs eventArgs)
        {
            //open Control of Equipments
            var intent = new Intent(this, typeof(ErrorsActivity));
            this.StartActivity(intent);

        }

    }
}